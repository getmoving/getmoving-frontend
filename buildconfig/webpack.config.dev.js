/* eslint-disable @typescript-eslint/no-var-requires */
const { merge } = require('webpack-merge');
const baseConfig = require('./webpack.config.base');

module.exports = merge(baseConfig, {
  mode: 'development',
  devServer: {
    port: 5000,
    historyApiFallback: true,
    watchContentBase: true,
  },
  devtool: 'source-map',
});
