/* eslint-disable @typescript-eslint/no-var-requires */
const { DefinePlugin, EnvironmentPlugin } = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');

module.exports = {
  entry: './src/index.tsx',
  output: {
    path: path.join(__dirname, '../dist'),
    filename: '[name].[contentHash].bundle.js',
  },
  watchOptions: {
    poll: 5000,
    ignored: ['node_modules'],
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx|ts|tsx)$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
      },
      {
        test: /\.svg$/,
        use: ['@svgr/webpack', 'url-loader'],
      },
      {
        test: /\.(jpe?g|png|gif|pdf)$/,
        use: ['file-loader'],
      },
      { test: /\.mjs$/, type: 'javascript/auto' },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({ template: './src/index.html' }),
    new DefinePlugin({
      ENV_PRODUCTION: JSON.stringify(process.env.ENV_PRODUCTION === 'true'),
    }),
    new EnvironmentPlugin({
      API_ROOT: 'http://localhost:3001/',
      NODE_ENV: '',
    }),
    new CopyPlugin({
      patterns: [{ from: './src/assets', to: 'assets/' }],
    }),
  ],
  resolve: {
    modules: ['src', 'node_modules'],
    extensions: ['.ts', '.tsx', '.js', '.jsx', '.json', '*'],
    alias: {
      '@hapi/joi': 'joi-browser',
    },
  },
};
