import firebase from 'firebase/app';
/* eslint-disable */
import 'firebase/auth';
/* eslint-enable */

const firebaseConfig = {
  apiKey: 'AIzaSyAhi0JqFUm-6qATmXRLeRZ8P8MEJB0ENdk',
  authDomain: 'getmoving-5a566.firebaseapp.com',
  databaseURL: 'https://getmoving-5a566.firebaseio.com',
  projectId: 'getmoving-5a566',
  storageBucket: 'getmoving-5a566.appspot.com',
  messagingSenderId: '848154387748',
  appId: '1:848154387748:web:aa54511d84f2762ff5ffb1',
  measurementId: 'G-8SSH9J9SGW',
};

// Initialize Firebase
const fApp = firebase.initializeApp(firebaseConfig);
export default fApp;
