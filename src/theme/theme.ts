import { createTheme } from '@material-ui/core/styles';
import colors from 'utils/colors';

const theme = createTheme({
  palette: {
    primary: {
      main: colors.primary,
      light: colors.active,
    },
    secondary: {
      main: colors.secondary,
    },
    error: {
      main: colors.error,
    },
  },
  mixins: {
    toolbar: {
      height: 85,
    },
  },
  typography: {
    fontFamily: 'Poppins, sans-serif',
    subtitle1: {
      fontSize: 30,
      fontWeight: 500,
    },
  },
  overrides: {
    MuiTypography: {
      body1: {
        fontSize: '14px',
      },
    },
    MuiButton: {
      root: {
        fontSize: '15px',
        borderRadius: 30,
        textTransform: 'unset',
        width: 200,
      },
      outlinedPrimary: {
        border: `2px solid ${colors.primary}`,
        '&:hover': {
          border: `2px solid ${colors.primary}`,
        },
      },
    },
  },
});

export default theme;
