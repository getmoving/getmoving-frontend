// // Do not remove, import is used as type and needed as such
// import { Theme } from '@material-ui/core/styles/createMuiTheme';

// declare module '@material-ui/core/styles/createMuiTheme' {
//   interface Theme {
//     appDrawer: {
//       width: React.CSSProperties['width'];
//       collapsedWidth: React.CSSProperties['width'];
//     };
//     labeledSelect: {
//       small: React.CSSProperties['width'];
//       large: React.CSSProperties['width'];
//     };
//     layout: {
//       main: React.CSSProperties['width'];
//       sidebar: React.CSSProperties['width'];
//     };
//     modal: {
//       small: React.CSSProperties['width'];
//       large: React.CSSProperties['width'];
//     };
//     customShadows: {
//       default: string;
//     };
//   }
//   // allow configuration using `createMuiTheme`
//   interface ThemeOptions {
//     appDrawer?: {
//       width?: React.CSSProperties['width'];
//       collapsedWidth?: React.CSSProperties['width'];
//     };
//     labeledSelect?: {
//       small: React.CSSProperties['width'];
//       large: React.CSSProperties['width'];
//     };
//     layout?: {
//       main: React.CSSProperties['width'];
//       sidebar: React.CSSProperties['width'];
//     };
//     modal?: {
//       small: React.CSSProperties['width'];
//       large: React.CSSProperties['width'];
//     };
//     customShadows?: {
//       default: string;
//     };
//   }
// }
