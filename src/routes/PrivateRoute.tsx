import React, { ComponentType, useEffect, useContext } from 'react';
import { Route, Redirect, RouteComponentProps } from 'react-router-dom';

import RouteContext from 'contexts/RouteContext';

interface Props {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  component: ComponentType<RouteComponentProps<any>> | ComponentType<any>;
  isAuthenticated: boolean;
  path: string;
  exact?: boolean;
}

const PrivateRoute: React.FC<Props> = (
  { component: Comp, isAuthenticated, path, exact, ...rest },
) => {
  const { setVariant, setComponentType } = useContext(RouteContext);
  useEffect(() => {
    setVariant('black');
    setComponentType('private');
  }, []);

  const routeComponent = (props: RouteComponentProps) => (
    isAuthenticated ? <Comp {...props} /> : <Redirect to="/" />);
  return <Route path={path} exact={exact} {...rest} render={routeComponent} />;
};

export default PrivateRoute;
