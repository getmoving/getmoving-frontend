import { Theme } from '@material-ui/core';
import { createStyles } from '@material-ui/core/styles';

interface Props {
  variant?: string;
}

const styles = (theme: Theme) => createStyles<string, Props>({
  hamburger: {
    position: (props) => (props.variant === 'transparent' ? 'static' : 'fixed'),
    top: 0,
    right: 0,
    zIndex: 1,
    color: 'white',
    '& svg': {
      width: '2rem',
      height: '2rem',
    },
  },
  loginText: {
    color: 'white',
    marginRight: theme.spacing(2),
    [theme.breakpoints.down('sm')]: {
      padding: theme.spacing(2),
    },
  },
  drawer: {
    backgroundColor: 'black',
  },
  buttonWidth: {
    width: '145px',
  },
  container: {
    // eslint-disable-next-line no-confusing-arrow
    background: (props) => props.variant === 'transparent'
      ? 'linear-gradient(black, transparent)'
      : 'black',
    padding: '1em 100px',
    height: theme.mixins.toolbar.height,
    position: (props) => (props.variant === 'transparent' ? 'absolute' : 'fixed'),
    zIndex: 500,
    transition: 'background .35s, opacity .35s .35s',
    [theme.breakpoints.down('sm')]: {
      height: 60,
      padding: theme.spacing(2),
      paddingRight: 0,
      width: '100%',
      boxSizing: 'border-box',
    },
  },
  relativePosition: { position: 'relative' },
  navLink: {
    color: `${theme.palette.secondary.main}`,
    textDecoration: 'unset',
    lineHeight: '14px',
    '&:hover': {
      fontWeight: 600,
      color: `${theme.palette.primary.light}`,
      transition: 'font-weight 0.05s ease-out 0.05s',
      '&::before': {
        content: '""',
        position: 'absolute',
        bottom: 0,
        left: 0,
        width: '50%',
        height: 3,
        borderRadius: 3,
        backgroundColor: theme.palette.primary.light,
      },
    },
    '&::after': {
      display: 'block',
      content: 'attr(title)',
      fontWeight: 'bold',
      height: '1px',
      color: 'transparent',
      overflow: 'hidden',
      visibility: 'hidden',
    },
  },
  navLinkContainer: {
    marginLeft: '15px',
  },
  activeNavLink: {
    color: `${theme.palette.primary.main}`,
    textDecoration: 'unset',
    lineHeight: '14px',
    fontWeight: 600,
    '&::before': {
      content: '""',
      position: 'absolute',
      bottom: 0,
      left: 0,
      width: '50%',
      height: 3,
      borderRadius: 3,
      backgroundColor: theme.palette.primary.main,
    },
    '&::after': {
      display: 'block',
      content: 'attr(title)',
      fontWeight: 'bold',
      height: '1px',
      color: 'transparent',
      overflow: 'hidden',
      visibility: 'hidden',
    },
  },
});

export default styles;
