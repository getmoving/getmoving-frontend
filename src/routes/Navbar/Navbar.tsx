import React, { useContext } from 'react';
import { Grid, Typography, Theme, useMediaQuery } from '@material-ui/core';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import { NavLink, useHistory } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import fApp from 'firebase/init-firebase';

import RouteContext from 'contexts/RouteContext';
import AuthContext from 'contexts/AuthContext';
import WhiteLogo from 'assets/WhiteLogo';
import styles from './Navbar.styles';
import navbarItems from './Navbar.utils';
import MobileView from './components/MobileView';
import RightSection from './components/RightSection';

const useStyles = makeStyles(styles);

const Navbar: React.FC = () => {
  const { variant = 'black', componentType } = useContext(RouteContext);
  const {
    container,
    navLinkContainer,
    relativePosition,
    activeNavLink,
    navLink,
  } = useStyles({
    variant,
  });
  const history = useHistory();
  const { user } = useContext(AuthContext);
  const { t } = useTranslation();
  const theme: Theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down('sm'));
  const renderMiddleSection = !['blank', 'login', 'register'].includes(componentType);

  const handleUserAuth = () => {
    if (user) fApp.auth().signOut();
    else history.push('/auth/login');
  };

  if (isMobile) return <MobileView handleUserAuth={handleUserAuth} />;

  return (
    <Grid container className={container} alignItems="center">
      <Grid item xs={renderMiddleSection ? 3 : 5}>
        <NavLink to="/">
          <WhiteLogo />
        </NavLink>
      </Grid>
      <Grid item xs={renderMiddleSection ? 6 : 2}>
        <Grid container spacing={3} className={navLinkContainer}>
          {renderMiddleSection && Array.from(navbarItems).map(([path, text]) => (
            <Grid key={path} item>
              <Typography className={relativePosition}>
                <NavLink
                  activeClassName={activeNavLink}
                  className={navLink}
                  to={path}
                  title={t(text)}
                >
                  {t(text)}
                </NavLink>
              </Typography>
            </Grid>
          ))}
        </Grid>
      </Grid>
      <Grid item xs={renderMiddleSection ? 3 : 5}>
        <Grid container justifyContent="flex-end">
          <RightSection handleUserAuth={handleUserAuth} />
        </Grid>
      </Grid>
    </Grid>
  );
};

export default Navbar;
