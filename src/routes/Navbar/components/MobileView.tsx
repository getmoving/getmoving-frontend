import React, { useState, useContext } from 'react';
import { Box, IconButton, Drawer, List, ListItem, Typography } from '@material-ui/core';
import { Menu } from '@material-ui/icons';
import { makeStyles } from '@material-ui/core/styles';
import { useTranslation } from 'react-i18next';
import { NavLink } from 'react-router-dom';

import WhiteLogo from 'assets/WhiteLogo';
import RouteContext from 'contexts/RouteContext';
import navbarItems from 'routes/Footer/Footer.utils';
import styles from 'routes/Navbar/Navbar.styles';
import RightSection from './RightSection';

interface Props {
  handleUserAuth: () => void
}

const useStyles = makeStyles(styles);

const MobileView: React.FC<Props> = ({ handleUserAuth }) => {
  const { variant, componentType } = useContext(RouteContext);
  const { t } = useTranslation();
  const [isOpen, setIsOpen] = useState(false);
  const renderMiddleSection = !['blank', 'login', 'register'].includes(componentType);
  const {
    container,
    relativePosition,
    activeNavLink,
    navLink,
    drawer,
    hamburger,
  } = useStyles({ variant });

  return (
    <Box className={container} display="flex" justifyContent="space-between">
      <NavLink to="/">
        <WhiteLogo />
      </NavLink>
      {
        componentType !== 'blank' && (
          <>
            <IconButton className={hamburger} onClick={() => setIsOpen(true)}>
              <Menu />
            </IconButton>
            <Drawer classes={{ paper: drawer }} anchor="right" open={isOpen} onClose={() => setIsOpen(false)}>
              <List>
                {renderMiddleSection && Array.from(navbarItems).map(([path, text]) => (
                  <ListItem button key={text}>
                    <Typography className={relativePosition}>
                      <NavLink
                        activeClassName={activeNavLink}
                        className={navLink}
                        to={path}
                        title={t(text)}
                      >
                        {t(text)}
                      </NavLink>
                    </Typography>
                  </ListItem>
                ))}
                <RightSection handleUserAuth={handleUserAuth} />
              </List>
            </Drawer>
          </>
        )
      }
    </Box>
  );
};

export default MobileView;
