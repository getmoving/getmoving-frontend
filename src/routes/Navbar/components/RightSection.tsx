import React, { useContext } from 'react';
import { Button, Grid, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { useHistory } from 'react-router-dom';
import { useTranslation } from 'react-i18next';

import AuthContext from 'contexts/AuthContext';
import RouteContext from 'contexts/RouteContext';
import styles from 'routes/Navbar/Navbar.styles';
import ProfileDropdown from './ProfileDropdown';

const useStyles = makeStyles(styles);

interface Props {
  handleUserAuth: () => void
}

const RightSection: React.FC<Props> = ({ handleUserAuth }) => {
  const { variant, componentType } = useContext(RouteContext);
  const { user } = useContext(AuthContext);
  const history = useHistory();
  const { t } = useTranslation();
  const { buttonWidth, loginText } = useStyles({ variant });

  const buttonType = componentType === 'register' ? 'login' : 'register';

  const renderLeftButton = () => {
    switch (componentType) {
      case 'login':
        return <Typography className={loginText}>{t('navbar.haveNoAcc')}</Typography>;
      case 'register':
        return <Typography className={loginText}>{t('navbar.haveAcc')}</Typography>;
      default:
        return user ? <ProfileDropdown /> : (
          <Button onClick={handleUserAuth} color="primary" className={buttonWidth}>
            {t('navbar.login')}
          </Button>
        );
    }
  };

  return (
    <>
      <Grid item style={{ textAlign: 'center', alignSelf: 'center' }}>
        {renderLeftButton()}
      </Grid>
      {!user && (
        <Grid item style={{ textAlign: 'center' }}>
          <Button
            className={buttonWidth}
            onClick={() => history.push(`/auth/${buttonType}`)}
            color="primary"
            variant="outlined"
          >
            {t(`navbar.${buttonType}`)}
          </Button>
        </Grid>
      )}
    </>
  );
};

export default RightSection;
