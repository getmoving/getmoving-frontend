import React from 'react';
import { Popper, Button, Grow, Paper, ClickAwayListener, Typography, Divider, Box } from '@material-ui/core';
import { AccountCircle, ExpandMore } from '@material-ui/icons';

import fApp from 'firebase/init-firebase';
import useProfileDropdown from './useProfileDropdown';

const ProfileDropdown: React.FC = () => {
  const {
    tooltip,
    endIcon,
    divider,
    buttonRoot,
    user,
    changePage,
    t,
    anchorRef,
    open,
    setOpen,
    handleToggle,
  } = useProfileDropdown();

  return (
    <>
      <div ref={anchorRef}>
        <Button
          classes={{ endIcon: open ? endIcon : '' }}
          color="primary"
          onClick={handleToggle}
          startIcon={<AccountCircle />}
          endIcon={<ExpandMore />}
        >
          {t('navbar.myProfile')}
        </Button>
      </div>
      <Popper
        placement="bottom"
        style={{ zIndex: 99999999 }}
        open={open}
        anchorEl={anchorRef.current}
        transition
        modifiers={{
          flip: { enabled: false },
          arrow: { enabled: true },
        }}
      >
        {({ TransitionProps }) => (
          <Grow {...TransitionProps}>
            <Paper className={tooltip}>
              <ClickAwayListener onClickAway={() => setOpen(false)}>
                <Box>
                  <Typography>{user?.displayName || 'Username'}</Typography>
                  <Divider className={divider} />
                  <Button onClick={() => changePage('/profile/personal-data')} className={buttonRoot}>
                    {t('profile.personalData')}
                  </Button>
                  <Button onClick={() => changePage('/profile/my-subscription')} className={buttonRoot}>
                    {t('navbar.mySubscription')}
                  </Button>
                  <Button onClick={() => changePage('/profile/invoices')} className={buttonRoot}>
                    {t('navbar.invoices')}
                  </Button>
                  <Button onClick={() => changePage('/profile/my-plan')} className={buttonRoot}>
                    {t('navbar.myWorkoutPlan')}
                  </Button>
                  <Divider className={divider} />
                  <Button onClick={() => fApp.auth().signOut()} className={buttonRoot}>{t('navbar.logout')}</Button>
                </Box>
              </ClickAwayListener>
            </Paper>
          </Grow>
        )}
      </Popper>
    </>
  );
};

export default ProfileDropdown;
