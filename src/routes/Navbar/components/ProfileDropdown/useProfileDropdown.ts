import { useContext, useEffect, useRef, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';

import AuthContext from 'contexts/AuthContext';
import styles from './ProfileDropdown.styles';

const useStyles = makeStyles(styles);

const useProfileDropdown = () => {
  const { t } = useTranslation();
  const { tooltip, endIcon, divider, buttonRoot } = useStyles();
  const [open, setOpen] = useState(false);
  const anchorRef = useRef<HTMLDivElement>(null);
  const { user } = useContext(AuthContext);
  const history = useHistory();

  const handleToggle = () => setOpen((prevOpen) => !prevOpen);
  const changePage = (path: string) => {
    history.push(path);
    handleToggle();
  };

  const prevOpen = useRef(open);
  useEffect(() => {
    if (prevOpen.current === true && open === false && anchorRef.current) {
      anchorRef.current.focus();
    }
    prevOpen.current = open;
  }, [open]);

  return {
    tooltip,
    endIcon,
    divider,
    user,
    changePage,
    anchorRef,
    handleToggle,
    open,
    setOpen,
    t,
    buttonRoot,
  };
};

export default useProfileDropdown;
