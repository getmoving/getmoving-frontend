import { createStyles } from '@material-ui/core/styles';
import colors from 'utils/colors';

const styles = () => createStyles({
  tooltip: {
    zIndex: 9999,
    backgroundColor: colors.black1F,
    borderRadius: 6,
    paddingTop: 10,
    paddingBottom: 10,
    width: 280,
    '& p, span': {
      color: 'white',
      marginLeft: 30,
      marginRight: 30,
      fontWeight: 'normal',
      justifyContent: 'left',
    },
    '& button': {
      width: '100%',
    },
    '& p': {
      fontSize: 20,
      fontWeight: 700,
    },
  },
  endIcon: {
    transform: 'rotate(180deg)',
  },
  divider: {
    backgroundColor: colors.greyF6,
    marginTop: 10,
    marginBottom: 10,
  },
  buttonRoot: {
    borderRadius: 'unset',
    '&:hover': {
      backgroundColor: colors.grey33,
    },
  },
});

export default styles;
