import { Theme } from '@material-ui/core';
import { createStyles } from '@material-ui/core/styles';

const styles = (theme: Theme) => createStyles({
  container: {
    backgroundColor: 'black',
  },
  navHolderContainer: {
    padding: '1em 100px',
    height: '78px',
    [theme.breakpoints.down('sm')]: {
      height: 'unset',
      textAlign: 'center',
    },
  },
  legalsHolderContainer: {
    padding: '1em 100px',
    height: '70px',
    [theme.breakpoints.down('sm')]: {
      height: 'unset',
      textAlign: 'center',
    },
  },
  dividerStyle: {
    width: '100%',
    backgroundColor: 'white',
  },
  modifierBiggerFontSize: {
    fontSize: '18px',
  },
  navLink: {
    color: `${theme.palette.secondary.main}`,
    textDecoration: 'unset',
    lineHeight: '18px',
    '&:hover': {
      fontWeight: 600,
      color: `${theme.palette.primary.light}`,
      transition: 'font-weight 0.05s ease-out 0.05s',
    },
    '&::after': {
      display: 'block',
      content: 'attr(title)',
      fontWeight: 'bold',
      height: '1px',
      color: 'transparent',
      overflow: 'hidden',
      visibility: 'hidden',
    },
  },
  activeNavLink: {
    color: `${theme.palette.primary.main}`,
    textDecoration: 'unset',
    lineHeight: '18px',
    position: 'relative',
    fontWeight: 600,
    '&::after': {
      position: 'absolute',
      content: '""',
      height: '3px',
      bottom: '-4px',
      left: '0',
      right: '0',
      width: '2em',
      background: `${theme.palette.primary.main}`,
      borderRadius: '3px',
    },
  },
  socialIcon: {
    '& svg:hover': {
      color: theme.palette.primary.main,
    },
  },
});

export default styles;
