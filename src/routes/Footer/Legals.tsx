import React from 'react';
import { Grid, Typography, Theme, useMediaQuery, Box } from '@material-ui/core';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import { useTranslation } from 'react-i18next';

import barionlogo from 'assets/bariontransparent.svg';
import LegalsModal from './LegalsModal';
import styles from './Footer.styles';

const useStyles = makeStyles(styles);

const Legals: React.FC = () => {
  const { legalsHolderContainer, navLink } = useStyles();
  const { t } = useTranslation();
  const theme: Theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down('sm'));
  const [document, setDocument] = React.useState<'terms' |'privacy' | ''>('');

  return (
    <Grid container className={legalsHolderContainer} alignItems="center">
      <Grid item xs={12} md={6} lg={6}>
        <Box display="flex" textAlign="center" alignItems="center">
          <Typography color="secondary">{t('legals.copyright')}</Typography>
          {!isMobile && <Box m={2} />}
          {!isMobile && <img alt="barion" style={{ width: 230 }} src={barionlogo} />}
        </Box>
      </Grid>
      {isMobile && (
        <Grid item>
          <img alt="barion" style={{ width: 230 }} src={barionlogo} />
        </Grid>
      )}
      <Grid item xs={12} md={6} lg={6}>
        <Grid container alignItems="center" justifyContent={isMobile ? 'center' : 'flex-end'} spacing={3}>
          <Grid item>
            <Typography
              style={{ cursor: 'pointer' }}
              onClick={() => setDocument('privacy')}
              className={navLink}
              title={t('legals.privacy')}
            >
              {t('legals.privacy')}
            </Typography>
          </Grid>
          <Grid item>
            <Typography
              style={{ cursor: 'pointer' }}
              className={navLink}
              onClick={() => setDocument('terms')}
              title={t('legals.termsOfUse')}
            >
              {t('legals.termsOfUse')}
            </Typography>
          </Grid>
        </Grid>
      </Grid>
      <LegalsModal document={document} handleClose={() => setDocument('')} />
    </Grid>
  );
};

export default Legals;
