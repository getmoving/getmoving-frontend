import React from 'react';
import { makeStyles, createStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';

interface Props {
  document: 'terms' | 'privacy' | '';
  handleClose: () => void;
}

const PRIVACY_URL = 'https://drive.google.com/file/d/1zbhGlFqoRo_gy4PQVaRKXjXz0fbPgyHF/preview';
const TERMS_URL = 'https://drive.google.com/file/d/18jcNKi7bwZQ9qDSFYoDJs1S2jnruvk3a/preview';

const useStyles = makeStyles(() => createStyles({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    '& .react-pdf__Document': {
      height: '100%',
      overflowY: 'scroll',
      outline: 'none',
    },
  },
}));

const LegalsModal: React.FC<Props> = ({ document, handleClose }) => {
  const classes = useStyles();

  return (
    <Modal
      aria-labelledby="transition-modal-title"
      aria-describedby="transition-modal-description"
      className={classes.modal}
      open={!!document}
      onClose={handleClose}
      closeAfterTransition
      BackdropComponent={Backdrop}
      BackdropProps={{
        timeout: 500,
      }}
    >
      <Fade in={!!document}>
        <iframe
          title="legals"
          src={document === 'terms' ? TERMS_URL : PRIVACY_URL}
          width="50%"
          height="100%"
        />
      </Fade>
    </Modal>
  );
};

export default LegalsModal;
