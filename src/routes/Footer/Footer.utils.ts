const navbarItems = new Map();
navbarItems.set('/about/videos', 'navbar.videos');
navbarItems.set('/about/recipes', 'navbar.recipes');
navbarItems.set('/about/workout-generator', 'navbar.generator');
navbarItems.set('/about/blog', 'navbar.blog');
navbarItems.set('/about/prices', 'navbar.prices');
navbarItems.set('/about/contact', 'navbar.contact');

export default navbarItems;
