import React from 'react';
import { NavLink } from 'react-router-dom';
import { Grid, Typography, IconButton, Theme, useMediaQuery } from '@material-ui/core';
import { useTranslation } from 'react-i18next';
import { makeStyles, useTheme } from '@material-ui/core/styles';

import { FacebookIcon, InstagramIcon } from 'assets/icons';
import styles from './Footer.styles';
import navbarItems from './Footer.utils';

const useStyles = makeStyles(styles);

const NavHolder: React.FC = () => {
  const { navHolderContainer, modifierBiggerFontSize,
    activeNavLink, navLink, socialIcon,
  } = useStyles();
  const { t } = useTranslation();
  const theme: Theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down('sm'));

  return (
    <Grid container className={navHolderContainer} alignItems="center">
      <Grid item xs={12} md={10} lg={10}>
        <Grid container spacing={isMobile ? 2 : 7}>
          {Array.from(navbarItems).map(([path, text]) => (
            <Grid item xs={12} sm="auto" key={path}>
              <Typography className={modifierBiggerFontSize}>
                <NavLink
                  activeClassName={activeNavLink}
                  className={navLink}
                  to={path}
                  title={t(text)}
                >
                  {t(text)}
                </NavLink>
              </Typography>
            </Grid>
          ))}
        </Grid>
      </Grid>
      <Grid item xs={12} md={2} lg={2}>
        <Grid container alignItems="center" justifyContent={isMobile ? 'center' : 'flex-end'}>
          <Grid item>
            <a href="https://www.facebook.com/GetMoving.Official/" target="__blank">
              <IconButton className={socialIcon}>
                <FacebookIcon />
              </IconButton>
            </a>
          </Grid>
          <Grid item>
            <a href="https://www.instagram.com/getmoving_official/" target="__blank">
              <IconButton className={socialIcon}>
                <InstagramIcon />
              </IconButton>
            </a>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default NavHolder;
