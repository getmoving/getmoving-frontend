import React from 'react';
import { Grid, Divider } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import styles from './Footer.styles';
import BottomNavHolder from './BottomNavHolder';
import Legals from './Legals';

const useStyles = makeStyles(styles);

const Footer: React.FC = () => {
  const { container, dividerStyle } = useStyles();
  return (
    <Grid container className={container} alignItems="center">
      <BottomNavHolder />
      <Divider className={dividerStyle} />
      <Legals />
    </Grid>
  );
};

export default Footer;
