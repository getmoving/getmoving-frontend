import { createStyles } from '@material-ui/core/styles';

const styles = () => createStyles({
  Container: { minHeight: '90vh' },
});

export default styles;
