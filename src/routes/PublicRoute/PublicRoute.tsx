import React, { ComponentType } from 'react';
import { Route, RouteComponentProps } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';

import Footer from 'routes/Footer';
import styles from './PublicRoute.styles';

const useStyles = makeStyles(styles);

interface Props {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  component: ComponentType<RouteComponentProps<any>> | ComponentType<any>;
  path: string;
  exact?: boolean;
}

const PublicRoute: React.FC<Props> = (
  { component: Comp, path, exact, ...rest },
) => {
  const classes = useStyles();
  const routeComponent = (props: RouteComponentProps) => (
    <>
      <div className={classes.Container}>
        <Comp {...props} />
      </div>
      <Footer />
    </>
  );
  return <Route path={path} exact={exact} {...rest} render={routeComponent} />;
};

export default PublicRoute;
