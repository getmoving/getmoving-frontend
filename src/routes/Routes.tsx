import React, { Suspense, lazy, useContext } from 'react';
import { LinearProgress } from '@material-ui/core';
import { Switch, Redirect, Route } from 'react-router-dom';

import AuthContext from 'contexts/AuthContext';
import PublicRoute from './PublicRoute';
import PrivateRoute from './PrivateRoute';

const LandingPage = lazy(() => import('containers/Landing/LandingPage'));
const AboutContainer = lazy(() => import('containers/About'));
const AuthContainer = lazy(() => import('containers/Auth'));
const ConfirmEmailPage = lazy(() => import('containers/ConfirmEmail'));
const ProfileContainer = lazy(() => import('containers/Profile'));

const Routes: React.FC = () => {
  const { user, isLoading } = useContext(AuthContext);
  if (isLoading) return <LinearProgress />;

  return (
    <Suspense fallback={<LinearProgress color="primary" />}>
      <Switch>
        <PublicRoute path="/" exact component={LandingPage} />
        <PrivateRoute path="/profile" component={ProfileContainer} isAuthenticated={!!user} />
        <Route path="/about" component={AboutContainer} />
        <PublicRoute path="/auth/confirm" component={ConfirmEmailPage} />
        <PublicRoute path="/auth" component={AuthContainer} />
        <Redirect to="/" />
      </Switch>
    </Suspense>
  );
};

export default Routes;
