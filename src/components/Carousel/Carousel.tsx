import React, { CSSProperties } from 'react';
import Slider, { Settings } from 'react-slick';
import { Box, useMediaQuery } from '@material-ui/core';
import { makeStyles, useTheme } from '@material-ui/core/styles';

import colors from 'utils/colors';
import { ChevronLeft, ChevronRight } from '@material-ui/icons';
import styles from './Carousel.styles';

const useStyles = makeStyles(styles);

interface Arrow {
  onClick?: () => void;
  style?: CSSProperties;
  className?: string;
}

interface CarouselProps {
  slidesToShow?: number;
  slidesToScroll?: number;
}

const Carousel: React.FC<CarouselProps> = ({ children, slidesToShow = 4, slidesToScroll = 4 }) => {
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down('xs'));
  const { root, dotsContainer, dots } = useStyles({ isMobile });

  const PrevArrow = ({ className, style, onClick }: Arrow) => (
    <ChevronLeft
      fontSize="large"
      className={className}
      style={{ ...style, color: colors.active }}
      onClick={onClick}
    />
  );

  const NextArrow = ({ className, style, onClick }: Arrow) => (
    <ChevronRight
      fontSize="large"
      className={className}
      style={{ ...style, color: colors.active }}
      onClick={onClick}
    />
  );

  const settings: Settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow,
    slidesToScroll,
    prevArrow: <PrevArrow />,
    nextArrow: <NextArrow />,
    appendDots: (d) => <Box className={dotsContainer}>{d}</Box>,
    customPaging: () => <Box className={dots} />,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          initialSlide: 2,
          dots: false,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          dots: false,
        },
      },
    ],
  };

  return (
    <Box className={root}>
      <Slider dotsClass={dotsContainer} {...settings}>
        {children}
      </Slider>
    </Box>
  );
};

export default Carousel;
