import { createStyles, Theme } from '@material-ui/core/styles';
import colors from 'utils/colors';
import { RightArrowIcon, LeftArrowIcon } from 'assets/icons';

interface Props {
  isMobile?: boolean;
}

const styles = (theme: Theme) => createStyles<string, Props>({
  root: {
    '& .slick-prev:before': {
      color: colors.active,
      content: `url(${LeftArrowIcon})`,
    },
    '& .slick-next:before': {
      color: colors.active,
      content: `url(${RightArrowIcon})`,
    },
  },
  dots: {
    margin: theme.spacing(0, 1),
    height: 7,
    width: 100,
    backgroundColor: colors.black1F,
    [theme.breakpoints.down('sm')]: {
      width: 40,
    },
  },
  dotsContainer: {
    marginTop: '2em',
    marginRight: '2em',
    textAlign: 'right',
    display: 'flex',
    justifyContent: 'flex-end',
    cursor: 'pointer',
    '& li': {
      listStyleType: 'none',
    },
    '& .slick-active > div': {
      backgroundColor: colors.active,
    },
  },
});

export default styles;
