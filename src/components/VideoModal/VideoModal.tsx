import React from 'react';
import ReactPlayer from 'react-player';
import { Modal, Box, IconButton, useMediaQuery } from '@material-ui/core';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import { HighlightOff } from '@material-ui/icons';
import styles from 'containers/Landing/Videos/Videos.styles';
import VideoContext from 'contexts/VideoContext';

const useStyles = makeStyles(styles);

const VideoModal: React.FC = () => {
  const { url, setUrl } = React.useContext(VideoContext);

  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down('xs'));

  const { videoBox, closeIcon } = useStyles({ isMobile });

  return (
    <Modal open={!!url}>
      <Box className={videoBox}>
        <IconButton className={closeIcon} color="primary" onClick={() => setUrl('')}>
          <HighlightOff fontSize="large" />
        </IconButton>
        <ReactPlayer
          height="100%"
          width="100%"
          url={url}
          controls
        />
      </Box>
    </Modal>
  );
};

export default VideoModal;
