import React from 'react';
import { Box, DialogTitle, DialogContentText, DialogContent, DialogActions, Dialog, Button } from '@material-ui/core';

interface Props {
  open: boolean;
  title: string;
  description?: string;
  close: () => void;
  ok: () => void;
  isConfirm: boolean;
  isDisabled?: boolean;
}

const AlertDialog: React.FC<Props> = ({
  title, description, open, close, ok, isConfirm, isDisabled,
}) => (
  <Dialog
    open={open}
    onClose={close}
  >
    <Box m={2}>
      <DialogTitle>{title}</DialogTitle>
      <DialogContent>
        <DialogContentText>
          {description}
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        {isConfirm ? (
          <>
            <Button disabled={isDisabled} onClick={close} color="primary">
              Mégse
            </Button>
            <Button disabled={isDisabled} variant="contained" onClick={ok} color="primary" autoFocus>
              Igen
            </Button>
          </>
        )
          : (
            <Button disabled={isDisabled} variant="contained" onClick={close} color="primary" autoFocus>
              OK
            </Button>
          )}
      </DialogActions>
    </Box>
  </Dialog>
);

export default AlertDialog;
