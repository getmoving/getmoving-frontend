import React from 'react';
import { createStyles, withStyles, makeStyles } from '@material-ui/core/styles';
import { InputBase, Box, Button, Theme } from '@material-ui/core';
import { Search } from '@material-ui/icons';

const useStyles = makeStyles({
  button: {
    borderTopLeftRadius: 0,
    borderBottomLeftRadius: 0,
    height: 41,
    top: -8,
    width: '10vw',
    minWidth: 60,
  },
});

const BootstrapInput = withStyles((theme: Theme) => createStyles({
  root: {
    'label + &': {
      marginTop: theme.spacing(3),
    },
    '& .MuiSvgIcon-root': {
      position: 'relative',
      left: 40,
      zIndex: 1,
    },
  },
  input: {
    paddingLeft: 42,
    borderTopLeftRadius: 30,
    borderBottomLeftRadius: 30,
    position: 'relative',
    backgroundColor: theme.palette.common.white,
    border: '1px solid #ced4da',
    fontSize: 16,
    width: '40vw',
    padding: '10px 12px',
    transition: theme.transitions.create(['border-color', 'box-shadow']),
    '&:focus': {
      borderColor: theme.palette.primary.main,
    },
  },
}))(InputBase);

interface Props {
  value: string;
  onChange: (searchText: string) => void;
  onPress: () => void;
  disabled: boolean;
  placeholder: string;
}

const SearchField: React.FC<Props> = ({ onChange, disabled, placeholder, value, onPress }) => {
  const { button } = useStyles();

  return (
    <Box>
      <BootstrapInput
        value={value}
        onChange={(e) => onChange(e.target.value)}
        startAdornment={<Search />}
        placeholder={placeholder}
      />
      <Button disabled={disabled} className={button} color="primary" variant="contained" onClick={onPress}>Keresés</Button>
    </Box>
  );
};

export default SearchField;
