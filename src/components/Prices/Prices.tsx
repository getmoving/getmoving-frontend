import React from 'react';
import { useTranslation } from 'react-i18next';
import { Box, Typography, Card, Grid, Button, Tooltip, IconButton } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { Help } from '@material-ui/icons';
import { useHistory, useLocation } from 'react-router-dom';

import { GreenTickIcon } from 'assets/icons';
import useSubscription from 'containers/Profile/Subscription/useSubscription';
import { useQuery } from 'utils/useQuery';
import AuthContext from 'contexts/AuthContext';
import { PaymentType } from 'containers/Profile/Subscription/Subscription.types';

const useStyles = makeStyles({
  cardRoot: {
    boxShadow: '-4px -4px 20px rgba(0, 0, 0, 0.05), 4px 4px 20px rgba(0, 0, 0, 0.05)',
    padding: 20,
    marginBottom: 32,
    maxWidth: 666,
    backgroundColor: 'rgba(255,255,255,0.8)',
    borderTopLeftRadius: 0,
    borderTopRightRadius: 0,
  },
  help: {
    position: 'absolute',
    top: 0,
    right: 0,
  },
});

interface Props {
  selected?: PaymentType;
  onSelect: (v: PaymentType) => void;
}

const Prices: React.FC<Props> = ({ onSelect, selected }) => {
  const history = useHistory();
  const { t } = useTranslation();
  const { loading } = useSubscription();
  const query = useQuery();
  const location = useLocation();
  const paymentId = query.get('paymentId');
  const { isActive, user } = React.useContext(AuthContext);

  const { cardRoot, help } = useStyles();
  const featureList = t('prices.features', { returnObjects: true }) as string[];

  return (
    <Box my={4} textAlign="center" margin="auto">
      {/* <Box mb={4}>
        <Typography>Nyitási akció 2021.12.01-től 2022.01.15-ig!</Typography>
      </Box> */}

      <Grid
        container
        direction="row"
        justifyContent="center"
        alignItems="center"
        spacing={4}
      >
        <Grid item>
          <Box width={393}>
            <Box
              bgcolor="#FFDC30"
              height={90}
              display="flex"
              justifyContent="center"
              alignItems="center"
              style={{ borderTopLeftRadius: 5, borderTopRightRadius: 5 }}
            >
              <Typography style={{ fontSize: 30 }}>
                Havi előfizetés
              </Typography>
            </Box>
            <Card className={cardRoot} style={{ border: selected === PaymentType.MONTHLY ? '2px solid #FFDC30' : 'none' }}>
              <Box borderBottom="2px solid #C4C4C4" textAlign="left" position="relative">
                <Tooltip title={t('prices.help') as string}>
                  <IconButton className={help}>
                    <Help color="primary" />
                  </IconButton>
                </Tooltip>
                <Box>
                  <Typography>Havonta:</Typography>
                  <Box display="flex" mb={2} alignItems="center">
                    <Typography style={{ fontSize: 17, fontWeight: 700, marginRight: 24 }}>
                      3 590 Ft / hó
                    </Typography>
                    {/* <Typography
                    style={{ fontSize: 12, color: '#EB5757', textDecoration: 'line-through' }}>
                  3 590 Ft / hó helyett
                    </Typography> */}
                  </Box>
                </Box>
                <Box height={78}>
                  <Typography>
                    Első havi akció*:
                  </Typography>
                  <Typography style={{ fontSize: 17, fontWeight: 700, marginRight: 24, color: 'red' }}>
                      1 290 Ft
                  </Typography>
                </Box>
                <Box height={48}>
                  <Typography>*Havi előfizetés esetén kedvezményes az első hónap.</Typography>
                </Box>
                {/* <Box height={48} /> */}
              </Box>
              <Box>
                <Grid container justifyContent="space-between" style={{ marginTop: 16 }}>
                  {featureList.map((item) => (
                    <Grid key={item} item xs={12}>
                      <Box display="flex" alignItems="center" my={1}>
                        <GreenTickIcon />
                        <Box m={1} />
                        <Typography>
                          {item}
                        </Typography>
                      </Box>
                    </Grid>
                  ))}
                </Grid>
                <Box m={2} />
                <Button
                  disabled={!!paymentId || isActive || loading}
                  variant="contained"
                  color="primary"
                  // eslint-disable-next-line no-nested-ternary
                  onClick={() => (user ? (location.pathname.includes('subscription') ? onSelect(PaymentType.MONTHLY) : history.push('/profile/subscription')) : history.push('/auth/login'))}
                >
                  Ezt választom
                </Button>
              </Box>
            </Card>
          </Box>
        </Grid>
        <Grid item>
          <Box width={393}>
            <Box
              bgcolor="#FFC800"
              height={90}
              display="flex"
              justifyContent="center"
              alignItems="center"
              style={{ borderTopLeftRadius: 5, borderTopRightRadius: 5 }}
            >
              <Typography style={{ fontSize: 30 }}>
                Éves előfizetés
              </Typography>
            </Box>
            <Card className={cardRoot} style={{ border: selected === PaymentType.YEARLY ? '2px solid #FFC800' : 'none' }}>
              <Box borderBottom="2px solid #C4C4C4" textAlign="left" position="relative">
                <Tooltip title={t('prices.help') as string}>
                  <IconButton className={help}>
                    <Help color="primary" />
                  </IconButton>
                </Tooltip>
                <Box>
                  <Typography>Összesen egy évre:*:</Typography>
                  <Box display="flex" mb={2} alignItems="center">
                    <Typography style={{ fontSize: 17, fontWeight: 700, marginRight: 24 }}>
                      34 990 Ft / év
                    </Typography>
                    {/* <Typography
                    style={{ fontSize: 12, color: '#EB5757', textDecoration: 'line-through' }}>
                      34 990 Ft / év helyett
                    </Typography> */}
                  </Box>
                </Box>
                <Box height={78}>
                  <Typography>Éves fizetés esetén havonta:*:</Typography>
                  <Box display="flex" mb={4} alignItems="center">
                    <Typography style={{ fontSize: 17, fontWeight: 700, marginRight: 24 }}>
                      2 915 Ft / hó
                    </Typography>
                    {/* <Typography
                    style={{ fontSize: 12, color: '#EB5757', textDecoration: 'line-through' }}>
                      2 915 Ft / hó helyett
                    </Typography> */}
                  </Box>
                </Box>
                <Box height={48}>
                  <Typography>
                    *Egyösszegű fizetés eséten kevesebb,
                    mint 10 hónap áráért 12 havi előfizetést adunk!
                  </Typography>
                </Box>
              </Box>
              <Box>
                <Grid container justifyContent="space-between" style={{ marginTop: 16 }}>
                  {featureList.map((item) => (
                    <Grid key={item} item xs={12}>
                      <Box display="flex" alignItems="center" my={1}>
                        <GreenTickIcon />
                        <Box m={1} />
                        <Typography>
                          {item}
                        </Typography>
                      </Box>
                    </Grid>
                  ))}
                </Grid>
                <Box m={2} />
                <Button
                  disabled={!!paymentId || isActive || loading}
                  variant="contained"
                  color="primary"
                  // eslint-disable-next-line no-nested-ternary
                  onClick={() => (user ? (location.pathname.includes('subscription') ? onSelect(PaymentType.YEARLY) : history.push('/profile/subscription')) : history.push('/auth/login'))}
                >
                  Ezt választom
                </Button>
              </Box>
            </Card>
          </Box>
        </Grid>
      </Grid>
    </Box>
  );
};

export default Prices;
