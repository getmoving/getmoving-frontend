import { BoxProps } from '@material-ui/core';

export interface Props extends BoxProps {
  centered?: boolean;
}
