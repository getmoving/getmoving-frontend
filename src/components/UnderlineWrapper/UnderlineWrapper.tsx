import React from 'react';
import { Box } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

import styles from './UnderlineWrapper.styles';
import { Props } from './UnderlineWrapper.types';

const useStyles = makeStyles(styles);

const UnderlineWrapper: React.FC<Props> = ({
  width,
  height,
  color,
  bottom,
  children,
  className,
  centered,
  ...rest
}) => {
  const { underline } = useStyles({ width, height, bottom, color, centered });

  return (
    <Box className={`${underline} ${className}`} {...rest}>
      {children}
    </Box>
  );
};

export default UnderlineWrapper;
