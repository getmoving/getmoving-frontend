import { Theme, createStyles } from '@material-ui/core/styles';
import { Props } from './UnderlineWrapper.types';

const styles = (theme: Theme) => createStyles<string, Props>({
  underline: {
    position: 'relative',
    width: 'fit-content',
    '&::after': {
      position: 'absolute',
      content: '""',
      bottom: (props) => props.bottom || -4,
      left: ({ centered, width }) => (centered ? `calc(50% - ${(width as number || 100) / 2}px)` : 0),
      right: '0',
      borderRadius: '3px',
      height: (props) => props.height || 6,
      width: (props) => props.width || 100,
      background: (props) => props.color || theme.palette.primary.main,
    },
  },
});

export default styles;
