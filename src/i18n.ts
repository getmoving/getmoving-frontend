import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import hu from './translation/hu.json';

const resources = {
  hu: {
    translation: hu,
  },
};

i18n
  .use(initReactI18next)
  .init({
    lng: 'hu',
    fallbackLng: 'hu',
    keySeparator: '.',
    resources,
    interpolation: {
      escapeValue: false,
    },
  });

export default i18n;
