import React, { useState } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import { ThemeProvider } from '@material-ui/core/styles';
import CookieConsent from 'react-cookie-consent';

import RouteContext, { VariantType, ComponentTypeInstance } from 'contexts/RouteContext';
import { withAuthContext } from 'contexts/AuthContext';
import Navbar from 'routes/Navbar';

import './i18n';
import { Typography } from '@material-ui/core';
import Routes from './routes';
import theme from './theme/theme';

if (ENV_PRODUCTION) {
  // this needs for yarn build
  // eslint-disable-next-line global-require
  require('offline-plugin/runtime');
}

const App = withAuthContext(() => {
  const [currentVariant, setCurrentVariant] = useState<VariantType>('transparent');
  const [componentType, setComponentType] = useState<ComponentTypeInstance>('blank');

  return (
    <ThemeProvider theme={theme}>
      <RouteContext.Provider
        value={{
          variant: currentVariant,
          setVariant: setCurrentVariant,
          componentType,
          setComponentType,
        }}
      >
        <Router>
          <Navbar />
          <Routes />
          <CookieConsent buttonText="Rendben!">
            <Typography>
              Weboldalunk annak érdekében,
              hogy jobb felhasználói élményt biztosítson cookie-kat használ.
            </Typography>
          </CookieConsent>
        </Router>
      </RouteContext.Provider>
    </ThemeProvider>
  );
});

ReactDOM.render(<App />, document.getElementById('app'));
