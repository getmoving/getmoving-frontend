import React, { useContext, useEffect } from 'react';
import { Box, Typography } from '@material-ui/core';
import { useTranslation } from 'react-i18next';

import RouteContext from 'contexts/RouteContext';
import { UnderlineWrapper } from 'components';
import img from 'assets/confirm.svg';

const ConfirmEmail: React.FC = () => {
  const { t } = useTranslation();
  const { setVariant, setComponentType } = useContext(RouteContext);

  useEffect(() => {
    setVariant('black');
    setComponentType('blank');
  }, []);

  return (
    <Box textAlign="center" pt={15}>
      <UnderlineWrapper>
        <Typography variant="h2">{t('auth.confirmEmail')}</Typography>
      </UnderlineWrapper>
      <img src={img} alt="confirmEmail" />
      <Typography>{t('auth.confirm')}</Typography>
    </Box>
  );
};

export default ConfirmEmail;
