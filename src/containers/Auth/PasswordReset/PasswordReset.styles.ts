import { Theme } from '@material-ui/core';
import { createStyles } from '@material-ui/core/styles';

const styles = (theme: Theme) => createStyles({
  title: {
    fontSize: 50,
    fontWeight: 900,
  },
  mobileTitle: {
    fontSize: 30,
    fontWeight: 900,
  },
  textStyle: {
    fontSize: 20,
  },
  mobileTextStyle: {
    fontSize: 16,
  },
  form: {
    marginTop: theme.spacing(5),
    padding: theme.spacing(10),
    display: 'flex',
    flexDirection: 'column',
  },
  mobileForm: {
    marginTop: theme.spacing(5),
    padding: 'unset',
    display: 'flex',
    flexDirection: 'column',
  },
});

export default styles;
