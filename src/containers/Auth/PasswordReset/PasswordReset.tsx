import React, { FormEvent, useState } from 'react';
import { Snackbar, Box, Typography, Button, TextField, Theme, useMediaQuery } from '@material-ui/core';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import { useTranslation } from 'react-i18next';
import MuiAlert, { AlertProps } from '@material-ui/lab/Alert';

import { UnderlineWrapper } from 'components';
import fApp from 'firebase/init-firebase';
import styles from './PasswordReset.styles';

function Alert(props: AlertProps) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const useStyles = makeStyles(styles);

const PasswordReset: React.FC = () => {
  const { title, mobileTitle, form, mobileForm, mobileTextStyle, textStyle } = useStyles();
  const { t } = useTranslation();
  const theme: Theme = useTheme();
  const [email, setEmail] = useState<string>('');
  const [snackbarSeverity, setSnackbarSeverity] = useState<undefined | 'success' | 'error'>();
  const [snackbarMessage, setSnackbarMessage] = useState<string>('');
  const [shallOpenSnackbar, setSnackbarVisibility] = useState<boolean>(false);
  const isWide = useMediaQuery('(min-width:1440px)');
  const isMobile = useMediaQuery(theme.breakpoints.down('sm'));

  const onSubmit = async (e: FormEvent) => {
    e.preventDefault();
    try {
      await fApp.auth().sendPasswordResetEmail(email);
      setSnackbarMessage(t('auth.pwResetEmailSent'));
      setSnackbarSeverity('success');
    } catch {
      setSnackbarMessage(t('auth.pwResetEmailError'));
      setSnackbarSeverity('error');
    } finally {
      setSnackbarVisibility(true);
    }
  };

  return (
    <Box my={4} mx={isMobile ? 1 : 8}>
      <UnderlineWrapper style={{ margin: 'auto' }} centered>
        <Typography className={isMobile ? mobileTitle : title}>{t('auth.forgotPasswordTitle')}</Typography>
      </UnderlineWrapper>
      <form className={isMobile ? mobileForm : form} onSubmit={onSubmit}>
        <Box mb={isMobile ? 2 : 5}>
          <Typography variant="h6" className={isMobile ? mobileTextStyle : textStyle}>{t('auth.pwResetEmailHelperText')}</Typography>
        </Box>
        <TextField
          required
          InputLabelProps={{ required: false }}
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          name="email"
          type="email"
          label={t('common.email')}
        />
        <Box textAlign="center" mt={5}>
          <Button type="submit" variant="contained" color="primary">
            {t('auth.send')}
          </Button>
        </Box>
      </form>
      <Snackbar
        open={shallOpenSnackbar}
        autoHideDuration={6000}
        onClose={() => setSnackbarVisibility(false)}
      >
        <Alert onClose={() => setSnackbarVisibility(false)} severity={snackbarSeverity}>
          <Typography variant={isWide ? 'h6' : 'body1'}>{snackbarMessage}</Typography>
        </Alert>
      </Snackbar>
    </Box>
  );
};

export default PasswordReset;
