import React, { FormEvent, useState, useEffect, useContext } from 'react';
import { Box, Typography, Button, TextField, Collapse, IconButton, InputAdornment } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { useTranslation } from 'react-i18next';
import { NavLink, useHistory } from 'react-router-dom';

import { DoubleRightArrow } from 'assets/icons';
import RouteContext from 'contexts/RouteContext';
import { UnderlineWrapper } from 'components';
import fApp from 'firebase/init-firebase';
import { Alert } from '@material-ui/lab';
import { Close, Visibility, VisibilityOff } from '@material-ui/icons';
import styles from './Login.styles';

const useStyles = makeStyles(styles);

const Login: React.FC = () => {
  const { title, form, link } = useStyles();
  const { t } = useTranslation();
  const history = useHistory();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [showPassword, setShowPassword] = useState(false);
  const { setVariant, setComponentType } = useContext(RouteContext);
  const [alertVisible, setAlertVisibility] = React.useState(false);

  useEffect(() => {
    setVariant('black');
    setComponentType('login');
  }, []);

  const onSubmit = async (e: FormEvent) => {
    e.preventDefault();
    try {
      const { user } = await fApp
        .auth()
        .signInWithEmailAndPassword(email, password);
      if (user) history.push('/about/videos');
    } catch (error) {
      setAlertVisibility(true);
    }
  };

  return (
    <Box my={4} mx={8}>
      <UnderlineWrapper style={{ margin: 'auto' }} centered>
        <Typography className={title}>{t('auth.login')}</Typography>
      </UnderlineWrapper>
      <form className={form} onSubmit={onSubmit}>
        <TextField
          required
          autoFocus
          InputLabelProps={{ required: false }}
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          name="email"
          type="email"
          label={t('common.email')}
        />
        <Box mb={5} />
        <TextField
          required
          InputLabelProps={{ required: false }}
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          name="password"
          type={showPassword ? 'text' : 'password'}
          label={t('auth.password')}
          InputProps={{ endAdornment: (
            <InputAdornment position="end">
              <IconButton
                style={{ padding: 6 }}
                onClick={() => setShowPassword(!showPassword)}
              >
                {showPassword ? <Visibility /> : <VisibilityOff />}
              </IconButton>
            </InputAdornment>) }}
        />
        <Box mt={5}>
          <NavLink to="/auth/reset-password" className={link}>{t('auth.forgotPasswordLink')}</NavLink>
        </Box>
        <Box textAlign="center" mt={5}>
          <Button type="submit" variant="contained" color="primary" endIcon={<DoubleRightArrow />}>
            {t('auth.login')}
          </Button>
        </Box>
      </form>
      <Box mt={4}>
        <Collapse in={alertVisible}>
          <Alert
            severity="error"
            action={(
              <IconButton
                aria-label="close"
                color="inherit"
                size="small"
                onClick={() => {
                  setAlertVisibility(false);
                }}
              >
                <Close fontSize="inherit" />
              </IconButton>
            )}
          >
            Sikertelen belépés! Hibás email cím vagy jelszó!
          </Alert>
        </Collapse>
      </Box>
    </Box>
  );
};

export default Login;
