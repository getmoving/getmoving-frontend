import { Theme } from '@material-ui/core';
import { createStyles } from '@material-ui/core/styles';
import color from 'utils/colors';

const styles = (theme: Theme) => createStyles({
  title: {
    fontSize: 50,
    fontWeight: 900,
  },
  link: {
    color: color.linkBlue,
    fontFamily: '\'Open Sans\', sans-serif',
    textDecoration: 'unset',
    letterSpacing: '1px',
  },
  form: {
    marginTop: theme.spacing(5),
    display: 'flex',
    flexDirection: 'column',
  },
});

export default styles;
