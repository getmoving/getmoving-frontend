import React from 'react';
import { Switch, Route, RouteComponentProps, Redirect } from 'react-router-dom';

import Register from './Register';
import AuthWrapper from './Wrapper';
import Login from './Login';
import PasswordReset from './PasswordReset';

const AboutContainer: React.FC<RouteComponentProps> = ({ match }) => (
  <AuthWrapper>
    <Switch>
      <Route path={`${match.path}/register`} component={Register} />
      <Route path={`${match.path}/login`} component={Login} />
      <Route path={`${match.path}/reset-password`} component={PasswordReset} />
      <Redirect to="/" />
    </Switch>
  </AuthWrapper>
);

export default AboutContainer;
