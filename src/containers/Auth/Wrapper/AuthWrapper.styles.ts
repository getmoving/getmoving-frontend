import { Theme } from '@material-ui/core';
import { createStyles } from '@material-ui/core/styles';
import landingbg from 'assets/landingbg.png';

const styles = (theme: Theme) => createStyles({
  extraSpacingTop: {
    paddingTop: theme.mixins.toolbar.height,
  },
  img: {
    backgroundImage: `url(${landingbg})`,
    backgroundPosition: '80%',
    backgroundSize: 'cover',
    height: '100vh',
  },
});

export default styles;
