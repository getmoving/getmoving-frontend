import React from 'react';
import { Box, Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import styles from './AuthWrapper.styles';

const useStyles = makeStyles(styles);

const AuthWrapper: React.FC = ({ children }) => {
  const { extraSpacingTop, img } = useStyles();

  return (
    <>
      <Box className={extraSpacingTop} />
      <Grid container>
        <Grid item xs="auto" md={4} lg={5}>
          <Box className={img} />
        </Grid>
        <Grid item xs={12} md={8} lg={7}>
          {children}
        </Grid>
      </Grid>
    </>
  );
};

export default AuthWrapper;
