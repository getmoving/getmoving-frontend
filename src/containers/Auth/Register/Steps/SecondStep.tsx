import React, { useContext } from 'react';
import { TextField, Box, Typography, Grid } from '@material-ui/core';
import { useTranslation } from 'react-i18next';
import RegisterContext from 'contexts/RegisterContext';

const SecondStep: React.FC = () => {
  const { t } = useTranslation();
  const { state: {
    password, passwordRepeat,
    addressCity, addressStreet, addressZipCode, addressNumber, addressCountry,
  }, dispatch } = useContext(RegisterContext);

  return (
    <>
      <Typography style={{ fontSize: 25, marginBottom: 30 }}>Számlázási cím</Typography>
      <TextField
        required
        InputLabelProps={{ required: false }}
        value={addressCountry}
        onChange={(e) => dispatch({ addressCountry: e.target.value })}
        name="country"
        label="Ország"
      />
      <Box mb={5} />
      <Grid container justifyContent="space-between">
        <Grid item xs={5}>
          <TextField
            fullWidth
            required
            InputLabelProps={{ required: false }}
            value={addressZipCode}
            onChange={(e) => dispatch({ addressZipCode: e.target.value })}
            name="zipCode"
            label="Irányítószám"
          />
        </Grid>
        <Grid item xs={5}>
          <TextField
            required
            fullWidth
            InputLabelProps={{ required: false }}
            value={addressCity}
            onChange={(e) => dispatch({ addressCity: e.target.value })}
            name="city"
            label="Város"
          />
        </Grid>
      </Grid>
      <Box mb={5} />
      <TextField
        required
        InputLabelProps={{ required: false }}
        value={addressStreet}
        onChange={(e) => dispatch({ addressStreet: e.target.value })}
        name="street"
        label="Közterület neve (utca)"
      />
      <Box mb={5} />
      <TextField
        InputLabelProps={{ required: false }}
        value={addressNumber}
        onChange={(e) => dispatch({ addressNumber: e.target.value })}
        name="number"
        label="Házszám, emelet, ajtó"
      />

      <Typography style={{ fontSize: 25, marginBottom: 30, marginTop: 30 }}>
        Profil véglegesítése
      </Typography>

      <TextField
        required
        InputLabelProps={{ required: false }}
        value={password}
        onChange={(e) => dispatch({ password: e.target.value })}
        name="password"
        type="password"
        label={t('auth.password')}
      />
      <Box mb={5} />
      <TextField
        required
        InputLabelProps={{ required: false }}
        value={passwordRepeat}
        onChange={(e) => dispatch({ passwordRepeat: e.target.value })}
        name="passwordRepeat"
        type="password"
        label={t('auth.passwordRepeat')}
      />
    </>
  );
};

export default SecondStep;
