import React, { useContext } from 'react';
import { Grid, TextField, FormControlLabel, Checkbox, Box, FormControl, FormLabel, RadioGroup, Radio } from '@material-ui/core';
import { useTranslation } from 'react-i18next';
import RegisterContext, { GENDER } from 'contexts/RegisterContext';

const FirstStep: React.FC = () => {
  const { t } = useTranslation();
  const {
    state: {
      lastName,
      firstName,
      accepted,
      subbed,
      email,
      gender,
    },
    dispatch,
  } = useContext(RegisterContext);

  return (
    <>
      <Grid container spacing={5}>
        <Grid item xs={12} sm={6}>
          <TextField
            fullWidth
            value={lastName}
            onChange={(e) => dispatch({ lastName: e.target.value })}
            required
            InputLabelProps={{ required: false }}
            name="lastName"
            autoFocus
            label={t('auth.lastName')}
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            fullWidth
            value={firstName}
            onChange={(e) => dispatch({ firstName: e.target.value })}
            required
            InputLabelProps={{ required: false }}
            name="firstName"
            label={t('auth.firstName')}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            fullWidth
            required
            InputLabelProps={{ required: false }}
            value={email}
            onChange={(e) => dispatch({ email: e.target.value })}
            name="email"
            type="email"
            label={t('common.email')}
          />
        </Grid>
        <Grid item xs={12}>
          <FormControl component="fieldset">
            <FormLabel component="legend">{t('auth.gender')}</FormLabel>
            <RadioGroup
              value={gender}
              onChange={(e) => dispatch({ gender: e.target.value as GENDER })}
            >
              <FormControlLabel value={GENDER.OTHER} control={<Radio color="primary" />} label={t('auth.other')} />
              <FormControlLabel value={GENDER.FEMALE} control={<Radio color="primary" />} label={t('auth.female')} />
              <FormControlLabel value={GENDER.MALE} control={<Radio color="primary" />} label={t('auth.male')} />
            </RadioGroup>
          </FormControl>
        </Grid>
      </Grid>
      <Box mb={5} />
      <FormControlLabel
        control={(
          <Checkbox
            required
            color="primary"
            checked={accepted}
            onChange={(e) => dispatch({ accepted: e.target.checked })}
          />
        )}
        label={t('auth.accept')}
      />
      <FormControlLabel
        control={(
          <Checkbox
            color="primary"
            checked={subbed}
            onChange={(e) => dispatch({ subbed: e.target.checked })}
          />
        )}
        label={t('auth.sub')}
      />
    </>
  );
};

export default FirstStep;
