import React, { FormEvent, useContext, useState, useEffect } from 'react';
import axios from 'axios';
import { Box, Typography, Button, Grid, Collapse, IconButton } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';

import RegisterContext, { withRegisterContext } from 'contexts/RegisterContext';
import { UnderlineWrapper } from 'components';
import { API_ROOT } from 'utils/constants';
import RouteContext from 'contexts/RouteContext';
import { Alert } from '@material-ui/lab';
import { Close } from '@material-ui/icons';
import styles from './Register.styles';
import FirstStep from './Steps/FirstStep';
import SecondStep from './Steps/SecondStep';

const STEPS = new Map();
STEPS.set(0, () => <FirstStep />);
STEPS.set(1, () => <SecondStep />);

const useStyles = makeStyles(styles);

const Register: React.FC = () => {
  const { setComponentType, setVariant } = useContext(RouteContext);
  const [alertVisible, setAlertVisibility] = React.useState(false);
  const { title, form } = useStyles();
  const { t } = useTranslation();
  const {
    state: { firstName, lastName, email, password, passwordRepeat, accepted, gender, subbed,
      addressCity, addressCountry, addressZipCode, addressStreet, addressNumber,
    },
  } = useContext(RegisterContext);
  const [step, setStep] = useState(0);
  const isLastStep = step === STEPS.size - 1;
  const history = useHistory();

  useEffect(() => {
    setComponentType('register');
    setVariant('black');
  }, []);

  const verifySteps = () => {
    switch (step) {
      case 0:
        return !!(firstName && lastName && email && accepted);
      case 1:
        return !!(addressCity && addressCountry && addressZipCode && addressStreet && password
          && passwordRepeat && password === passwordRepeat);
      default:
        return true;
    }
  };

  const onSubmit = (e: FormEvent) => {
    e.preventDefault();
    if (!isLastStep) setStep(step + 1);

    if (isLastStep) {
      const user = {
        email,
        password,
        firstName,
        lastName,
        gender,
        subbed,
        address: {
          country: addressCountry,
          zipCode: addressZipCode,
          city: addressCity,
          street: addressStreet,
          number: addressNumber,
        },
      };

      axios
        .post(`${API_ROOT}/api/register-user`, user)
        .then(() => {
          history.push('/auth/confirm');
        })
        .catch(() => {
          setAlertVisibility(true);
        });
    }
  };

  return (
    <Box my={4} mx={8}>
      <Box display="flex" justifyContent="space-between" alignItems="center">
        <UnderlineWrapper>
          <Typography className={title}>{t('auth.register')}</Typography>
        </UnderlineWrapper>
        <Typography variant="h4">{`${step + 1}/${STEPS.size}`}</Typography>
      </Box>
      <form className={form} onSubmit={onSubmit}>
        <Box>{STEPS.get(step)}</Box>
        <Box mb={5} />
        <Grid container justifyContent={step ? 'space-between' : 'center'}>
          {step ? (
            <Grid item>
              <Button
                onClick={() => setStep(step - 1)}
                type="button"
                variant="outlined"
                color="inherit"
              >
                {t('auth.back')}
              </Button>
            </Grid>
          ) : null}
          <Grid item>
            <Button
              disabled={!verifySteps()}
              type="submit"
              variant="contained"
              color="primary"
            >
              {t(`auth.${isLastStep ? 'register' : 'next'}`)}
            </Button>
          </Grid>
        </Grid>
      </form>
      <Box mt={4}>
        <Collapse in={alertVisible}>
          <Alert
            severity="error"
            action={(
              <IconButton
                aria-label="close"
                color="inherit"
                size="small"
                onClick={() => {
                  setAlertVisibility(false);
                }}
              >
                <Close fontSize="inherit" />
              </IconButton>
            )}
          >
            Sikertelen regisztráció! Hibás email cím, vagy nincs internet kapcsolat!
          </Alert>
        </Collapse>
      </Box>
    </Box>
  );
};

export default withRegisterContext(Register);
