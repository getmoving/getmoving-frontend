import React, { useContext, useEffect } from 'react';
import { Box, Typography, Theme, Button, useMediaQuery } from '@material-ui/core';
import { makeStyles, useTheme } from '@material-ui/core/styles';

import RouteContext from 'contexts/RouteContext';
import { UnderlineWrapper } from 'components';
import planbg from 'assets/blogbg.png';
import workoutplan from 'assets/workoutplan.png';
import { useHistory } from 'react-router-dom';
import colors from 'utils/colors';
import AuthContext from 'contexts/AuthContext';
import { GeneratorFeatures } from './components';

const useStyles = makeStyles((theme: Theme) => ({
  img: {
    display: 'flex',
    backgroundImage: `url(${planbg})`,
    width: '100%',
    height: 300,
    backgroundSize: 'cover',
    backgroundPosition: 'center',
  },
  workoutplanimg: {
    display: 'flex',
    backgroundImage: `url(${workoutplan})`,
    width: '100%',
    height: '100%',
    backgroundSize: 'cover',
    backgroundPosition: 'center',
  },
  title: {
    fontSize: 50,
    fontWeight: 900,
    color: 'white',
  },
  container: {
    [theme.breakpoints.only('xs')]: {
      padding: theme.spacing(4, 2),
    },
    padding: theme.spacing(8, 2),
    backgroundColor: colors.greyF6,
    textAlign: 'center',
  },
  trialTitle: {
    textTransform: 'uppercase',
    [theme.breakpoints.only('xs')]: {
      fontSize: 20,
    },
  },
  details: {
    margin: '30px auto',
    maxWidth: 800,
  },
}));

const WorkoutPlanPublic: React.FC = () => {
  const { img, title, workoutplanimg, container, trialTitle, details } = useStyles();
  const { setVariant, setComponentType } = useContext(RouteContext);
  const { user } = useContext(AuthContext);
  const history = useHistory();
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down('xs'));

  useEffect(() => {
    setVariant('black');
    setComponentType('public');
  }, []);

  return (
    <Box pt={10}>
      <Box className={img}>
        <UnderlineWrapper centered color="white" margin="auto">
          <Typography className={title}>Edzésterv generátor</Typography>
        </UnderlineWrapper>
      </Box>
      <Box mt={4} mx={isMobile ? 2 : 12}>
        <Typography style={{ color: '#666666', marginBottom: 32 }}>
        Szeretnél életmódváltásba kezdeni, de nem tudod hol kezdd?
        Edzenél, de gőzöd sincs, hogy mit kéne csinálnod?
Tapasztald meg te is, hogy milyen érzés egy jól felépített edzésterv alapján edzeni!
Tartozz azon kevesek közé, akik folyamatosan tudnak fejlődni a testedzés terén,
köszönhetően a személyreszabott edzéstervnek! Csatlakozz most!
        </Typography>
        <GeneratorFeatures />
        <Box mt={4}>
          <Box className={container}>
            <UnderlineWrapper margin="auto" centered>
              <Typography className={trialTitle} variant="subtitle1">
                Kezdj bele
              </Typography>
            </UnderlineWrapper>
            <Typography className={details}>
              Válaszolj néhány egyszerű kérdésre és könnyedén elkészítheted saját edzésterved.
            </Typography>
            <Button color="primary" variant="contained" onClick={() => history.push(user ? '/profile/my-subscription' : '/auth/login')}>
              Kezdés
            </Button>
          </Box>
        </Box>
        <Box my={8} margin="auto" maxWidth={1024}>
          <img className={workoutplanimg} src={workoutplan} alt="workout plan" />
        </Box>
      </Box>
    </Box>
  );
};

export default WorkoutPlanPublic;
