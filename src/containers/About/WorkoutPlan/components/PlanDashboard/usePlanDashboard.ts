import { useContext } from 'react';
import useAxios from 'axios-hooks';
import { API_ROOT } from 'utils/constants';
import AuthContext from 'contexts/AuthContext';
import { WeekProps } from './Plan.types';

const usePlanDashboard = () => {
  const { userNode, user, refetch: refetchUserNode } = useContext(AuthContext);
  const [{ data, loading }, refetch] = useAxios<WeekProps | undefined>(`${API_ROOT}/api/get-current-week/${user?.uid}/${userNode?.planId}`);

  return { currentWeek: data, loading, refetch, refetchUserNode };
};

export default usePlanDashboard;
