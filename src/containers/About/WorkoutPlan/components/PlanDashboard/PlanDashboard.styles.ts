import { createStyles } from '@material-ui/core/styles';

const styles = () => createStyles({
  container: {
    marginTop: '85px',
  },
});

export default styles;
