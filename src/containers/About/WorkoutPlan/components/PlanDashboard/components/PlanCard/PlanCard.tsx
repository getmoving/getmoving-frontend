import React, { FormEvent, useState, useContext } from 'react';
import { Button, Card, CardContent, CardHeader, CircularProgress, Tooltip, IconButton, useMediaQuery } from '@material-ui/core';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import { useTranslation } from 'react-i18next';
import axios, { AxiosPromise } from 'axios';
import { Help } from '@material-ui/icons';
import { DateTime } from 'luxon';

import { WeekProps, DayProps } from 'containers/About/WorkoutPlan/components/PlanDashboard/Plan.types';
import { API_ROOT } from 'utils/constants';
import AuthContext from 'contexts/AuthContext';
import styles from './PlanCard.styles';
import { ExerciseTable, PersonalSection } from './components';

const useStyles = makeStyles(styles);

interface Props {
  title: string;
  day: DayProps;
  dayId: string;
  refetch: () => AxiosPromise<WeekProps | undefined>;
}

const NOW = DateTime.fromMillis(Date.now()).toISODate();

const PlanCard: React.FC<Props> = ({ title, dayId, day, refetch }) => {
  const { t } = useTranslation();
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down('xs'));
  const { container, doneButton, help } = useStyles({ isMobile });
  const [isLoading, setLoading] = useState(false);
  const [date, setDate] = useState(day.isDone ? day.dateOfCompletion : NOW);
  const [note, setNote] = useState(day.note);
  const { user } = useContext(AuthContext);

  React.useEffect(() => {
    if (day.dateOfCompletion && day.isDone) setDate(day.dateOfCompletion);
    if (day.note) setNote(day.note);
  }, [day]);

  const onSubmit = async (e: FormEvent) => {
    e.preventDefault();
    try {
      setLoading(true);
      await axios.post(`${API_ROOT}/api/day-complete`, { userId: user?.uid, dayId, note, date });
      await refetch();
    } catch (error) {
      // eslint-disable-next-line no-alert
      alert(error.message); // TODO: use styled alerts
    } finally {
      setLoading(false);
    }
  };

  return (
    <Card className={container}>
      <Tooltip title={t('generator.help') as string}>
        <IconButton className={help}>
          <Help color="primary" />
        </IconButton>
      </Tooltip>
      <CardHeader title={title} titleTypographyProps={{ align: 'center' }} />
      <CardContent style={{ paddingTop: isMobile ? 0 : 32 }}>
        <ExerciseTable refetch={refetch} dayId={dayId} day={day} />
        <PersonalSection
          date={date}
          setDate={setDate}
          note={note}
          setNote={setNote}
          isDone={day.isDone}
        />
        <Button
          disabled={isLoading || day.isDone}
          endIcon={isLoading && <CircularProgress size={16} />}
          onClick={onSubmit}
          variant="contained"
          color="primary"
          className={doneButton}
        >{t('generator.exerciseFinished')}
        </Button>
      </CardContent>
    </Card>
  );
};

export default PlanCard;
