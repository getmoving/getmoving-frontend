import { createStyles } from '@material-ui/core/styles';
import colors from 'utils/colors';

const styles = () => createStyles({
  tableRoot: {
    borderBottom: '1px solid rgba(224, 224, 224, 1)',
  },
  blankCell: {
    padding: '16px',
  },
  contentCell: {
    borderBottom: 'unset',
  },

  actionButton: {
    borderRadius: 'unset',
    border: '1px solid #CCC',
    margin: '0px 1px',
  },
  playButton: {
    backgroundColor: colors.primary,
    '&:hover': {
      backgroundColor: 'rgb(178, 140, 0)',
    },
  },
  playIcon: {
    fontSize: '1rem',
    fill: 'white',
  },
});

export default styles;
