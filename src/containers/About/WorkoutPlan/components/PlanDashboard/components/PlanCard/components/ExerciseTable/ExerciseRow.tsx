import React from 'react';
import axios, { AxiosPromise } from 'axios';
import { TableRow, TableCell, IconButton, Box, Tooltip } from '@material-ui/core';
import { Add, Refresh, Remove, PlayArrow } from '@material-ui/icons';
import { makeStyles } from '@material-ui/core/styles';

import { ExerciseBlockProps, WeekProps, COUNTING_TYPE } from 'containers/About/WorkoutPlan/components/PlanDashboard/Plan.types';
import AuthContext from 'contexts/AuthContext';
import VideoContext from 'contexts/VideoContext';
import { API_ROOT } from 'utils/constants';

import styles from './ExerciseTable.styles';

interface Props {
  code: string;
  block: ExerciseBlockProps;
  dayId: string;
  refetch: () => AxiosPromise<WeekProps | undefined>;
  isDone: boolean;
}

const useStyles = makeStyles(styles);

const ExerciseRow: React.FC<Props> = ({ code, block, dayId, refetch, isDone }) => {
  const { user } = React.useContext(AuthContext);
  const [isLoading, setLoading] = React.useState(false);
  const { blankCell, contentCell, playButton, playIcon, actionButton } = useStyles();
  const { setUrl } = React.useContext(VideoContext);

  const regenerate = async (level: number) => {
    try {
      setLoading(true);
      await axios.post(`${API_ROOT}/api/regenerate`, {
        uid: user?.uid,
        block: code.toLowerCase(),
        level: level.toString(),
        dayId,
      });
      await refetch();
    } catch (error) {
      // eslint-disable-next-line no-alert
      alert(error.message);
    } finally {
      setLoading(false);
    }
  };

  return (
    <TableRow key={code}>
      <TableCell className={contentCell}>{code}</TableCell>
      <TableCell className={contentCell}>
        {block.exercise.name}
      </TableCell>
      <TableCell className={`${blankCell} ${contentCell}`}>
        <IconButton className={playButton} size="small" onClick={() => setUrl(block.exercise.video.url)}>
          <PlayArrow className={playIcon} />
        </IconButton>
      </TableCell>
      <TableCell className={contentCell}>
        {block.exercise.equipments.map((e) => e.name).join(', ') || '-'}
      </TableCell>
      <TableCell className={contentCell} align="center"> {`${block.sets} x ${block.reps} ${block.exercise.countingType === COUNTING_TYPE.REP ? 'ism.' : 'mp'}`} </TableCell>
      <TableCell className={contentCell} align="center">
        <Box style={{ margin: 'auto', display: 'flex' }}>
          <Tooltip title="Nehezítés">
            <div>
              <IconButton
                className={actionButton}
                disabled={isLoading || block.level === '9' || isDone}
                size="small"
                onClick={async () => regenerate(Number(block.level) + 1)}
              >
                <Add />
              </IconButton>
            </div>
          </Tooltip>
          <Tooltip title="Újragenerálás">
            <div>
              <IconButton
                className={actionButton}
                disabled={isLoading || isDone}
                size="small"
                onClick={async () => regenerate(Number(block.level))}
              >
                <Refresh />
              </IconButton>
            </div>
          </Tooltip>
          <Tooltip title="Könnyítés">
            <div>
              <IconButton
                className={actionButton}
                disabled={isLoading || block.level === '0' || isDone}
                size="small"
                onClick={async () => regenerate(Number(block.level) - 1)}
              >
                <Remove />
              </IconButton>
            </div>
          </Tooltip>
        </Box>
      </TableCell>
      <TableCell className={contentCell} align="center">{Number(block.level) + 1} </TableCell>
    </TableRow>
  );
};

export default ExerciseRow;
