import { createStyles } from '@material-ui/core/styles';

interface Props {
  isMobile?: boolean;
}

const styles = () => createStyles<string, Props>({
  container: {
    position: 'relative',
    margin: ({ isMobile }) => (isMobile ? 16 : '2em'),
    boxShadow: '-4px -4px 10px rgba(0, 0, 0, 0.05), 4px 4px 10px rgba(0, 0, 0, 0.05)',
  },
  doneButton: {
    width: 'calc(100% - 2em)',
    boxShadow: 'unset',
    textTransform: 'uppercase',
    margin: '1em',
  },
  help: {
    position: 'absolute',
    top: 8,
    right: 16,
  },
});

export default styles;
