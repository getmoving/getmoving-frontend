import React from 'react';
import { Box, Grid, TextField } from '@material-ui/core';

interface Props {
  note: string;
  setNote: (note: string) => void;
  date: string;
  setDate: (date: string) => void;
  isDone: boolean;
}

const PersonalSection: React.FC<Props> = ({ note, setNote, date, setDate, isDone }) => (
  <Box p="1em">
    <Grid container spacing={1}>
      <Grid item xs={12} md={5} lg={5}>
        <TextField
          disabled={isDone}
          style={{ width: '80%' }}
          type="date"
          value={date}
          onChange={(e) => setDate(e.target.value)}
          label="Edzés dátuma"
          InputLabelProps={{
            shrink: true,
          }}
        />
      </Grid>
      <Grid item xs={12} md={7} lg={7}>
        <TextField
          style={{ width: '100%' }}
          label="Megjegyzés"
          placeholder="Rövid megjegyzés hozzáadása..."
          value={note}
          disabled={isDone}
          onChange={(e) => setNote(e.target.value)}
          InputLabelProps={{
            shrink: true,
          }}
        />
      </Grid>
    </Grid>
  </Box>
);

export default PersonalSection;
