/* eslint-disable no-console */
import React from 'react';
import { AxiosPromise } from 'axios';
import { Table, TableHead, TableRow, TableCell, TableBody, IconButton, TableContainer, useMediaQuery, Box } from '@material-ui/core';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import { PlayArrow } from '@material-ui/icons';

import { BlockType, DayProps, WeekProps } from 'containers/About/WorkoutPlan/components/PlanDashboard/Plan.types';
import { VideosProps } from 'containers/About/PrivateVideos/PrivateVideos.types';
import VideoContext from 'contexts/VideoContext';
import styles from './ExerciseTable.styles';
import ExerciseRow from './ExerciseRow';
import MobileExerciseCard from './MobileExerciseCard';

const BLOCKS = ['A1', 'A2', 'A3', 'B1', 'B2', 'B3', 'C1', 'C2', 'C3'];

const useStyles = makeStyles(styles);

interface Props {
  day: DayProps;
  dayId: string;
  refetch: () => AxiosPromise<WeekProps | undefined>;
}

const ExerciseTable: React.FC<Props> = ({ day, dayId, refetch }) => {
  const { blankCell, tableRoot, contentCell, playButton, playIcon } = useStyles();
  const { setUrl } = React.useContext(VideoContext);
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down('xs'));

  if (isMobile) {
    return (
      <Box>
        {['a0', ...BLOCKS].map((b) => {
          const block = day.exercises[b.toLocaleLowerCase() as BlockType];
          return (
            <MobileExerciseCard
              key={b}
              code={b}
              block={block}
              dayId={dayId}
              refetch={refetch}
              isDone={day.isDone}
              warmupUrl={day.exercises.warmUp?.video.url}
            />
          );
        })}
      </Box>
    );
  }

  const renderWarmUp = (video: VideosProps) => (
    <TableRow>
      <TableCell className={contentCell} />
      <TableCell className={contentCell}>
        Bemelegítés
      </TableCell>
      <TableCell className={`${blankCell} ${contentCell}`}>
        <IconButton className={playButton} size="small" onClick={() => setUrl(video.url)}>
          <PlayArrow className={playIcon} />
        </IconButton>
      </TableCell>
      <TableCell className={contentCell}>
          -
      </TableCell>
      <TableCell className={contentCell} align="center">-</TableCell>
      <TableCell className={contentCell} align="center">
          -
      </TableCell>
      <TableCell className={contentCell} align="center">
          -
      </TableCell>
    </TableRow>
  );

  return (
    <TableContainer>
      <Table size="small" className={tableRoot}>
        <TableHead>
          <TableRow>
            <TableCell />
            <TableCell>Gyakorlat neve</TableCell>
            <TableCell>Videó</TableCell>
            <TableCell>Eszköz</TableCell>
            <TableCell align="center">Ismétlésszám</TableCell>
            <TableCell align="center" />
            <TableCell align="center">Szint</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {day.exercises.warmUp && renderWarmUp(day.exercises.warmUp.video)}
          {BLOCKS.map((b) => {
            const block = day.exercises[b.toLocaleLowerCase() as BlockType];
            if (!block) return null;
            return (
              <ExerciseRow
                key={b}
                code={b}
                block={block}
                dayId={dayId}
                refetch={refetch}
                isDone={day.isDone}
              />
            );
          })}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default ExerciseTable;
