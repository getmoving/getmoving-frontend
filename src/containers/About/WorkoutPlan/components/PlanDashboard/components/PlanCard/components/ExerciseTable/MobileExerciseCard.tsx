import React from 'react';
import axios, { AxiosPromise } from 'axios';
import { IconButton, Box, Tooltip, Grid, Typography, Divider } from '@material-ui/core';
import { Add, Refresh, Remove, PlayArrow } from '@material-ui/icons';
import { makeStyles } from '@material-ui/core/styles';

import { ExerciseBlockProps, WeekProps, COUNTING_TYPE } from 'containers/About/WorkoutPlan/components/PlanDashboard/Plan.types';
import AuthContext from 'contexts/AuthContext';
import VideoContext from 'contexts/VideoContext';
import { API_ROOT } from 'utils/constants';

import styles from './ExerciseTable.styles';

interface Props {
  code: string;
  block?: ExerciseBlockProps;
  dayId: string;
  refetch: () => AxiosPromise<WeekProps | undefined>;
  isDone: boolean;
  warmupUrl?: string;
}

const useStyles = makeStyles(styles);

const MobileExerciseCard: React.FC<Props> = ({
  code, block, dayId, refetch, isDone, warmupUrl,
}) => {
  const { user } = React.useContext(AuthContext);
  const [isLoading, setLoading] = React.useState(false);
  const { playButton, playIcon, actionButton } = useStyles();
  const { setUrl } = React.useContext(VideoContext);

  const regenerate = async (level: number) => {
    try {
      setLoading(true);
      await axios.post(`${API_ROOT}/api/regenerate`, {
        uid: user?.uid,
        block: code.toLowerCase(),
        level: level.toString(),
        dayId,
      });
      await refetch();
    } catch (error) {
      // eslint-disable-next-line no-alert
      alert(error.message);
    } finally {
      setLoading(false);
    }
  };

  if (!block && code === 'a0' && warmupUrl) {
    return (
      <Box key={code}>
        <Divider style={{ backgroundColor: 'black' }} />
        <Box my={2}>
          <Grid container alignItems="center">
            <Grid item xs={5}>
              <Typography />
            </Grid>
            <Grid item xs={7}>
              <IconButton className={playButton} size="medium" onClick={() => setUrl(warmupUrl)}>
                <PlayArrow className={playIcon} />
              </IconButton>
            </Grid>
          </Grid>
        </Box>

        <Box my={2}>
          <Grid container alignItems="center">
            <Grid item xs={5}>
              <Typography>Gyakorlat:</Typography>
            </Grid>
            <Grid item xs={7}>
              <Typography>Bemelegítés</Typography>
            </Grid>
          </Grid>
        </Box>

        <Box my={2}>
          <Grid container alignItems="center">
            <Grid item xs={5}>
              <Typography>Eszköz:</Typography>
            </Grid>
            <Grid item xs={7}>
              <Typography>-</Typography>
            </Grid>
          </Grid>
        </Box>

        <Box my={2}>
          <Grid container alignItems="center">
            <Grid item xs={5}>
              <Typography>Ismétlésszám:</Typography>
            </Grid>
            <Grid item xs={7}>
              <Typography>-</Typography>
            </Grid>
          </Grid>
        </Box>

        <Box my={2}>
          <Grid container alignItems="center">
            <Grid item xs={5}>
              <Typography>Nehezítés:</Typography>
            </Grid>
            <Typography>-</Typography>
          </Grid>
        </Box>
      </Box>
    );
  }

  if (!block) return null;

  return (
    <Box key={code}>
      <Divider style={{ backgroundColor: 'black' }} />
      <Box my={2}>
        <Grid container alignItems="center">
          <Grid item xs={5}>
            <Typography>{code}</Typography>
          </Grid>
          <Grid item xs={7}>
            <IconButton className={playButton} size="medium" onClick={() => setUrl(block.exercise.video.url)}>
              <PlayArrow className={playIcon} />
            </IconButton>
          </Grid>
        </Grid>
      </Box>

      <Box my={2}>
        <Grid container alignItems="center">
          <Grid item xs={5}>
            <Typography>Gyakorlat:</Typography>
          </Grid>
          <Grid item xs={7}>
            <Typography>{block.exercise.name}</Typography>
          </Grid>
        </Grid>
      </Box>

      <Box my={2}>
        <Grid container alignItems="center">
          <Grid item xs={5}>
            <Typography>Eszköz:</Typography>
          </Grid>
          <Grid item xs={7}>
            <Typography>
              {block.exercise.equipments.map((e) => e.name).join(', ') || '-'}
            </Typography>
          </Grid>
        </Grid>
      </Box>

      <Box my={2}>
        <Grid container alignItems="center">
          <Grid item xs={5}>
            <Typography>Ismétlésszám:</Typography>
          </Grid>
          <Grid item xs={7}>
            <Typography>
              {`${block.sets} x ${block.reps} ${block.exercise.countingType === COUNTING_TYPE.REP ? 'ism.' : 'mp'}`}
            </Typography>
          </Grid>
        </Grid>
      </Box>

      <Box my={2}>
        <Grid container alignItems="center">
          <Grid item xs={5}>
            <Typography>Nehezítés:</Typography>
          </Grid>
          <Grid item xs={7}>
            <Box style={{ margin: 'auto', display: 'flex' }}>
              <Tooltip title="Nehezítés">
                <div>
                  <IconButton
                    className={actionButton}
                    disabled={isLoading || block.level === '9' || isDone}
                    size="small"
                    onClick={async () => regenerate(Number(block.level) + 1)}
                  >
                    <Add />
                  </IconButton>
                </div>
              </Tooltip>
              <Tooltip title="Újragenerálás">
                <div>
                  <IconButton
                    className={actionButton}
                    disabled={isLoading || isDone}
                    size="small"
                    onClick={async () => regenerate(Number(block.level))}
                  >
                    <Refresh />
                  </IconButton>
                </div>
              </Tooltip>
              <Tooltip title="Könnyítés">
                <div>
                  <IconButton
                    className={actionButton}
                    disabled={isLoading || block.level === '0' || isDone}
                    size="small"
                    onClick={async () => regenerate(Number(block.level) - 1)}
                  >
                    <Remove />
                  </IconButton>
                </div>
              </Tooltip>
            </Box>
          </Grid>
        </Grid>
      </Box>
    </Box>
  );
};

export default MobileExerciseCard;
