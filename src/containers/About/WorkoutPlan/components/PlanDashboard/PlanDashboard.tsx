import React, { useContext, useEffect, useState } from 'react';
import { Box, Button, CircularProgress, Typography } from '@material-ui/core';
import axios from 'axios';

import AuthContext from 'contexts/AuthContext';
import { API_ROOT } from 'utils/constants';
import { Alert } from 'components';
import PlanCard from './components';
import { DayNumberType } from './Plan.types';
import usePlanDashboard from './usePlanDashboard';

const DAYS = ['A', 'B', 'C', 'D', 'E'];

const daysSince = (date: number) => {
  const now = new Date();
  now.setHours(0, 0, 0, 0);
  const timeNow = now.getTime();
  const oneDay = 1000 * 60 * 60 * 24;
  const diffInTime = timeNow - date;
  const diffInDays = Math.round(diffInTime / oneDay);
  return diffInDays;
};

const PlanDashboard: React.FC = () => {
  const { currentWeek: week, refetch, refetchUserNode } = usePlanDashboard();
  const { user } = useContext(AuthContext);
  const [isAlertOpen, setAlertOpen] = useState(false);
  const [isLoading, setLoading] = useState(false);

  useEffect(() => {
    const generate = async () => {
      await axios.post(`${API_ROOT}/api/generate-week`, { uid: user?.uid });
      refetch();
    };
    if (week?.isDone && daysSince(week?.startDate) >= 7) {
      generate();
    }
  }, [week?.isDone]);

  const resetPlan = async () => {
    setLoading(true);
    if (user?.uid) {
      await axios.post(`${API_ROOT}/api/reset-plan`, { userId: user.uid });
      await refetchUserNode();
      await refetch();
    }
    setAlertOpen(false);
    setLoading(false);
  };

  if (!week) return <Box pt={13} ml={3}><CircularProgress /></Box>;
  return (
    <Box pt={14} pb={3} mx="auto" maxWidth="1240px">
      {DAYS.map((dayLetter, i) => {
        const day = week[`day${i}` as DayNumberType];
        if (!day) return null;
        return (
          <PlanCard
            key={dayLetter}
            dayId={`day${i}`}
            title={`"${dayLetter}" nap`}
            day={day}
            refetch={refetch}
          />
        );
      })}
      <Box mt={2} textAlign="center">
        <Button disabled={isLoading} onClick={() => setAlertOpen(true)} variant="contained" color="primary">Újragenerálás</Button>
        <Box m={2} />
        <Typography>Az újragenerálással törlődik a jelenlegi edzésterved!</Typography>
      </Box>
      <Alert
        isDisabled={isLoading}
        isConfirm
        ok={resetPlan}
        close={() => setAlertOpen(false)}
        open={isAlertOpen}
        title="Újragenerálás"
        description="Az újragenerálással törlődik a jelenlegi edzésterved! Biztos szeretnéd törölni?"
      />
    </Box>
  );
};

export default PlanDashboard;
