import { VideosProps, CategoriesProps } from 'containers/About/PrivateVideos/PrivateVideos.types';

export enum COUNTING_TYPE {
  REP = 'REPITITON',
  DUR = 'DURATION',
}

export interface EquipmentsProps {
  id: string;
  name: string;
  imageUrl: string;
}

export interface ExercisesProps {
  id: string;
  name: string;
  videoId: string;
  video: VideosProps;
  categoryIds: string[];
  categories: CategoriesProps[];
  count: number;
  countingType: COUNTING_TYPE;
  equipmentIds: string[];
  equipments: EquipmentsProps[];
}

export interface ExerciseBlockProps {
  exerciseId: string;
  exercise: ExercisesProps;
  reps: number;
  sets: number;
  level: string;
}

export interface WarmUpProps {
  videoId: string;
  video: VideosProps;
}

export interface BlockProps {
  warmUp?: WarmUpProps;
  a1?: ExerciseBlockProps;
  a2?: ExerciseBlockProps;
  a3?: ExerciseBlockProps;
  b1?: ExerciseBlockProps;
  b2?: ExerciseBlockProps;
  b3?: ExerciseBlockProps;
  c1?: ExerciseBlockProps;
  c2?: ExerciseBlockProps;
  c3?: ExerciseBlockProps;
}

export interface DayProps {
  isDone: boolean;
  dateOfCompletion: string;
  note: string;
  exercises: BlockProps;
}

export interface WeekProps {
  isDone: boolean;
  startDate: number;
  day0?: DayProps;
  day1?: DayProps;
  day2?: DayProps;
  day3?: DayProps;
  day4?: DayProps;
}

export type DayNumberType = 'day0' | 'day1' | 'day2' | 'day3' | 'day4';
export type BlockType = 'a1' | 'a2' | 'a3' | 'b1' | 'b2' | 'b3' | 'c1' | 'c2' | 'c3';
