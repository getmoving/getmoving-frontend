import React from 'react';
import { Box, Button, Grid, Typography, CircularProgress } from '@material-ui/core';
import { HighlightedCard } from './components';
import usePlanSetup from './usePlanSetup';
import { questionArrayType } from './PlanSetup.helpers';

const PlanSetup = () => {
  const {
    borderedBoxContainer,
    title,
    givePlanButton,
    t,
    questionArray,
    dispatch,
    onSubmit,
    isLoading,
  } = usePlanSetup();
  return (
    <Box
      pt={14}
      pb={3}
      ml="auto"
      mr="auto"
      maxWidth="1240px"
    >
      <Box className={borderedBoxContainer}>
        <Typography className={title}>{t('generator.title')}</Typography>
        <Box mt="2em" mb="2em">
          <Grid container spacing={5} justifyContent="center">
            {(questionArray as questionArrayType[]).map((obj, index) => (
              <Grid item xs={12} md={9} lg={9} key={obj.title}>
                <HighlightedCard {...obj} index={index} dispatch={dispatch} />
              </Grid>
            ))}
            <Grid item xs={12} md={9} lg={9}>
              <Button
                disabled={isLoading}
                endIcon={isLoading && <CircularProgress size={16} />}
                color="primary"
                variant="contained"
                className={givePlanButton}
                onClick={onSubmit}
              >{t('generator.givePlanButton')}
              </Button>
            </Grid>
          </Grid>
        </Box>
      </Box>
    </Box>
  );
};

export default PlanSetup;
