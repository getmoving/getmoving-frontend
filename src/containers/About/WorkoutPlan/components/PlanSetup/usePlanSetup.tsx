import AuthContext from 'contexts/AuthContext';
import axios from 'axios';
import { useTranslation } from 'react-i18next';
import { makeStyles } from '@material-ui/core/styles';
import React, { useContext, useEffect, useReducer, useMemo, useState } from 'react';
import { API_ROOT } from 'utils/constants';
import styles from './PlanSetup.styles';
import { EquipmentSwitch } from './components';
import { initialState, reducer, optionProps, DataProps, fitnessLevelArray } from './PlanSetup.helpers';

const useStyles = makeStyles(styles);

export default function usePlanSetup() {
  const { title, borderedBoxContainer, givePlanButton } = useStyles();
  const [equipmentOptions, setEquipmentOptions] = useState<optionProps[]>([]);
  const [isLoading, setIsLoading] = useState(false);
  const [atGym, setAtGym] = useState(false);
  const [state, dispatch] = useReducer(reducer, initialState);
  const { user, refetch } = useContext(AuthContext);
  const { t } = useTranslation();

  const getEquipments = async () => {
    const arr = await axios.get(`${API_ROOT}/api/get-equipments`);
    const formatted = arr.data.map(({ id, name, imageUrl }: DataProps) => ({
      label: name,
      value: id,
      icon: imageUrl,
    }));
    setEquipmentOptions(formatted);
  };

  useEffect(() => {
    getEquipments();
    return () => setEquipmentOptions([]);
  }, []);

  const onSubmit = async () => {
    try {
      setIsLoading(true);
      await axios.post(`${API_ROOT}/api/add-generator-node`, {
        ...state,
        uid: user?.uid,
        atGym,
        equipment: atGym ? [] : state.equipment,
        weekNumber: Number(state.weekNumber),
      });
      await axios.post(`${API_ROOT}/api/generate-week`, { uid: user?.uid });
      await refetch();
    } catch (error) {
      // eslint-disable-next-line no-alert
      alert(error.message); // TODO: use styled alerts
    }
  };

  const questionArray = useMemo(() => ([
    {
      title: 'exerciseNumber',
      value: state.exerciseNumber,
      options: [
        { label: '2', value: '2' },
        { label: '3', value: '3' },
        { label: '4', value: '4' },
        { label: '5', value: '5' },
      ],
    },
    {
      title: 'exerciseLength',
      value: state.exerciseLength,
      options: [
        { label: '45', value: '45' },
        { label: '60', value: '60' },
        { label: '75', value: '75' },
      ],
    },
    {
      title: 'fitness',
      value: state.fitness,
      options: fitnessLevelArray,
    },
    {
      title: 'equipment',
      value: state.equipment,
      multi: true,
      options: equipmentOptions,
      disabled: atGym,
      additionalComponent: () => <EquipmentSwitch atGym={atGym} setAtGym={setAtGym} />,
    },
  ]), [state, equipmentOptions, atGym, setAtGym]);

  return {
    borderedBoxContainer, title, givePlanButton, t, questionArray, dispatch, onSubmit, isLoading,
  };
}
