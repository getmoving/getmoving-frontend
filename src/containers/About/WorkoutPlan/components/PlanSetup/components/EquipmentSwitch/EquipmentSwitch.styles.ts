import { createStyles } from '@material-ui/core/styles';

const styles = () => createStyles({
  gridContainer: {
    marginLeft: -11,
    padding: '2em',
  },
  buttonLeft: {
    width: '100%',
    borderRadius: '5px 0px 0px 5px',
    boxShadow: 'unset',
  },
  buttonRight: {
    width: '100%',
    borderRadius: '0px 5px 5px 0px',
    boxShadow: 'unset',
  },
});

export default styles;
