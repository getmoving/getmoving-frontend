import React from 'react';
import { Grid, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

import styles from './EquipmentSwitch.styles';

const useStyles = makeStyles(styles);

interface Props {
  atGym: boolean;
  setAtGym: React.Dispatch<React.SetStateAction<boolean>>;
}

const EquipmentSwitch: React.FC<Props> = ({ atGym, setAtGym }) => {
  const { gridContainer, buttonLeft, buttonRight } = useStyles();
  return (
    <Grid container justifyContent="center" className={gridContainer}>
      <Grid item xs={12} md={5} lg={5}>
        <Button
          className={buttonLeft}
          color={atGym ? 'primary' : 'secondary'}
          variant="contained"
          onClick={() => setAtGym(true)}
        >
            Konditermi edzés
        </Button>
      </Grid>
      <Grid item xs={12} md={5} lg={5}>
        <Button
          className={buttonRight}
          color={atGym ? 'secondary' : 'primary'}
          variant="contained"
          onClick={() => setAtGym(false)}
        >
           Otthoni edzés
        </Button>
      </Grid>
    </Grid>
  );
};

export default EquipmentSwitch;
