import { createStyles } from '@material-ui/core/styles';

const styles = () => createStyles({
  formControlLabelRoot: {
    border: '1px solid rgba(234, 234, 234, 1)',
    width: '100%',
    borderRadius: 3,
  },
  label: {
    width: '100%',
  },

  labelImg: {
    float: 'right',
    padding: '0.5em 2em',
  },
});

export default styles;
