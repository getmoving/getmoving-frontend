import React from 'react';
import { Checkbox, FormControlLabel, Grid } from '@material-ui/core';
import { ActionInterface, optionProps, stateType } from 'containers/About/WorkoutPlan/components/PlanSetup/PlanSetup.helpers';

interface SingularCheckboxGroupProps {
  title: keyof stateType;
  value: string | string[];
  options: optionProps[];
  dispatch: React.Dispatch<ActionInterface>;
  disabled?: boolean;
}

const SingularCheckboxGroup:
  React.FC<SingularCheckboxGroupProps> = ({
    options,
    value,
    dispatch,
    title,
    disabled,
  }) => (
    <Grid container spacing={1}>
      {options.map((option) => (
        <Grid item xs={12} md={6} lg={6} key={option.label}>
          <FormControlLabel
            disabled={disabled}
            style={{ border: '1px solid rgba(234, 234, 234, 1)', width: '100%', borderRadius: 3 }}
            control={(
              <Checkbox
                checked={value === option.value}
                color="primary"
                value={String(option.value)}
                onChange={(e) => dispatch({ type: title, data: e.target.value })}
              />
            )}
            label={option.label}
          />
        </Grid>
      ))}
    </Grid>
  );

export default SingularCheckboxGroup;
