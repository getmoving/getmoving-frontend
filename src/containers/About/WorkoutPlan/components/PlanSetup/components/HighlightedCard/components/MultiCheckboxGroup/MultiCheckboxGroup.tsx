import React from 'react';
import { Checkbox, FormControlLabel, Grid } from '@material-ui/core';
import { ActionInterface, optionProps, stateType } from 'containers/About/WorkoutPlan/components/PlanSetup/PlanSetup.helpers';
import { makeStyles } from '@material-ui/core/styles';

import styles from './MultiCheckboxGroup.styles';

const useStyles = makeStyles(styles);

interface MultiCheckboxGroup {
  title: keyof stateType;
  value: string | string[];
  options: optionProps[];
  dispatch: React.Dispatch<ActionInterface>;
  disabled?: boolean;
}

const MultiCheckboxGroup:
  React.FC<MultiCheckboxGroup> = ({
    options,
    value,
    dispatch,
    title,
    disabled,
  }) => {
    const { formControlLabelRoot, label, labelImg } = useStyles();
    return (
      <Grid container spacing={1}>
        {options.map((option) => (
          <Grid item xs={12} md={6} lg={6} key={option.label}>
            <FormControlLabel
              disabled={disabled}
              classes={{
                root: formControlLabelRoot,
                label,
              }}
              control={(
                <Checkbox
                  checked={value.includes(option.value as string)}
                  color="primary"
                  value={String(option.value)}
                  onChange={(e) => dispatch({ type: title, data: e.target.value })}

                />
              )}
              label={(
                <Grid container spacing={1} alignContent="center" justifyContent="space-between">
                  <Grid item style={{ margin: 'auto 0' }} xs={6} md={6} lg={6}>
                    {option.label}
                  </Grid>
                  <Grid item xs={6} md={6} lg={6}>
                    <img className={labelImg} src={option.icon} alt={option.label} />
                  </Grid>
                </Grid>
              )}
            />
          </Grid>
        ))}
      </Grid>
    );
  };

export default MultiCheckboxGroup;
