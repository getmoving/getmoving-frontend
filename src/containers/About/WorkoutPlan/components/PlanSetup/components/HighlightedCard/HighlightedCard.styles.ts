import { createStyles } from '@material-ui/core/styles';
import colors from 'utils/colors';

const styles = () => createStyles({
  cardRoot: {
    boxShadow: '-4px -4px 20px rgba(0, 0, 0, 0.05), 4px 4px 20px rgba(0, 0, 0, 0.05)',
  },
  avatar: {
    backgroundColor: colors.primary,
    fontWeight: 600,
  },
  cardHeaderRoot: {
    padding: '2em',
  },
  cardHeaderTitle: {
    fontSize: '1.3em',
    fontWeight: 600,
  },
  cardContentRoot: {
    padding: '0em 6.5em',
  },
});

export default styles;
