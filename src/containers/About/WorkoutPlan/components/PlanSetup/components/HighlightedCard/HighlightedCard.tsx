import { Avatar, Card, CardHeader, CardContent } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import React from 'react';
import { ActionInterface, optionProps, stateType } from 'containers/About/WorkoutPlan/components/PlanSetup/PlanSetup.helpers';
import { useTranslation } from 'react-i18next';
import styles from './HighlightedCard.styles';
import { SingularCheckboxGroup, MultiCheckboxGroup } from './components';

const useStyles = makeStyles(styles);

interface HighlightedCardProps {
  title: keyof stateType;
  value: string | string[];
  options: optionProps[];
  dispatch: React.Dispatch<ActionInterface>;
  multi?: boolean;
  index: number;
  disabled?: boolean;
  additionalComponent?: () => JSX.Element;
}

const HighlightedCard: React.FC<HighlightedCardProps> = ({
  title,
  index,
  value,
  options,
  dispatch,
  multi,
  disabled,
  additionalComponent,
}) => {
  const { cardRoot, cardHeaderRoot, cardHeaderTitle, cardContentRoot, avatar } = useStyles();
  const { t } = useTranslation();
  return (
    <Card className={cardRoot}>
      <CardHeader
        title={t(String(`generator.${title}`))}
        classes={{
          root: cardHeaderRoot,
          title: cardHeaderTitle }}
        avatar={(
          <Avatar aria-label="recipe" className={avatar}>
            {index + 1}
          </Avatar>
        )}
      />
      <CardContent className={cardContentRoot}>
        {additionalComponent && additionalComponent()}
        {multi
          ? (
            <MultiCheckboxGroup
              title={title}
              value={value}
              options={options}
              dispatch={dispatch}
              disabled={disabled}
            />
          )
          : (
            <SingularCheckboxGroup
              title={title}
              value={value}
              options={options}
              dispatch={dispatch}
              disabled={disabled}
            />
          )}
      </CardContent>
    </Card>
  );
};

export default React.memo(HighlightedCard);
