export const initialState = {
  exerciseNumber: '2',
  exerciseLength: '45',
  fitness: '1',
  equipment: [] as string[],
  weekNumber: '0',
};

export type stateType = typeof initialState;

export interface optionProps {
  label: string;
  value: string | string[];
  icon?: string;
}

export interface questionArrayType {
  title: keyof stateType;
  value: string | string[];
  options: optionProps[];
  multi?: boolean;
  additionalComponent?: () => JSX.Element;
}

export interface ActionInterface {
  type: 'exerciseNumber' | 'exerciseLength' | 'fitness' | 'weekNumber' | 'equipment';
  data: string;
}

export interface DataProps {
  name: string;
  id: string;
  imageUrl: string;
}

export const reducer = (s: stateType, a: ActionInterface) => {
  switch (a.type) {
    case 'exerciseNumber':
      return { ...s, exerciseNumber: a.data };
    case 'exerciseLength':
      return { ...s, exerciseLength: a.data };
    case 'fitness':
      return { ...s, fitness: a.data };
    case 'weekNumber':
      return { ...s, weekNumber: a.data };
    case 'equipment': {
      const tmp = [...s.equipment];
      if (tmp.includes(a.data as string)) {
        tmp.splice(tmp.findIndex((el) => el === a.data), 1);
      } else {
        tmp.push(a.data as string);
      }
      return { ...s, equipment: tmp };
    }
    default:
      throw new Error();
  }
};

export const fitnessLevelArray = Array(10).fill('').map((_, i) => ({ label: String(i + 1), value: String(i + 1) }));
