import { createStyles } from '@material-ui/core/styles';

const styles = () => createStyles({
  title: {
    fontSize: '1.3em',
    fontWeight: 600,
    textAlign: 'center',
    maxWidth: '60%',
    margin: '50px auto',
    color: '#333333',
  },
  borderedBoxContainer: {
    border: '1px solid #E0E0E0',
    boxSizing: 'border-box',
    borderRadius: '5px',
  },
  givePlanButton: {
    width: '100%',
    textTransform: 'uppercase',
  },
});

export default styles;
