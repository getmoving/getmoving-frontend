import React from 'react';
import AuthContext from 'contexts/AuthContext';
import { Box, CircularProgress } from '@material-ui/core';

import RouteContext from 'contexts/RouteContext';
import { withVideoContext } from 'contexts/VideoContext';
import { PlanSetup, PlanDashboard } from './components';

const WorkoutPlan = () => {
  const { userNode, isLoading } = React.useContext(AuthContext);
  const { setVariant, setComponentType } = React.useContext(RouteContext);

  React.useEffect(() => {
    setVariant('black');
    setComponentType('public');
  }, []);

  if (isLoading) {
    return (
      <Box minHeight="100vh" paddingTop="5em">
        <CircularProgress />
      </Box>
    );
  }

  return userNode?.generator ? <PlanDashboard /> : <PlanSetup />;
};

export default withVideoContext(WorkoutPlan);
