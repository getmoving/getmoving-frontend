import React, { useContext, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import { Box, CircularProgress, Typography, Grid } from '@material-ui/core';
import RouteContext from 'contexts/RouteContext';
import useAxios from 'axios-hooks';
import axios from 'axios';

import { API_ROOT } from 'utils/constants';
import { UnderlineWrapper } from 'components';
import colors from 'utils/colors';
import styles from './Recipes.styles';
import { RecipeProps } from './Recipes.types';
import LevelAndDuration from './components/LevelAndDuration';
import Ingredients from './components/Ingredients';
import RecipeCard from './components/RecipeCard';

const useStyles = makeStyles(styles);

const RecipeDetails: React.FC = () => {
  const { setVariant, setComponentType } = useContext(RouteContext);
  const { id } = useParams<{id: string}>();
  const [{ data = undefined, loading }] = useAxios<RecipeProps>(`${API_ROOT}/api/get-recipe-by-id/${id}`);
  const { recipeImg, title, relevantRecipesTitle } = useStyles({ imageUrl: data?.imageUrl });
  const [relevantRecipes, setRelevantRecipes] = React.useState<RecipeProps[]>();

  useEffect(() => {
    const getVideos = async () => {
      const res = await axios.get<RecipeProps[]>(`${API_ROOT}/api/get-relevant-recipes/${id}`);
      setRelevantRecipes(res.data);
    };
    getVideos();
  }, [id]);

  useEffect(() => {
    setVariant('black');
    setComponentType('public');
  }, []);

  if (loading || !data) return <CircularProgress />;
  return (
    <Box pt={10} mb={4}>
      <Box className={recipeImg} />
      <Box mt={4} maxWidth={1030} mx="auto">
        <UnderlineWrapper color="black">
          <Typography style={{ color: 'black' }} className={title} variant="h2">{data.title}</Typography>
        </UnderlineWrapper>
        <Box mt={3} mb={6}>
          <LevelAndDuration level={data.level} duration={data.duration} />
        </Box>
        <Grid container spacing={4}>
          <Grid item xs={12} md={6}>
            <Ingredients data={data.block} />
            {data?.imageUrl && <img style={{ width: '100%', marginTop: 32 }} alt="recipe-small-img" src={data.imageUrl} />}
          </Grid>
          <Grid item xs={12} md={6}>
            <Box>
              <Typography style={{ fontWeight: 700, fontSize: '18', color: colors.grey6D }}>{data.details}</Typography>
              <Typography style={{ color: colors.grey66, whiteSpace: 'pre-wrap', marginTop: 32 }}>{data.body}</Typography>
            </Box>
          </Grid>
        </Grid>
      </Box>
      {relevantRecipes && relevantRecipes.length ? (
        <Box maxWidth={1240} mx="auto">
          <Box my={5}>
            <UnderlineWrapper>
              <Typography className={relevantRecipesTitle}>Kapcsolódó receptek</Typography>
            </UnderlineWrapper>
          </Box>
          <Grid container spacing={4}>
            {relevantRecipes.map((d) => <RecipeCard key={d.id} data={d} />)}
          </Grid>
        </Box>
      ) : <></>}
    </Box>
  );
};

export default RecipeDetails;
