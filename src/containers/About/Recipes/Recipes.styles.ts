import { createStyles } from '@material-ui/core/styles';
import recipesbg from 'assets/recipesbg.png';

interface Props {
  imageUrl?: string;
}

const styles = () => createStyles<string, Props>({
  img: {
    display: 'flex',
    backgroundImage: `url(${recipesbg})`,
    width: '100%',
    height: 300,
    backgroundSize: 'cover',
    backgroundPosition: 'center',
  },
  recipeImg: {
    display: 'flex',
    backgroundImage: ({ imageUrl }) => `url(${imageUrl})`,
    width: '100%',
    height: 'calc(100vh - 256px)',
    backgroundSize: 'cover',
    backgroundPosition: 'center',
  },
  title: {
    fontSize: 50,
    fontWeight: 900,
    color: 'white',
  },
  relevantRecipesTitle: {
    textTransform: 'uppercase',
    fontWeight: 500,
    fontSize: 30,
  },
});

export default styles;
