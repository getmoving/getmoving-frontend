import React, { useContext, useEffect } from 'react';
import { Box, Typography, Grid, CircularProgress } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { useTranslation } from 'react-i18next';

import RouteContext from 'contexts/RouteContext';
import { UnderlineWrapper } from 'components';
import styles from './Recipes.styles';
import useRecipes from './useRecipes';
import RecipeCard from './components/RecipeCard';

const useStyles = makeStyles(styles);

const Recipes: React.FC = () => {
  const { t } = useTranslation();
  const { img, title } = useStyles({ imageUrl: '' });
  const { setVariant, setComponentType } = useContext(RouteContext);
  const { data, loading, error } = useRecipes();

  useEffect(() => {
    setVariant('black');
    setComponentType('public');
  }, []);

  if (loading || error) return <CircularProgress />;

  return (
    <Box pt={10} mb={4}>
      <Box className={img}>
        <UnderlineWrapper centered color="white" margin="auto">
          <Typography className={title}>{t('recipes.title')}</Typography>
        </UnderlineWrapper>
      </Box>
      <Box mt={4} mx="auto" maxWidth={1240}>
        <Grid container spacing={4}>
          {data?.map((d) => <RecipeCard key={d.id} data={d} />)}
        </Grid>
      </Box>
    </Box>
  );
};

export default Recipes;
