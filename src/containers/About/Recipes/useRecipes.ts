import useGetRequest from 'hooks/useGetRequest';
import { API_ROOT } from 'utils/constants';
import { RecipeProps } from './Recipes.types';

const useRecipes = () => {
  const { data, loading, error } = useGetRequest<RecipeProps[]>(`${API_ROOT}/admin/get-recipes`);

  return { data, loading, error };
};

export default useRecipes;
