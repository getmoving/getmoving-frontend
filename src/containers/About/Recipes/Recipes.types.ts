export enum RECIPE_LEVELS {
  EASY = 1,
  MEDIUM = 2,
  HARD = 3,
}

export interface IngredientsType {
    title: string;
    ingredients: string[],
}

export type RecipeProps = {
  id: string;
  body: string;
  details: string;
  title: string;
  level: RECIPE_LEVELS;
  duration: number;
  block: IngredientsType[];
  imageUrl: string;
  thumbnailUrl: string;
  categoryIds: string[];
}
