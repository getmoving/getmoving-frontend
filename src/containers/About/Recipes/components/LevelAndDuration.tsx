import React from 'react';
import { useTranslation } from 'react-i18next';
import { Box, Typography } from '@material-ui/core';
import { Schedule } from '@material-ui/icons';
import { RECIPE_LEVELS } from 'containers/About/Recipes/Recipes.types';

interface Props {
  level: RECIPE_LEVELS;
  duration: number;
}

const LevelAndDuration: React.FC<Props> = ({ level, duration }) => {
  const { t } = useTranslation();

  return (
    <Box display="flex" alignItems="center">
      <Typography variant="body2" color="textSecondary" component="p">
        {t('recipes.levels', { returnObjects: true })[level - 1]}
      </Typography>
      <Box display="flex" borderLeft="1px solid grey" ml={2} height={22} alignItems="center">
        <Box mx={2}>
          <Schedule style={{ color: '#757575' }} />
        </Box>
        <Typography variant="body2" color="textSecondary" component="p">
          {t('recipes.duration', { duration })}
        </Typography>
      </Box>
    </Box>
  );
};

export default LevelAndDuration;
