import React, { useContext } from 'react';
import { useHistory } from 'react-router-dom';
import { Card, CardMedia, Typography, CardContent, Box, CardActions, Button, Grid, Collapse, IconButton } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { ChevronRight, Close } from '@material-ui/icons';
import { useTranslation } from 'react-i18next';

import { RecipeProps } from 'containers/About/Recipes/Recipes.types';
import AuthContext from 'contexts/AuthContext';
import { Alert } from '@material-ui/lab';
import LevelAndDuration from './LevelAndDuration';

interface Props {
  data: RecipeProps;
}

const useStyles = makeStyles({
  root: {
    maxWidth: 393,
  },
  media: {
    height: 220,
  },
  button: {
    width: 'initial',
    fontSize: 14,
    fontWeight: 'normal',
  },
  label: {
    paddingLeft: 8,
    paddingRight: 8,
  },
  title: {
    fontSize: 18,
  },
});

const RecipeCard: React.FC<Props> = ({ data }) => {
  const { t } = useTranslation();
  const classes = useStyles();
  const history = useHistory();
  const { isActive } = useContext(AuthContext);
  const [alertVisible, setAlertVisibility] = React.useState(false);

  return (
    <Grid item xs={12} sm={6} md={4}>
      <Card className={classes.root}>
        <CardMedia
          className={classes.media}
          image={data.thumbnailUrl || data.imageUrl}
          title="Recipe"
        />
        <CardContent>
          <Box mt={2} mb={4}>
            <Typography className={classes.title} gutterBottom variant="h5" component="h2">
              {data.title}
            </Typography>
          </Box>
          <LevelAndDuration level={data.level} duration={data.duration} />
        </CardContent>
        <CardActions style={{ display: 'block', textAlign: 'right' }}>
          <Button
            classes={{ label: classes.label }}
            className={classes.button}
            size="small"
            color="default"
            endIcon={<ChevronRight />}
            onClick={() => {
              if (isActive) {
                history.push(`/about/recipe-details/${data.id}`);
                window.scrollTo({ top: 0, behavior: 'smooth' });
              } else {
                setAlertVisibility(true);
              }
            }}
          >
            {t('recipes.readMore')}
          </Button>
        </CardActions>
        <Collapse in={alertVisible}>
          <Alert
            severity="error"
            action={(
              <IconButton
                aria-label="close"
                color="inherit"
                size="small"
                onClick={() => {
                  setAlertVisibility(false);
                }}
              >
                <Close fontSize="inherit" />
              </IconButton>
            )}
          >
            A folytatáshoz elő kell fizetned!
          </Alert>
        </Collapse>
      </Card>
    </Grid>
  );
};

export default RecipeCard;
