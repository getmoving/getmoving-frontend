import React from 'react';
import { useTranslation } from 'react-i18next';
import { Box, Typography } from '@material-ui/core';
import { IngredientsType } from 'containers/About/Recipes/Recipes.types';
import colors from 'utils/colors';

interface Props {
  data: IngredientsType[];
}

const Ingredients: React.FC<Props> = ({ data }) => {
  const { t } = useTranslation();

  return (
    <Box bgcolor={colors.greyF6} p={3}>
      <Typography style={{ color: colors.grey6D, fontWeight: 700, fontSize: '18px' }}>{t('recipes.ingredients')}</Typography>
      {data.map(({ title, ingredients }) => (
        <Box key={title} mt={4}>
          <Typography style={{ color: colors.grey6D, fontWeight: 600, fontSize: '16px' }}>{title}</Typography>
          {ingredients.map((i) => (
            <Typography style={{ color: colors.grey6D }} key={i}>{i}</Typography>
          ))}
        </Box>
      ))}
    </Box>
  );
};

export default Ingredients;
