import React, { useContext, useEffect } from 'react';
import { Box } from '@material-ui/core';
import RouteContext from 'contexts/RouteContext';
import { Contact } from 'containers/Landing';

const ContactPage: React.FC = () => {
  const { setVariant, setComponentType } = useContext(RouteContext);

  useEffect(() => {
    setVariant('black');
    setComponentType('public');
  }, []);

  return (
    <Box pt={10}>
      <Contact />
    </Box>
  );
};

export default ContactPage;
