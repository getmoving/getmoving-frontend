export interface VideosProps {
  id: string;
  title: string;
  description: string
  duration: number;
  date: string;
  url: string;
  categoryIds?: string[];
}

export interface CategoriesProps {
  id: string;
  name: string;
  description: string;
  imageUrl: string;
}

export interface CategoriesListProps {
  id: string;
  name: string;
  count: number;
}
