import { useState, useContext, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useTranslation } from 'react-i18next';
import useAxios from 'axios-hooks';
import axios from 'axios';

import { API_ROOT } from 'utils/constants';
import RouteContext from 'contexts/RouteContext';
import styles from './Videos.styles';
import { VideosProps, CategoriesListProps } from './PrivateVideos.types';

const useStyles = makeStyles(styles);
const LIMIT = 20;

const usePrivateVideos = () => {
  const { t } = useTranslation();
  const classes = useStyles();
  const [showLimitedVideos, setShowLimitedVideos] = useState(true);
  const [nextVideo, setNextVideo] = useState<string>();
  const [category, setCategory] = useState<CategoriesListProps | null>(null);
  const [searchText, setSearchText] = useState<string>('');
  const { setVariant, setComponentType } = useContext(RouteContext);
  const [videos, setVideos] = useState<VideosProps[]>([]);
  const [endOfVideos, setEndOfVideos] = useState(false);
  const [isLoading, setLoading] = useState(true);
  const [{ data: categories = [] }] = useAxios<CategoriesListProps[]>(`${API_ROOT}/api/get-categories`);

  useEffect(() => {
    setVariant('black');
    setComponentType('public');
  }, []);

  useEffect(() => {
    const getVideos = async () => {
      setLoading(true);
      const res = await axios.get<VideosProps[]>(
        `${API_ROOT}/api/get-videos/title/${nextVideo ? encodeURIComponent(nextVideo) : nextVideo}`,
      );
      setLoading(false);
      if (res.data.length < LIMIT) setEndOfVideos(true);
      setVideos([...videos, ...res.data]);
    };
    if (showLimitedVideos) getVideos();
  }, [nextVideo, showLimitedVideos]);

  const searchVideos = async () => {
    if (searchText || category) {
      setLoading(true);
      setShowLimitedVideos(false);
      const res = await axios.get<VideosProps[]>(`${API_ROOT}/api/search-videos`,
        { params: { searchText, categoryId: category?.id } });
      setLoading(false);
      setVideos(res.data);
    } else {
      setVideos([]);
      setShowLimitedVideos(true);
    }
  };

  const onSearch = () => searchVideos();

  useEffect(() => {
    searchVideos();
  }, [category]);

  return { videos,
    setNextVideo,
    endOfVideos,
    categories,
    isLoading,
    classes,
    t,
    searchVideos,
    showLimitedVideos,
    category,
    setCategory,
    searchText,
    setSearchText,
    onSearch,
  };
};

export default usePrivateVideos;
