import { createStyles } from '@material-ui/core/styles';
import videosbg from 'assets/videosbg.png';

const styles = () => createStyles({
  img: {
    display: 'flex',
    backgroundImage: `url(${videosbg})`,
    width: '100%',
    height: 300,
    backgroundSize: 'cover',
    backgroundPosition: 'center',
  },
  title: {
    fontSize: 50,
    fontWeight: 900,
    color: 'white',
    textAlign: 'center',
  },
  videoTitle: {
    textTransform: 'uppercase',
    fontWeight: 'bold',
    letterSpacing: 1.6,
  },
  player: {
    marginBottom: 6,
    cursor: 'pointer',
    '&:hover': {
      filter: 'drop-shadow(2px 4px 6px black)',
    },
  },
});

export default styles;
