import React from 'react';
import { Box, Typography, Button, Grid, CircularProgress, TextField, useMediaQuery } from '@material-ui/core';
import { useTheme } from '@material-ui/core/styles';
import { ExpandMore } from '@material-ui/icons';
import ReactPlayer from 'react-player';
import Autocomplete from '@material-ui/lab/Autocomplete';

import { UnderlineWrapper, SearchField } from 'components';
import VideoContext, { withVideoContext } from 'contexts/VideoContext';
import usePrivateVideos from './usePrivateVideos';
import { CategoriesListProps } from './PrivateVideos.types';

const Videos: React.FC = () => {
  const { setUrl } = React.useContext(VideoContext);
  const {
    t, videos, setNextVideo, endOfVideos, categories, isLoading, classes, setSearchText,
    showLimitedVideos, category, setCategory, onSearch, searchText,
  } = usePrivateVideos();
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down('xs'));

  return (
    <Box pt={10}>
      <Box className={classes.img}>
        <UnderlineWrapper centered color="white" margin="auto" bottom={40}>
          <Typography className={classes.title}>{t('videos.private.title')}</Typography>
          <Box position="relative" top={32}>
            <SearchField
              value={searchText}
              onPress={onSearch}
              disabled={isLoading}
              onChange={setSearchText}
              placeholder={t('videos.private.placeholder')}
            />
          </Box>
        </UnderlineWrapper>
      </Box>
      <Box mt={4} mx={isMobile ? 0 : 12}>
        <Autocomplete<CategoriesListProps>
          options={categories}
          disabled={isLoading}
          getOptionLabel={(option) => `${option.name} (${option.count})`}
          style={{ width: 400, marginBottom: 32 }}
          renderInput={(params) => <TextField {...params} label="Kategóriák" variant="standard" />}
          value={category}
          onChange={(_, newValue) => setCategory(newValue)}
        />
        <Grid container spacing={5}>
          {videos.map((v) => (
            <Grid key={v.id} item sm={12} md={6} lg={4}>
              <Box className={classes.player} onClick={() => setUrl(v.url)}>
                <ReactPlayer
                  style={{ pointerEvents: 'none' }}
                  url={v.url}
                  light
                  width={400}
                  height={222}
                />
              </Box>
              <Typography className={classes.videoTitle}>{v.title}</Typography>
              <Typography>
                {v.categoryIds?.map((id) => categories.find((c) => c.id === id)?.name)}
              </Typography>
            </Grid>
          ))}
        </Grid>
      </Box>
      <Box my={5} display="flex" flexDirection="column" alignItems="center">
        {showLimitedVideos && (
          <Button
            color="primary"
            variant="contained"
            disabled={endOfVideos || isLoading}
            onClick={() => setNextVideo(videos[videos.length - 1].title)}
            endIcon={isLoading ? <CircularProgress size={16} /> : <ExpandMore />}
          >
            {t('videos.private.loadMore')}
          </Button>
        )}
        <br />
        <Button onClick={() => window.scrollTo({ top: 0, behavior: 'smooth' })}>
          {t('videos.private.scrollToTop')}
        </Button>
      </Box>
    </Box>
  );
};

export default withVideoContext(Videos);
