/* eslint-disable react/no-danger */
import React, { useContext, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import { Box, CircularProgress, Typography, Grid } from '@material-ui/core';
import RouteContext from 'contexts/RouteContext';
import useAxios from 'axios-hooks';
import axios from 'axios';
import { API_ROOT } from 'utils/constants';
import { UnderlineWrapper } from 'components';
import colors from 'utils/colors';
import styles from './Blog.styles';
import { BlogProps, BlogCategory } from './Blog.types';
import DateAndCategory from './components/DateAndCategory';
import BlogCard from './components/BlogCard';

const useStyles = makeStyles(styles);

const BlogDetails: React.FC = () => {
  const { setVariant, setComponentType } = useContext(RouteContext);
  const { id } = useParams<{id: string}>();
  const [{ data = undefined, loading }] = useAxios<BlogProps>(`${API_ROOT}/api/get-blog-by-id/${id}`);
  const [{ data: categories = [] }] = useAxios<BlogCategory[]>(`${API_ROOT}/admin/get-blog-categories`);
  const {
    blogImg, title, relevantBlogsTitle, bodyImg, root,
  } = useStyles({ imageUrl: data?.coverUrl });
  const [relevantBlogs, setRelevantBlogs] = React.useState<BlogProps[]>();

  useEffect(() => {
    const getVideos = async () => {
      const res = await axios.get<BlogProps[]>(`${API_ROOT}/api/get-relevant-blogs/${id}`);
      setRelevantBlogs(res.data);
    };
    getVideos();
  }, [id]);

  useEffect(() => {
    setVariant('black');
    setComponentType('public');
  }, []);

  if (loading || !data) return <CircularProgress />;
  return (
    <Box pt={10} mb={4}>
      <Box className={blogImg} />
      <Box mt={4} maxWidth={1030} mx="auto" className={root}>
        <UnderlineWrapper color="black">
          <Typography style={{ color: 'black' }} className={title} variant="h2">{data.title}</Typography>
        </UnderlineWrapper>
        <Box mt={3} mb={6}>
          <DateAndCategory
            createdAt={data.createdAt}
            categories={categories.filter((c) => data.categoryIds.includes(c.id))}
          />
        </Box>
        <div style={{ fontFamily: 'Poppins', color: colors.grey66, whiteSpace: 'pre-wrap', marginTop: 32 }} dangerouslySetInnerHTML={{ __html: data.body1 }} />
        {data.image1Url && (
          <Box textAlign="center">
            <img src={data.image1Url} alt="blog1" className={bodyImg} />
          </Box>
        )}
        <div style={{ fontFamily: 'Poppins', color: colors.grey66, whiteSpace: 'pre-wrap', marginTop: 32 }} dangerouslySetInnerHTML={{ __html: data.body2 }} />
        <Box my={2}>
          {data.image2Url && (
            <Box textAlign="center">
              <img src={data.image2Url} alt="blog2" className={bodyImg} />
            </Box>
          )}
        </Box>
      </Box>
      {relevantBlogs && relevantBlogs.length ? (
        <Box maxWidth={1240} mx="auto">
          <Box my={5}>
            <UnderlineWrapper>
              <Typography className={relevantBlogsTitle}>Kapcsolódó cikkek</Typography>
            </UnderlineWrapper>
          </Box>
          <Grid container spacing={4}>
            {relevantBlogs.map((d) => <BlogCard key={d.id} data={d} categories={categories} />)}
          </Grid>
        </Box>
      ) : <></>}
    </Box>
  );
};

export default BlogDetails;
