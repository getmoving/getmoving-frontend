export interface BlogProps {
  id: string;
  title: string;
  details: string;
  body1: string;
  body2: string;
  coverUrl: string;
  thumbnailUrl: string;
  image1Url?: string;
  image2Url?: string;
  categoryIds: string[];
  createdAt: number;
}

export interface BlogCategory {
  id: string;
  name: string;
}
