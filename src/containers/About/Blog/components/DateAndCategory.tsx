import React from 'react';
import { Box, Typography } from '@material-ui/core';
import { BlogCategory } from 'containers/About/Blog/Blog.types';

interface Props {
  createdAt: number;
  categories: BlogCategory[];
}

const DateAndCategory: React.FC<Props> = ({ createdAt, categories }) => (
  <Box display="flex" alignItems="center">
    <Typography variant="body2" color="textSecondary" component="p">
      {new Date(createdAt).toLocaleDateString()}
    </Typography>
    <Box display="flex" borderLeft="1px solid grey" ml={2} height={22} alignItems="center">
      <Box mx={2}>
        <Typography variant="body2" color="textSecondary" component="p">
          {categories.map((c) => c.name).join(', ')}
        </Typography>
      </Box>
    </Box>
  </Box>
);

export default DateAndCategory;
