import { createStyles, Theme } from '@material-ui/core/styles';
import blogbg from 'assets/blogbg.png';

interface Props {
  imageUrl?: string;
}

const styles = (theme: Theme) => createStyles<string, Props>({
  root: {
    [theme.breakpoints.down('xs')]: {
      margin: theme.spacing(0, 2),
    },
  },
  img: {
    display: 'flex',
    backgroundImage: `url(${blogbg})`,
    width: '100%',
    height: 300,
    backgroundSize: 'cover',
    backgroundPosition: 'center',
  },
  blogImg: {
    display: 'flex',
    backgroundImage: ({ imageUrl }) => `url(${imageUrl})`,
    width: '100%',
    height: 'calc(100vh - 256px)',
    backgroundSize: 'cover',
    backgroundPosition: 'center',
  },
  title: {
    fontSize: 50,
    fontWeight: 900,
    color: 'white',
    [theme.breakpoints.down('xs')]: {
      fontSize: 26,
    },
  },
  relevantBlogsTitle: {
    textTransform: 'uppercase',
    fontWeight: 500,
    fontSize: 30,
  },
  bodyImg: {
    maxWidth: 500,
    maxHeight: 500,
    width: '50vw',
    height: '50vw',
    objectFit: 'contain',
  },
});

export default styles;
