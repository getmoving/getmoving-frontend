import React, { useContext, useEffect } from 'react';
import { Box, Typography, Grid, CircularProgress } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { useTranslation } from 'react-i18next';

import RouteContext from 'contexts/RouteContext';
import { UnderlineWrapper } from 'components';
import styles from './Blog.styles';
import useBlogs from './useBlogs';
import BlogCard from './components/BlogCard';

const useStyles = makeStyles(styles);

const Blogs: React.FC = () => {
  const { t } = useTranslation();
  const { img, title } = useStyles({ imageUrl: '' });
  const { setVariant, setComponentType } = useContext(RouteContext);
  const { data, loading, error, categories } = useBlogs();

  useEffect(() => {
    setVariant('black');
    setComponentType('public');
  }, []);

  if (loading || error) return <CircularProgress />;

  return (
    <Box pt={10} mb={4}>
      <Box className={img}>
        <UnderlineWrapper centered color="white" margin="auto">
          <Typography className={title}>{t('blog.title')}</Typography>
        </UnderlineWrapper>
      </Box>
      <Box mt={4} mx="auto" maxWidth={1240}>
        <Grid container spacing={4}>
          {data?.map((d) => <BlogCard key={d.id} data={d} categories={categories} />)}
        </Grid>
      </Box>
    </Box>
  );
};

export default Blogs;
