import useAxios from 'axios-hooks';
import useGetRequest from 'hooks/useGetRequest';
import { API_ROOT } from 'utils/constants';
import { BlogProps, BlogCategory } from './Blog.types';

const useBlogs = () => {
  const { data, loading, error } = useGetRequest<BlogProps[]>(`${API_ROOT}/admin/get-blogs`);
  const [{ data: categories = [] }] = useAxios<BlogCategory[]>(`${API_ROOT}/admin/get-blog-categories`);

  return { data, loading, error, categories };
};

export default useBlogs;
