import React from 'react';
import { Switch, RouteComponentProps, Redirect } from 'react-router-dom';

import PublicRoute from 'routes/PublicRoute';
import PrivateRoute from 'routes/PrivateRoute';
import AuthContext from 'contexts/AuthContext';

import WorkoutPlan from './WorkoutPlan';
import PrivateVideos from './PrivateVideos';
import PublicVideos from './PublicVideos';
import Prices from './Prices';
import Recipes from './Recipes';
import Blog from './Blog';
import BlogDetails from './Blog/BlogDetails';
import Contact from './ContactPage';
import RecipeDetails from './Recipes/RecipeDetails';
import WorkoutPlanPublic from './WorkoutPlan/WorkoutPlanPublic';

const AboutContainer: React.FC<RouteComponentProps> = ({ match }) => {
  const { isActive } = React.useContext(AuthContext);

  return (
    <Switch>
      {isActive
        ? <PrivateRoute isAuthenticated={isActive} path={`${match.path}/videos`} component={PrivateVideos} />
        : <PublicRoute path={`${match.path}/videos`} component={PublicVideos} />}
      <PublicRoute path={`${match.path}/recipes`} component={Recipes} />
      <PrivateRoute isAuthenticated={isActive} path={`${match.path}/recipe-details/:id`} component={RecipeDetails} />
      <PublicRoute path={`${match.path}/blog`} component={Blog} />
      <PrivateRoute isAuthenticated={isActive} path={`${match.path}/blog-details/:id`} component={BlogDetails} />
      {isActive
        ? <PrivateRoute isAuthenticated={isActive} path={`${match.path}/workout-generator`} component={WorkoutPlan} />
        : <PublicRoute path={`${match.path}/workout-generator`} component={WorkoutPlanPublic} />}
      <PublicRoute path={`${match.path}/prices`} component={Prices} />
      <PublicRoute path={`${match.path}/contact`} component={Contact} />
      <Redirect to="/" />
    </Switch>
  );
};

export default AboutContainer;
