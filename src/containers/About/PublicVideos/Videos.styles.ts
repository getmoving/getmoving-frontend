import { createStyles } from '@material-ui/core/styles';
import videosbg from 'assets/videosbg.png';

const styles = () => createStyles({
  img: {
    display: 'flex',
    backgroundImage: `url(${videosbg})`,
    width: '100%',
    height: 300,
    backgroundSize: 'cover',
    backgroundPosition: 'center',
  },
  title: {
    fontSize: 50,
    fontWeight: 900,
    color: 'white',
  },
});

export default styles;
