import React, { useContext, useEffect } from 'react';
import { Box, Typography, useMediaQuery } from '@material-ui/core';
import { makeStyles, useTheme } from '@material-ui/core/styles';

import RouteContext from 'contexts/RouteContext';
import { UnderlineWrapper } from 'components';
import VideoSlider from 'containers/Landing/Videos/components/VideoSlider';
import VideoModal from 'containers/Landing/Videos/VideoModal';
import useVideos from 'containers/Landing/Videos/useVideos';
import { withVideoContext } from 'containers/Landing/Videos/VideoContext';
import styles from './Videos.styles';
import Features from './Features';

const useStyles = makeStyles(styles);

const Videos: React.FC = () => {
  const { img, title } = useStyles();
  const { setVariant, setComponentType } = useContext(RouteContext);
  const { t, loading, videos } = useVideos();
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down('xs'));

  useEffect(() => {
    setVariant('black');
    setComponentType('public');
  }, []);

  return (
    <Box pt={10}>
      <Box className={img}>
        <UnderlineWrapper centered color="white" margin="auto">
          <Typography className={title}>Oktatóvideók</Typography>
        </UnderlineWrapper>
      </Box>
      <Box mt={4} mx={isMobile ? 2 : 12}>
        {/* <UnderlineWrapper>
          <Typography variant="subtitle1">
            {t('videos.categories')}
          </Typography>
        </UnderlineWrapper> */}
        <Features />
        <Box mt={4}>
          <UnderlineWrapper>
            <Typography variant="subtitle1">
              {t('videos.lookInto')}
            </Typography>
          </UnderlineWrapper>
          <VideoSlider videos={videos} loading={loading} />
        </Box>
      </Box>
      <VideoModal />
    </Box>
  );
};

export default withVideoContext(Videos);
