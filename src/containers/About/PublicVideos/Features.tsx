import React from 'react';
import { Box, Divider, Grid, Typography } from '@material-ui/core';
import { useTranslation } from 'react-i18next';
import { GreenTickIcon } from 'assets/icons';

const Features: React.FC = () => {
  const { t } = useTranslation();
  const featureList = t('videos.features', { returnObjects: true }) as string[];

  return (
    <Box>
      <Divider />
      <Box my={4}>
        <Grid container justifyContent="space-between">
          {featureList.map((item) => (
            <Grid key={item} item>
              <Box display="flex" alignItems="center" my={1}>
                <GreenTickIcon />
                <Box m={1} />
                <Typography>
                  {item}
                </Typography>
              </Box>
            </Grid>
          ))}
        </Grid>
      </Box>
      <Divider />
    </Box>
  );
};

export default Features;
