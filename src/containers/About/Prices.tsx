import React, { useContext, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { Box, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

import pricesbg from 'assets/pricesbg.jpeg';
import RouteContext from 'contexts/RouteContext';
import { UnderlineWrapper, Prices } from 'components';

const useStyles = makeStyles({
  bg: {
    backgroundImage: `url(${pricesbg})`,
    width: '100%',
    height: '100%',
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    display: 'flex',
    flexDirection: 'column',
  },
});

const PricesPage: React.FC = () => {
  const { bg } = useStyles();
  const { t } = useTranslation();
  const { setVariant, setComponentType } = useContext(RouteContext);

  useEffect(() => {
    setVariant('black');
    setComponentType('public');
  }, []);

  return (
    <Box pt={10} textAlign="center">
      <Box className={bg}>
        <Box pt={2}>
          <UnderlineWrapper style={{ margin: 'auto' }} centered>
            <Typography style={{ fontSize: 50, fontWeight: 900 }}>{t('prices.title')}</Typography>
          </UnderlineWrapper>
        </Box>
        <Prices onSelect={() => null} />
      </Box>
    </Box>
  );
};

export default PricesPage;
