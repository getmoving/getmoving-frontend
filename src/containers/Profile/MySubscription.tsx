/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Box, Button, CircularProgress, Typography } from '@material-ui/core';
import { useHistory } from 'react-router-dom';

import AuthContext from 'contexts/AuthContext';
import { API_ROOT, PRICES } from 'utils/constants';
import colors from 'utils/colors';
import { PaymentType } from './Subscription/Subscription.types';

const MySubscription: React.FC = () => {
  const [loading, setLoading] = React.useState(false);
  const { isActive, userNode, isLoading, user, refetch } = React.useContext(AuthContext);
  const history = useHistory();
  const [payment, setPayment] = useState<any>(); // TODO type

  useEffect(() => {
    if (user) {
      setLoading(true);
      const getPayment = async () => {
        const res = await axios.get(`${API_ROOT}/api/get-last-payment`, { params: { userId: user.uid } });
        setPayment(res.data);
        setLoading(false);
      };
      getPayment();
    }
  }, [user]);

  const unsubscribe = async () => {
    // eslint-disable-next-line no-alert
    if (!window.confirm('Biztos lemondod az előfizetésed?')) return;
    if (!user?.uid) return;
    try {
      setLoading(true);
      await axios.post(`${API_ROOT}/api/unsubscribe`, { userId: user.uid });
      await refetch();
    } catch (error) {
      // eslint-disable-next-line no-alert
      alert(error.message); // TODO: use styled alerts
    } finally {
      setLoading(false);
    }
  };

  if (isLoading || loading || !userNode) return <CircularProgress />;

  const buttonDisabled = isLoading || loading || userNode?.payment?.isCancelled || !isActive;
  return (
    <Box pt={14} minHeight="calc(100vh - 149px)" boxSizing="border-box" maxWidth={1240}>
      <Box pt={2} mb={2}>
        <Typography style={{ fontSize: 20 }}>Előfizetésem</Typography>
      </Box>
      {!userNode.payment && (
        <Typography>
          Még nem fizettél elő.
        </Typography>
      )}
      {userNode.payment && (
        <Typography>
        A következő számlád összege: {
          // eslint-disable-next-line no-nested-ternary
            !userNode.payment.isActive || userNode.payment.isCancelled ? '-'
              : userNode.payment.paymentType === PaymentType.MONTHLY
                ? PRICES.MONTHLY.MONTH
                : PRICES.YEARLY.YEAR
          }
        </Typography>
      )}
      {userNode.payment?.validUntil && (
        <Typography>
          És időpontja: {
            !userNode.payment.isActive || userNode.payment.isCancelled ? '-'
              : new Date(userNode.payment.validUntil).toLocaleDateString()
          }
        </Typography>
      )}

      {payment?.FundingInformation && (
        <Box
          borderRadius="5px"
          boxShadow="-4px -4px 20px rgba(0, 0, 0, 0.05), 4px 4px 20px rgba(0, 0, 0, 0.05)"
          px={4}
          py={2}
          width={300}
          my={6}
        >
          <Typography>Kártya utolsó számjegyei:</Typography>
          <Typography>{`${payment.FundingInformation?.BankCard?.BankCardType}: **** **** ****${payment.FundingInformation?.BankCard?.MaskedPan}`}</Typography>
          <Typography>{`Lejár: ${payment.FundingInformation?.BankCard?.ValidThruYear}/${payment.FundingInformation?.BankCard?.ValidThruMonth}`}</Typography>
        </Box>
      )}
      {isActive && userNode?.payment?.isCancelled && userNode?.payment?.validUntil && (
        <Box mt={4}>
          <Typography style={{ color: colors.green }}>
            {`Még van aktív előfizetésed (${new Date(userNode.payment.validUntil).toLocaleDateString()})`}
          </Typography>
        </Box>
      )}
      <Box>
        <Button
          color="primary"
          disabled={!buttonDisabled || isLoading || loading || isActive}
          variant="contained"
          onClick={() => history.push('subscription')}
          style={{ color: 'black', marginTop: 16 }}
        >
          Előfizetek
        </Button>
      </Box>
      <Box>
        <Button
          disabled={buttonDisabled}
          variant="text"
          style={{ color: buttonDisabled ? colors.grey66 : colors.error, marginTop: 16 }}
          onClick={unsubscribe}
        >
          Előfizetés törlése
        </Button>
      </Box>
    </Box>
  );
};

export default MySubscription;
