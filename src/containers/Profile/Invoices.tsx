/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { Box, CircularProgress, Typography, Paper, TableContainer, Table, TableHead, TableRow, TableCell, TableBody } from '@material-ui/core';

import AuthContext from 'contexts/AuthContext';
import { API_ROOT } from 'utils/constants';
import { PaymentType } from './Subscription/Subscription.types';

const Invoices: React.FC = () => {
  const [payments, setPayments] = useState<any[]>(); // TODO type
  const { user, isLoading } = React.useContext(AuthContext);

  useEffect(() => {
    if (user) {
      const getUserPayments = async () => {
        const res = await axios.get(`${API_ROOT}/api/get-user-payments`, { params: { userId: user.uid } });
        setPayments(res.data);
      };
      getUserPayments();
    }
  }, [user]);

  if (isLoading) return <CircularProgress />;

  if (!payments || !payments?.length) {
    return <Typography>Nincs még számlád</Typography>;
  }

  return (
    <Box pt={14} mb={8} minHeight="calc(100vh - 149px)" boxSizing="border-box" maxWidth={1240}>
      <Box pt={2}>
        <Typography style={{ fontSize: 20, marginBottom: 16 }}>
          Számláim
        </Typography>
        <TableContainer component={Paper}>
          <Table>
            <TableHead style={{ backgroundColor: '#fff7d9', borderRadius: 5 }}>
              <TableRow>
                <TableCell>Dátum</TableCell>
                <TableCell>Leírás</TableCell>
                <TableCell>Szolgáltatási időszak</TableCell>
                <TableCell>Fizetési mód</TableCell>
                <TableCell align="right">Összeg</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {payments?.map((payment: any) => (
                <TableRow key={payment.id}>
                  <TableCell>{new Date(payment.CompletedAt).toLocaleDateString()}</TableCell>
                  {payment.Transactions ? <TableCell>{payment.Transactions[0]?.Items[0].Description === PaymentType.MONTHLY ? 'Havi előfizetés' : 'Éves előfizetés'}</TableCell> : <TableCell>-</TableCell>}
                  <TableCell>
                    {new Date(payment.CompletedAt).toLocaleDateString()}
                  -
                    {new Date(payment.CompletedAt).toLocaleDateString()}
                  </TableCell>
                  {payment.FundingInformation ? (
                    <TableCell>
                      {`${payment.FundingInformation.BankCard.BankCardType}: **** **** **** ${payment.FundingInformation.BankCard.MaskedPan}`}
                    </TableCell>
                  ) : (
                    <TableCell>
                  -
                    </TableCell>
                  )}
                  <TableCell align="right">{payment.Total} Ft</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Box>
    </Box>
  );
};

export default Invoices;
