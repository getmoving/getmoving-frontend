import React from 'react';
import { uuid } from 'utils/uuid';
import { Box, CircularProgress, Typography, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { useTranslation } from 'react-i18next';

import emptyplan from 'assets/emptyplan.png';
import { useHistory } from 'react-router-dom';
import useMyPlan from './useMyPlan';
import DayBlock from './components/DayBlock';

const useStyles = makeStyles({
  img: {
    marginTop: 16,
    backgroundImage: `url(${emptyplan})`,
    width: 'auto',
    height: 250,
    backgroundSize: 'contain',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
  },
});

const MyPlan: React.FC = () => {
  const { data } = useMyPlan();
  const { t } = useTranslation();
  const { img } = useStyles();
  const history = useHistory();

  const renderNoPlan = () => (
    <Box textAlign="center">
      <Box className={img} />
      <Box my={4}>
        <Typography>{t('myPlan.noPlanYet')}</Typography>
      </Box>
      <Button
        color="primary"
        variant="contained"
        onClick={() => history.push('/about/workout-generator')}
      >{t('myPlan.button')}
      </Button>
    </Box>
  );

  if (!data) return <CircularProgress />;

  const havePlan = data.length;

  return (
    <Box pt={14} minHeight="calc(100vh - 149px)" boxSizing="border-box" maxWidth={1240}>
      <Typography style={{ fontSize: 20 }}>Edzéstervem</Typography>
      {havePlan ? (
        <Box my={4}>
          <Typography>{t('myPlan.check')}</Typography>
        </Box>
      ) : null}
      {havePlan ? data.map((d) => <DayBlock key={uuid()} weekProps={d} />) : renderNoPlan()}
    </Box>
  );
};

export default MyPlan;
