import { createStyles } from '@material-ui/core/styles';

const styles = () => createStyles({
  cardRoot: {
    boxShadow: '-4px -4px 20px rgba(0, 0, 0, 0.05), 4px 4px 20px rgba(0, 0, 0, 0.05)',
    padding: 30,
    marginBottom: 32,
    borderRadius: 5,
  },
});

export default styles;
