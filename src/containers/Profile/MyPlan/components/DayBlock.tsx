import React from 'react';
import { Box, Typography, Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { WeekProps, DayProps } from 'containers/About/WorkoutPlan/components/PlanDashboard/Plan.types';
import { useTranslation } from 'react-i18next';
import colors from 'utils/colors';
import { GreenTickIcon, ErrorIcon } from 'assets/icons';
import styles from 'containers/Profile/MyPlan/MyPlan.styles';

const useStyles = makeStyles(styles);

interface Props {
  weekProps: WeekProps
}

const DAYS = ['A', 'B', 'C', 'D', 'E'];

const DayBlock: React.FC<Props> = ({ weekProps: { day0, day1, day2, day3, day4, startDate } }) => {
  const daysArray = [day0, day1, day2, day3, day4];
  const { cardRoot } = useStyles();
  const { t } = useTranslation();

  const renderBlock = (dayLetter: string, day?: DayProps) => {
    if (!day) return null;
    return (
      <Grid key={dayLetter} item xs={12} md={6}>
        <Box className={cardRoot} style={{ border: `${day.isDone ? `1px solid  ${colors.green}` : 'none'}` }}>
          <Typography style={{ fontSize: 25 }}>{t('myPlan.nDay', { n: dayLetter })}</Typography>
          <Box display="flex" alignItems="center" mt={1}>
            {day.isDone ? <GreenTickIcon /> : <ErrorIcon />}
            <Typography style={{ marginLeft: 16 }}>{day.isDone ? `Teljesítve! (${day.dateOfCompletion})` : t('myPlan.notDoneYet')}</Typography>
          </Box>
        </Box>
      </Grid>
    );
  };

  return (
    <Box>
      <Box mb={1}>
        <Typography style={{ color: colors.primary, fontWeight: 700, fontSize: 18 }}>
          {new Date(startDate).toLocaleDateString()}
        </Typography>
      </Box>
      <Grid container spacing={2}>
        {daysArray.map((day, i) => renderBlock(DAYS[i], day))}
      </Grid>
    </Box>
  );
};

export default DayBlock;
