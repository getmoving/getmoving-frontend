import { useContext, useEffect } from 'react';
import useAxios from 'axios-hooks';

import AuthContext from 'contexts/AuthContext';
import { API_ROOT } from 'utils/constants';
import { WeekProps } from 'containers/About/WorkoutPlan/components/PlanDashboard/Plan.types';

const compare = (a: WeekProps, b: WeekProps) => {
  if (a.startDate < b.startDate) {
    return 1;
  }
  if (a.startDate > b.startDate) {
    return -1;
  }
  return 0;
};

const useMyPlan = () => {
  const { userNode } = useContext(AuthContext);
  const [{ data = undefined, loading }, refetch] = useAxios<Record<string, unknown>>(`${API_ROOT}/api/get-plans/${userNode?.planId}`);
  const weeks = data && Object.keys(data).map((key) => data[key]) as WeekProps[];

  useEffect(() => {
    refetch();
  }, []);

  return {
    data: weeks ? weeks.sort(compare) : [],
    loading,
  };
};

export default useMyPlan;
