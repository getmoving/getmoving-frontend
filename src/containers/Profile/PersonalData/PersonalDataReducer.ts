import { GENDER } from 'contexts/RegisterContext';

export const initialState = {
  email: '',
  firstName: '',
  lastName: '',
  isLoading: false,
  gender: GENDER.OTHER,
};

type State = typeof initialState;
type Action = Partial<State>;

export const reducer = (s: State, a: Action) => ({ ...s, ...a });
