import React from 'react';
import { Box, Typography, CircularProgress, TextField, Button, FormControl, FormLabel, RadioGroup, FormControlLabel, Radio } from '@material-ui/core';
import { GENDER } from 'contexts/RegisterContext';
import usePersonalData from './usePersonalData';

const PersonalData: React.FC = () => {
  const {
    data, onSubmit, form, t, state, dispatch, cancelButton, setInitialState, loading, showButtons,
  } = usePersonalData();

  if (!data) return <CircularProgress />;

  return (
    <Box pt={14} minHeight="calc(100vh - 149px)" boxSizing="border-box" maxWidth={1240}>
      <Typography style={{ fontSize: 20 }}>{t('profile.personalData')}</Typography>
      <form className={form} onSubmit={onSubmit}>
        <Box my={4} display="flex" justifyContent="space-between">
          <TextField
            style={{ marginRight: 16 }}
            fullWidth
            required
            InputLabelProps={{ required: false }}
            value={state.lastName}
            onChange={(e) => dispatch({ lastName: e.target.value })}
            name="lastName"
            label={t('auth.lastName')}
          />
          <TextField
            style={{ marginLeft: 16 }}
            fullWidth
            required
            InputLabelProps={{ required: false }}
            value={state.firstName}
            onChange={(e) => dispatch({ firstName: e.target.value })}
            name="firstName"
            label={t('auth.firstName')}
          />
        </Box>
        <TextField
          fullWidth
          required
          InputLabelProps={{ required: false }}
          value={state.email}
          onChange={(e) => dispatch({ email: e.target.value })}
          name="email"
          type="email"
          label={t('common.email')}
        />
        <Box m={4} />
        <FormControl component="fieldset">
          <FormLabel component="legend">{t('auth.gender')}</FormLabel>
          <RadioGroup
            value={state.gender}
            onChange={(e) => dispatch({ gender: e.target.value as GENDER })}
          >
            <FormControlLabel value={GENDER.OTHER} control={<Radio color="primary" />} label={t('auth.other')} />
            <FormControlLabel value={GENDER.FEMALE} control={<Radio color="primary" />} label={t('auth.female')} />
            <FormControlLabel value={GENDER.MALE} control={<Radio color="primary" />} label={t('auth.male')} />
          </RadioGroup>
        </FormControl>
        {showButtons && (
          <Box mt={4} display="flex" justifyContent="space-between">
            <Button
              className={cancelButton}
              onClick={setInitialState}
              type="button"
              color="primary"
              variant="outlined"
            >
              {t('common.cancel')}
            </Button>
            <Button
              endIcon={(loading) && <CircularProgress size={16} />}
              disabled={loading}
              type="submit"
              color="primary"
              variant="contained"
            >
              {t('common.save')}
            </Button>
          </Box>
        )}
      </form>
    </Box>
  );
};

export default PersonalData;
