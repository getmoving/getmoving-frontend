import { createStyles } from '@material-ui/core/styles';

const styles = () => createStyles({
  form: {
    maxWidth: 600,
  },
  cancelButton: {
    color: 'black',
    borderColor: 'black',
  },
});

export default styles;
