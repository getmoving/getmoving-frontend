import { useContext, useEffect, useReducer, FormEvent, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useTranslation } from 'react-i18next';
import useAxios from 'axios-hooks';
import axios from 'axios';

import AuthContext from 'contexts/AuthContext';
import { API_ROOT } from 'utils/constants';
import { reducer, initialState } from './PersonalDataReducer';

import styles from './PersonalData.styles';

const useStyles = makeStyles(styles);

const usePersonalData = () => {
  const { cancelButton, form } = useStyles();
  const { t } = useTranslation();
  const { user } = useContext(AuthContext);
  const [{ data, loading }, refetch] = useAxios(`${API_ROOT}/api/get-user-by-id/${user?.uid}`);
  const [state, dispatch] = useReducer(reducer, initialState);
  const [showButtons, setShowButtons] = useState(false);

  const initialObj = {
    email: data.email, firstName: data.firstName, lastName: data.lastName, gender: data.gender,
  };

  const setInitialState = () => {
    setShowButtons(false);
    dispatch(initialObj);
  };

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const { isLoading, ...rest } = state;

  useEffect(() => {
    if (JSON.stringify(rest) !== JSON.stringify(initialObj)) setShowButtons(true);
  }, [state.email, state.firstName, state.lastName, state.gender]);

  useEffect(() => {
    if (user) refetch();
  }, [user]);

  useEffect(() => {
    if (!data) return;
    setInitialState();
  }, [data]);

  const onSubmit = async (e: FormEvent) => {
    e.preventDefault();
    if (!user) return;
    try {
      dispatch({ isLoading: true });
      await axios.put(`${API_ROOT}/api/update-user/${user.uid}`, rest);
      refetch();
    } catch (error) {
      // eslint-disable-next-line no-alert
      alert(error.message); // TODO: use styled alerts
    } finally {
      dispatch({ isLoading: false });
    }
  };

  return {
    data,
    cancelButton,
    form,
    onSubmit,
    loading: loading || state.isLoading,
    state,
    dispatch,
    setInitialState,
    showButtons,
    t,
  };
};

export default usePersonalData;
