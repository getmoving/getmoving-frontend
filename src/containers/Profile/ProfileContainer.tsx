import React from 'react';
import { Switch, RouteComponentProps, Redirect, Route, NavLink } from 'react-router-dom';
import { Typography, Box, Theme, useMediaQuery } from '@material-ui/core';
import { makeStyles, useTheme } from '@material-ui/core/styles';

import fApp from 'firebase/init-firebase';
import Footer from 'routes/Footer';
import { UnderlineWrapper } from 'components';

import PersonalData from './PersonalData';
import MyPlan from './MyPlan';
import Subscription from './Subscription';
import MySubscription from './MySubscription';
import Invoices from './Invoices';

const useStyles = makeStyles((theme: Theme) => ({
  navLink: {
    color: 'grey',
    textDecoration: 'unset',
    lineHeight: '14px',
    '&:hover': {
      fontWeight: 600,
      color: `${theme.palette.primary.light}`,
      transition: 'font-weight 0.05s ease-out 0.05s',
      '&::before': {
        content: '""',
        position: 'absolute',
        bottom: 0,
        left: 0,
        width: '50%',
        height: 3,
        borderRadius: 3,
        backgroundColor: theme.palette.primary.light,
      },
    },
    '&::after': {
      display: 'block',
      content: 'attr(title)',
      fontWeight: 'bold',
      height: '1px',
      color: 'transparent',
      overflow: 'hidden',
      visibility: 'hidden',
    },
  },
  activeNavLink: {
    color: `${theme.palette.primary.light}`,
    textDecoration: 'unset',
    lineHeight: '14px',
    fontWeight: 600,
    '&::before': {
      content: '""',
      position: 'absolute',
      bottom: 0,
      left: 0,
      width: '50%',
      height: 3,
      borderRadius: 3,
      backgroundColor: theme.palette.primary.main,
    },
    '&::after': {
      display: 'block',
      content: 'attr(title)',
      fontWeight: 'bold',
      height: '1px',
      color: 'transparent',
      overflow: 'hidden',
      visibility: 'hidden',
    },
  },
}));

const ProfileContainer: React.FC<RouteComponentProps> = ({ match }) => {
  const { activeNavLink, navLink } = useStyles();
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down('xs'));
  return (
    <>
      <Box display={isMobile ? 'grid' : 'flex'} justifyContent="space-between">
        <Box
          textAlign="center"
          minHeight="100%"
          width={isMobile ? '100%' : '20%'}
          position={isMobile ? 'initial' : 'fixed'}
        >
          <Box mt={14} mb={7}>
            <UnderlineWrapper margin="auto">
              <Typography style={{ fontSize: 50, fontWeight: 900 }}>Profil</Typography>
            </UnderlineWrapper>
          </Box>
          <Typography style={{ position: 'relative', margin: 'auto', width: 'fit-content', marginBottom: 16 }}>
            <NavLink
              activeClassName={activeNavLink}
              className={navLink}
              to="personal-data"
              title="Személyes adatok"
            >
              Személyes adatok
            </NavLink>
          </Typography>
          <Typography style={{ position: 'relative', margin: 'auto', width: 'fit-content', marginBottom: 16 }}>
            <NavLink
              activeClassName={activeNavLink}
              className={navLink}
              to="my-subscription"
              title="Előfizetésem"
            >
              Előfizetésem
            </NavLink>
          </Typography>
          <Typography style={{ position: 'relative', margin: 'auto', width: 'fit-content', marginBottom: 16 }}>
            <NavLink
              activeClassName={activeNavLink}
              className={navLink}
              to="invoices"
              title="Számláim"
            >
              Számláim
            </NavLink>
          </Typography>
          <Typography style={{ position: 'relative', margin: 'auto', width: 'fit-content', marginBottom: 16 }}>
            <NavLink
              activeClassName={activeNavLink}
              className={navLink}
              to="my-plan"
              title="Edzéstervem"
            >
              Edzéstervem
            </NavLink>
          </Typography>
          <Typography style={{ position: 'relative', margin: 'auto', width: 'fit-content', marginBottom: 16 }}>
            <NavLink
              activeClassName={activeNavLink}
              className={navLink}
              to="/auth"
              onClick={() => fApp.auth().signOut()}
              title="Kilépés"
            >
              Kilépés
            </NavLink>
          </Typography>
        </Box>
        <Box ml={isMobile ? 0 : '20%'} pl={4} width="100%" mr={isMobile ? 0 : '5%'} borderLeft="1px solid #C4C4C4">
          <Switch>
            <Route path={`${match.path}/personal-data`} component={PersonalData} />
            <Route path={`${match.path}/my-plan`} component={MyPlan} />
            <Route path={`${match.path}/subscription`} component={Subscription} />
            <Route path={`${match.path}/my-subscription`} component={MySubscription} />
            <Route path={`${match.path}/invoices`} component={Invoices} />
            <Redirect to="/" />
          </Switch>
        </Box>
      </Box>
      <Footer />
    </>
  );
};

export default ProfileContainer;
