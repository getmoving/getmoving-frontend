import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import axios from 'axios';
import { Box, Typography, Collapse, Button, FormControlLabel, Checkbox, TextField } from '@material-ui/core';

import AuthContext from 'contexts/AuthContext';
import barionlogo from 'assets/barion.svg';
import { Prices, UnderlineWrapper } from 'components';
import { API_ROOT } from 'utils/constants';
import { useQuery } from 'utils/useQuery';
import colors from 'utils/colors';
import { CheckCircleOutline, ErrorOutline } from '@material-ui/icons';
import useSubscription from './useSubscription';
import { PaymentType } from './Subscription.types';

const Subscription: React.FC = () => {
  const { t } = useTranslation();
  const [paymentState, setPaymentState] = useState(''); // TODO enum
  const { isActive, userNode } = React.useContext(AuthContext);
  const { onSubmit, loading } = useSubscription();
  const [paymentType, setPaymentType] = useState<PaymentType | null>(null);
  const [accepted, setAccepted] = useState(false);
  const [coupon, setCoupon] = useState('');
  const [recurringAccepted, setRecurringAccepted] = useState(false);
  const [isCouponValid, setCouponValid] = useState(false);
  const [isCouponInputActive, setCouponInputActive] = useState(false);
  const query = useQuery();
  const paymentId = query.get('paymentId');

  useEffect(() => {
    if (paymentId) {
      const getPaymentState = async () => {
        const res = await axios.get(`${API_ROOT}/api/get-payment-state`, { params: { paymentId } });
        setPaymentState(res.data);
      };
      getPaymentState();
    }
  }, [paymentId]);

  const checkCoupon = async () => {
    const res = await axios.get(`${API_ROOT}/api/check-coupon`, { params: { code: coupon } });
    setCouponValid(res.data);
    setCouponInputActive(false);
  };

  return (
    <Box pt={14} textAlign="center">
      <Box pt={2}>
        <UnderlineWrapper style={{ margin: 'auto' }} centered>
          <Typography style={{ fontSize: 50, fontWeight: 900 }}>{t('navbar.subscription')}</Typography>
        </UnderlineWrapper>
      </Box>
      <Prices onSelect={setPaymentType} selected={paymentType || undefined} />
      <Collapse in={!!paymentType}>
        <Box textAlign="left">
          {paymentType === PaymentType.YEARLY && (
            <Box display="flex" mb={3}>
              <Box mr={2}>
                <TextField
                  variant="outlined"
                  value={coupon}
                  onChange={(e) => setCoupon(e.target.value)}
                  name="coupon"
                  label={t('prices.coupon')}
                  onBlur={checkCoupon}
                  onFocus={() => setCouponInputActive(true)}
                  // eslint-disable-next-line no-nested-ternary
                  InputProps={{ endAdornment: coupon.length && !isCouponInputActive
                    ? (isCouponValid ? <CheckCircleOutline style={{ color: 'green' }} /> : <ErrorOutline style={{ color: 'red' }} />)
                    : undefined }}
                />
              </Box>
              <Typography style={{ alignSelf: 'center' }}>
                Amennyiben rendelkezel kuponkóddal, kérjük itt add meg!
              </Typography>
            </Box>
          )}
          <Typography>
            {paymentType === PaymentType.MONTHLY ? t('prices.monthlyHelp') : t('prices.yearlyHelp')}
          </Typography>
          <Box my={2}>
            <FormControlLabel
              control={(
                <Checkbox
                  required
                  color="primary"
                  checked={accepted}
                  onChange={(e) => setAccepted(e.target.checked)}
                />
              )}
              label={t('auth.accept')}
            />
            <br />
            <FormControlLabel
              control={(
                <Checkbox
                  required
                  color="primary"
                  checked={recurringAccepted}
                  onChange={(e) => setRecurringAccepted(e.target.checked)}
                />
              )}
              label="Az ismétlődő fizetéssel járó kötelességemet elfogadom"
            />
          </Box>
          <Box my={2} textAlign="center">
            <Button
              disabled={!!paymentId
                || isActive || loading || !accepted || !recurringAccepted || isCouponInputActive}
              onClick={() => paymentType && onSubmit(paymentType, coupon)}
              variant="contained"
              color="primary"
            >
            Fizetek
            </Button>
          </Box>
          <Box mt={5}>
            <img alt="barion" style={{ width: 500 }} src={barionlogo} />
          </Box>
        </Box>
      </Collapse>
      {paymentId && paymentState && (
        <Box my={2}>
          <Typography
            style={{ fontWeight: 'bold', color: paymentState === 'Succeeded' ? colors.green : colors.error }}
          >
            {paymentState === 'Succeeded' ? 'Sikeres fizetés!' : 'Sikertelen fizetés'}
          </Typography>
        </Box>
      )}
      {isActive && !paymentId && userNode?.payment?.validUntil && (
        <Box mt={2}>
          <Typography
            style={{ fontWeight: 'bold', color: userNode?.payment?.isCancelled ? colors.error : colors.green }}
          >
            {userNode?.payment?.isCancelled
              ? `Előfizetésed lejár: ${new Date(userNode.payment.validUntil).toLocaleDateString()}`
              : `Van már megújuló előfizetésed (${new Date(userNode.payment.validUntil).toLocaleDateString()})`}
          </Typography>
        </Box>
      )}
    </Box>
  );
};

export default Subscription;
