export interface Subscription {
  GatewayUrl: string;
}

// do not modify, must be the same as on the backend
export enum PaymentType {
  MONTHLY = 'MONTHLY',
  YEARLY = 'YEARLY',
}
