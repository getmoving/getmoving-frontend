/* eslint-disable @typescript-eslint/no-explicit-any */
import axios from 'axios';
import { API_ROOT } from 'utils/constants';
import { useContext, useState } from 'react';
import AuthContext from 'contexts/AuthContext';
import { Subscription, PaymentType } from './Subscription.types';

const useSubscription = () => {
  const { user } = useContext(AuthContext);
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState<Subscription>();

  const onSubmit = async (paymentType: PaymentType, coupon: string) => {
    try {
      setLoading(true);
      const res = await axios.post<Subscription>(`${API_ROOT}/api/subscribe`, {
        uid: user?.uid,
        paymentType,
        coupon: paymentType === PaymentType.YEARLY ? coupon : '',
      });
      setData(res.data); // TODO data kell itt?
      window.open(res.data.GatewayUrl, '_self');
    } catch (error) {
      // eslint-disable-next-line no-alert
      alert((error as any).message); // TODO: use styled alerts
    } finally {
      setLoading(false);
    }
  };
  return { loading, onSubmit, data };
};

export default useSubscription;
