export { default as Banner } from './Banner';
export { default as Contact } from './Contact';
export { default as Offers } from './Offers';
export { default as Intro } from './Intro';
export { default as Trial } from './Trial';
export { default as About } from './About';
export { default as Videos } from './Videos';
