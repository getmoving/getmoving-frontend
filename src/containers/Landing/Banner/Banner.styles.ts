import { createStyles, Theme } from '@material-ui/core/styles';
import landingbg from 'assets/landingbg.png';

interface Props {
  isMobile?: boolean;
}

const styles = (theme: Theme) => createStyles<string, Props>({
  title: {
    color: theme.palette.secondary.main,
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 96,
    textTransform: 'uppercase',
    zIndex: 400,
    [theme.breakpoints.down('sm')]: {
      fontSize: 60,
    },
    [theme.breakpoints.down('xs')]: {
      marginLeft: 32,
      marginRight: 32,
      lineHeight: 1.25,
    },
  },
  description: {
    color: theme.palette.secondary.main,
    maxWidth: 610,
    fontSize: 20,
    margin: 'auto',
    marginBottom: 30,
    textAlign: 'center',
    zIndex: 400,
    padding: theme.spacing(2),
  },
  hr: {
    backgroundColor: theme.palette.secondary.main,
    width: 100,
    height: 6,
    margin: 'auto',
    borderRadius: 3,
    marginBottom: theme.spacing(5),
    zIndex: 400,
  },
  imageContainer: {
    backgroundImage: `url(${landingbg})`,
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    height: '100vh',
    width: '100%',
    filter: ({ isMobile }) => (isMobile ? 'brightness(0.5)' : 'initial'),
    backgroundPositionX: ({ isMobile }) => (isMobile ? '75%' : 'initial'),
  },
  dividerContainer: {
    height: '100%',
    overflow: 'hidden',
  },
  zIndex400: {
    zIndex: 400,
    textTransform: 'uppercase',
  },
  leftMargin: {
    marginLeft: '.7em',
  },
  welcomeTextContainer: {
    top: ({ isMobile }) => (isMobile ? '10vh' : '33vh'),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    width: '98vw',
  },
  darkerOverlay: {
    background: 'black',
    opacity: '0.4',
    height: '400%',
    transform: 'rotate(30deg)',
    marginTop: '-71.5%',
    marginLeft: '-20.5%',
  },
  ligtherOverlay: {
    background: 'black',
    opacity: '0.2',
    height: '400%',
    transform: 'rotate(30deg)',
    marginTop: '-21.5%',
    marginLeft: '-20.5%',
  },
});

export default styles;
