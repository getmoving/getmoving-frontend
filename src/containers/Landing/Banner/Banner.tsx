import React, { useContext } from 'react';
import { Typography, Box, Divider, Button, Grid, useMediaQuery, Theme } from '@material-ui/core';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import { useHistory } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import VisibilitySensor from 'react-visibility-sensor';

import RouteContext from 'contexts/RouteContext';
import { DoubleRightArrow } from 'assets/icons';
import styles from './Banner.styles';

const useStyles = makeStyles(styles);

const Banner: React.FC = () => {
  const theme: Theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down('xs'));
  const {
    darkerOverlay,
    ligtherOverlay,
    dividerContainer,
    zIndex400,
    title,
    description,
    leftMargin,
    hr,
    imageContainer,
    welcomeTextContainer,
  } = useStyles({ isMobile });
  const history = useHistory();
  const { t } = useTranslation();
  const { setVariant } = useContext(RouteContext);

  return (
    <>
      <Box position="absolute">
        <Box position="relative" className={welcomeTextContainer}>
          <Typography className={title}>{t('landing.getmoving')}</Typography>
          <Divider className={hr} />
          <Typography className={description}>
            {t('landing.description')}
          </Typography>
          <Button
            onClick={() => history.push('auth/register')}
            color="primary"
            variant="contained"
            className={zIndex400}
          >
            {t('landing.register')}
            <span className={leftMargin}>
              <DoubleRightArrow />
            </span>
          </Button>
        </Box>
      </Box>
      <VisibilitySensor
        partialVisibility
        onChange={(isVisible) => {
          setVariant(isVisible ? 'transparent' : 'black');
        }}
      >
        <Box className={imageContainer}>
          <Grid container className={dividerContainer}>
            {!isMobile ? <Grid item xs={8} md={8} lg={8} className={darkerOverlay} /> : null}
            {!isMobile ? <Grid item xs={5} md={5} lg={5} className={ligtherOverlay} /> : null}
            <Grid item xs={1} md={1} lg={1} />
          </Grid>
        </Box>
      </VisibilitySensor>
    </>
  );
};

export default Banner;
