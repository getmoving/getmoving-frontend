import React from 'react';
import { Box, Typography } from '@material-ui/core';
import { uuid } from 'utils/uuid';

import { Carousel, UnderlineWrapper } from 'components';
import colors from 'utils/colors';
import RenderCards from './components/RenderCards';
import useAbout from './useAbout';
import Placeholder from './Placeholder';

const About: React.FC = () => {
  const { t, error, loading, cards, quotes, rootContainer } = useAbout();
  if (error) return <Typography color="error">{error}</Typography>;

  return (
    <Box py={6} bgcolor={colors.greyF6} className={rootContainer}>
      <UnderlineWrapper>
        <Typography style={{ textTransform: 'uppercase' }} variant="subtitle1">
          {t('landing.about.title')}
        </Typography>
      </UnderlineWrapper>
      <Box className={quotes}>
        <Carousel slidesToShow={4} slidesToScroll={4}>
          {loading || !cards
            ? Array(8).fill('').map(() => <Placeholder key={uuid()} />)
            : cards.map((d) => <RenderCards key={d.name} cardProps={d} />)}
        </Carousel>
      </Box>
    </Box>
  );
};

export default About;
