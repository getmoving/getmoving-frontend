export interface AboutDataProps {
  src: string;
  name: string;
  description: string;
}
