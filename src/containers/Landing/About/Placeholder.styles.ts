import { createStyles } from '@material-ui/core/styles';

const styles = () => createStyles({
  cardContainer: {
    padding: '1em 2em 2em 2em',
  },
  circle: {
    margin: 'auto',
  },
  text: {
    margin: '1em auto 0 auto',
  },
});

export default styles;
