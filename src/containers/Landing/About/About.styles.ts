import { Theme } from '@material-ui/core';
import { createStyles } from '@material-ui/core/styles';
import quote from 'assets/quote.svg';

interface Props {
  isMobile?: boolean;
}

const styles = (theme: Theme) => createStyles<string, Props>({
  rootContainer: {
    padding: ({ isMobile }) => (isMobile ? '2em' : '2em 100px'),
  },
  quotes: {
    paddingTop: '4em',
    position: 'relative',
    '&:before': {
      zIndex: 5,
      content: `url(${quote})`,
      position: 'absolute',
      left: 0,
      top: 36,
      opacity: 0.8,
      [theme.breakpoints.down('sm')]: {
        transform: 'scale(0.7)',
      },
    },
    '&:after': {
      zIndex: 5,
      content: `url(${quote})`,
      position: 'absolute',
      right: 0,
      bottom: 36,
      opacity: 0.8,
      [theme.breakpoints.down('sm')]: {
        height: 50,
      },
      transform: 'rotate(180deg)',
      [theme.breakpoints.down('sm')]: {
        transform: 'scale(0.7) rotate(180deg)',
      },
    },
  },
});

export default styles;
