import { useTranslation } from 'react-i18next';
import { useTheme, makeStyles } from '@material-ui/core/styles';
import { Theme, useMediaQuery } from '@material-ui/core';
import { AboutDataProps as CardProps } from 'containers/Landing/About/About.types';

import useGetRequest from 'hooks/useGetRequest';
import { API_ROOT } from 'utils/constants';
import styles from './About.styles';

const useStyles = makeStyles(styles);

export default function useAbout() {
  const { t } = useTranslation();
  const theme: Theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down('sm'));
  const { quotes, rootContainer } = useStyles({ isMobile });
  const { data, loading, error } = useGetRequest(`${API_ROOT}/api/get-about`);
  const cards = data as CardProps[] | undefined;

  return { t, error, isMobile, loading, cards, quotes, rootContainer };
}
