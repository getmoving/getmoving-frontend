import React from 'react';
import { Card } from '@material-ui/core';
import Skeleton from '@material-ui/lab/Skeleton';
import { makeStyles } from '@material-ui/core/styles';

import styles from './Placeholder.styles';

const useStyles = makeStyles(styles);

const PlaceHolder = () => {
  const { cardContainer, circle, text } = useStyles();
  return (
    <Card className={cardContainer}>
      <Skeleton variant="circle" width={80} height={80} className={circle} />
      <Skeleton variant="text" width="30%" className={text} />
      <Skeleton variant="rect" width="100%" height={80} className={text} />
    </Card>
  );
};

export default PlaceHolder;
