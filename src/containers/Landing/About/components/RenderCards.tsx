import React from 'react';
import { AboutDataProps as CardProps } from 'containers/Landing/About/About.types';
import Card from './Card';

interface Props {
  cardProps: CardProps;
}

const RenderCards: React.FC<Props> = ({ cardProps }) => (
  <Card {...cardProps} />
);

export default RenderCards;
