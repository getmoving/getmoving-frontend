import { Theme } from '@material-ui/core';
import { createStyles } from '@material-ui/core/styles';

const styles = (theme: Theme) => createStyles({
  box: {
    padding: theme.spacing(3),
    margin: theme.spacing(1),
    backgroundColor: 'white',
    maxWidth: 360,
    textAlign: 'center',
    position: 'relative',
    borderRadius: 6,
  },
  cardTitle: {
    fontWeight: 700,
    fontSize: 17,
    marginTop: 20,
    marginBottom: 20,
    textTransform: 'uppercase',
  },
  image: {
    height: 120,
    width: 120,
    margin: 'auto',
    objectFit: 'cover',
    borderRadius: '50%',
  },
  quote: {
    width: '100px !important',
  },
});

export default styles;
