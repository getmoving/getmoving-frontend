import React from 'react';
import { Box, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { AboutDataProps } from 'containers/Landing/About/About.types';
import styles from './Card.styles';

const useStyles = makeStyles(styles);

const Card: React.FC<AboutDataProps> = ({ name, description, src }) => {
  const { cardTitle, image, box } = useStyles();
  return (
    <Box className={box}>
      <img className={image} alt="profile" src={src} />
      <Typography className={cardTitle}>{name}</Typography>
      <Typography style={{ height: 150 }}>{description}</Typography>
    </Box>
  );
};

export default Card;
