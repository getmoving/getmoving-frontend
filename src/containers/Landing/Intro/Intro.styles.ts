import { createStyles, Theme } from '@material-ui/core/styles';
import colors from 'utils/colors';

const styles = (theme: Theme) => createStyles({
  root: {
    backgroundColor: colors.greyF6,
    display: 'flex',
    padding: theme.spacing(8, 12),
    [theme.breakpoints.down('sm')]: {
      flexDirection: 'column',
      padding: theme.spacing(4, 2),
    },
  },
  videoContainer: {
    width: '60%',
    height: 400,
    backgroundColor: 'white',
    textAlign: 'center',
    [theme.breakpoints.down('sm')]: {
      width: '100%',
    },
  },
  img: {
    marginTop: theme.spacing(3),
    height: 50,
    [theme.breakpoints.down('sm')]: {
      marginTop: theme.spacing(3),
    },
  },
  rotatedImg: {
    height: 50,
    transform: 'rotate(180deg)',
    float: 'right',
    [theme.breakpoints.down('sm')]: {
    },
  },
  underlined: {
    textTransform: 'uppercase',
    [theme.breakpoints.down('sm')]: {
      marginBottom: theme.spacing(4),
    },
  },
  introduction: {
    fontSize: 18,
    fontStyle: 'italic',
  },
  name: {
    fontSize: 16,
    marginLeft: theme.spacing(3),
    marginTop: theme.spacing(4),
    [theme.breakpoints.down('sm')]: {
      marginLeft: 0,
    },
  },
});

export default styles;
