import React from 'react';
import { Box, Typography, Theme, useMediaQuery } from '@material-ui/core';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import { useTranslation } from 'react-i18next';
import ReactPlayer from 'react-player';

import { UnderlineWrapper } from 'components';
import { CONTACTS } from 'utils/constants';
import quote from 'assets/quote.svg';
import styles from './Intro.styles';

const useStyles = makeStyles(styles);

const Intro: React.FC = () => {
  const { name, underlined, rotatedImg, introduction, root, videoContainer, img } = useStyles();
  const { t } = useTranslation();
  const theme: Theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down('sm'));

  const title = (
    <UnderlineWrapper className={underlined}>
      <Typography variant="subtitle1">{t('landing.introduction.title')}</Typography>
    </UnderlineWrapper>
  );

  return (
    <Box className={root}>
      {isMobile && title}
      <Box className={videoContainer}>
        <ReactPlayer
          url="https://www.youtube.com/watch?v=KOGTaxk74kw"
          width="100%"
          height="100%"
        />
      </Box>
      <Box width={isMobile ? '100%' : '40%'}>
        <Box ml={3}>
          {!isMobile && title}
          <img className={img} alt="quote" src={quote} />
          <Box my={2} mx={isMobile ? 0 : 3}>
            <Typography className={introduction}>
              {t('landing.introduction.description')}
            </Typography>
          </Box>
          <img className={rotatedImg} alt="quote" src={quote} />
          <Typography className={name}>{CONTACTS.name}</Typography>
        </Box>
      </Box>
    </Box>
  );
};

export default Intro;
