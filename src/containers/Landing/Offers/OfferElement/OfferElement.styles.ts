import { createStyles, Theme } from '@material-ui/core/styles';

const styles = (theme: Theme) => createStyles({
  avatar: {
    width: 50,
    height: 50,
    backgroundColor: '#333',
  },
  buttonStyle: {
    fontSize: '18px',
    fontWeight: 500,
  },
  container: {
    backgroundColor: 'unset',
    boxShadow: 'unset',
  },
  content: {
    paddingTop: 'unset',
    minHeight: '100px',
  },
  descriptionStyle: {
    fontSize: '.9em',
    lineHeight: '20px',
  },
  falseButtonStyle: {
    color: 'black',
    textDecoration: 'unset',
    paddingLeft: theme.spacing(1),
    fontSize: '1em',
  },
  noBottomPadding: {
    paddingBottom: 'unset',
  },
  titleStyle: {
    fontSize: '20px',
  },
});

export default styles;
