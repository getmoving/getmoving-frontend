import React from 'react';
import {
  Avatar,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  Typography,
} from '@material-ui/core';
import { NavLink } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import { useTranslation } from 'react-i18next';

import { DoubleRightArrow } from 'assets/icons';
import styles from './OfferElement.styles';

const useStyles = makeStyles(styles);

interface Props {
  title: string;
  description: string;
  Logo: React.FC;
  path: string;
}

const OfferElement: React.FC<Props> = ({ title, description, Logo, path }) => {
  const { t } = useTranslation();
  const {
    avatar,
    buttonStyle,
    container,
    content,
    descriptionStyle,
    falseButtonStyle,
    noBottomPadding,
    titleStyle,
  } = useStyles();
  return (
    <Card className={container}>
      <CardHeader
        avatar={
          // eslint-disable-next-line react/jsx-wrap-multilines
          <Avatar aria-label="recipe" className={avatar}>
            <Logo />
          </Avatar>
        }
        title={
          // eslint-disable-next-line react/jsx-wrap-multilines
          <Typography variant="h6" className={titleStyle}>
            {t(title)}
          </Typography>
        }
      />
      <CardContent className={`${noBottomPadding} ${content}`}>
        <Typography className={descriptionStyle}>{t(description)}</Typography>
      </CardContent>
      <CardActions>
        <NavLink to={path} className={falseButtonStyle}>
          <Typography className={buttonStyle}>
            Tovább <DoubleRightArrow />
          </Typography>
        </NavLink>
      </CardActions>
    </Card>
  );
};

export default OfferElement;
