import React from 'react';
import { Box, Grid, Typography } from '@material-ui/core';
import { useTranslation } from 'react-i18next';
import { makeStyles } from '@material-ui/core/styles';

import { UnderlineWrapper } from 'components';
import OffersObject from './constants';
import OfferElement from './OfferElement';
import styles from './Offers.styles';

const useStyles = makeStyles(styles);

const Offers: React.FC = () => {
  const { boxContainer, gridContainer, underlined } = useStyles();
  const { t } = useTranslation();

  return (
    <Box className={boxContainer}>
      <UnderlineWrapper color="black" margin="auto" centered>
        <Typography className={underlined} variant="subtitle1">
          {t('landing.offers.title')}
        </Typography>
      </UnderlineWrapper>
      <Grid container className={gridContainer}>
        {OffersObject.map((offer) => (
          <Grid item xs={12} sm={6} lg={3} key={offer.path}>
            <OfferElement {...offer} />
          </Grid>
        ))}
      </Grid>
    </Box>
  );
};

export default Offers;
