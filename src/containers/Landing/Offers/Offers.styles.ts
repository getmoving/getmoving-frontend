import { createStyles, Theme } from '@material-ui/core/styles';
import colors from 'utils/colors';

const styles = (theme: Theme) => createStyles({
  boxContainer: {
    backgroundColor: colors.primary,
    minHeight: 360,
    padding: theme.spacing(3),
  },
  gridContainer: {
    padding: theme.spacing(2, 7),
    [theme.breakpoints.down('sm')]: {
      padding: theme.spacing(0),
    },
  },
  underlined: {
    marginBottom: theme.spacing(3),
    textTransform: 'uppercase',
  },
});

export default styles;
