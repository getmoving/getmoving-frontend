import OfferBlogLogo from 'assets/OfferBlogLogo';
import OfferGeneratorLogo from 'assets/OfferGeneratorLogo';
import OfferVideoLogo from 'assets/OfferVideoLogo';
import OfferRecipeLogo from 'assets/OfferRecipeLogo';

export default [
  {
    title: 'landing.offers.videos.title',
    description: 'landing.offers.videos.description',
    Logo: OfferVideoLogo,
    path: '/about/videos',
  },
  {
    title: 'landing.offers.recipes.title',
    description: 'landing.offers.recipes.description',
    Logo: OfferRecipeLogo,
    path: '/about/recipes',
  },
  {
    title: 'landing.offers.generator.title',
    description: 'landing.offers.generator.description',
    Logo: OfferGeneratorLogo,
    path: '/about/workout-generator',
  },
  {
    title: 'landing.offers.blog.title',
    description: 'landing.offers.blog.description',
    Logo: OfferBlogLogo,
    path: '/about/blog',
  },
];
