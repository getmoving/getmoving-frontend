import React, { useContext, useEffect } from 'react';
import RouteContext from 'contexts/RouteContext';
import { Banner, Intro, Offers, Contact, About, Videos } from '.';

const LandingPage: React.FC = () => {
  const { setComponentType } = useContext(RouteContext);

  useEffect(() => {
    setComponentType('public');
  }, []);

  return (
    <>
      <Banner />
      <Intro />
      <Offers />
      <About />
      <Videos />
      <Contact />
    </>
  );
};

export default LandingPage;
