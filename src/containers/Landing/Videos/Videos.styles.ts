import { createStyles } from '@material-ui/core/styles';

interface Props {
  isMobile?: boolean;
}

const styles = () => createStyles<string, Props>({
  rootContainer: {
    padding: ({ isMobile }) => (isMobile ? '2em' : '2em 100px'),
  },
  title: {
    textTransform: 'uppercase',
  },
  container: {
    padding: '0px 40px 50px 0px',
  },
  carouselContentContainer: {
    padding: '50px 60px',
  },
  videoBox: {
    width: ({ isMobile }) => (isMobile ? '90%' : '50%'),
    height: '50%',
    margin: 'auto',
    marginTop: ({ isMobile }) => (isMobile ? '30%' : '10%'),
    position: 'relative',
  },
  closeIcon: {
    position: 'absolute',
    right: ({ isMobile }) => (isMobile ? 0 : -100),
    top: ({ isMobile }) => (isMobile ? -56 : -14),
  },
  leftIcon: {
    position: 'absolute',
    left: -100,
    top: 'calc(50% - 30px)',
  },
  rightIcon: {
    position: 'absolute',
    right: -100,
    top: 'calc(50% - 30px)',
  },
  playIcon: {
    position: 'absolute',
    cursor: 'pointer',
    top: 'calc(50% - 27px)',
    left: 'calc(50% - 27px)',
  },
});

export default styles;
