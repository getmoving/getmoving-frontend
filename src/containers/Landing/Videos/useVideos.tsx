import { useState, useContext, useEffect } from 'react';
import { useMediaQuery, Theme } from '@material-ui/core';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import { useTranslation } from 'react-i18next';

import VideoContext from './VideoContext';
import styles from './Videos.styles';

const useStyles = makeStyles(styles);

const useVideos = () => {
  const {
    url, setUrl, isOpen, setIsOpen, request: { data: videos, loading, error },
  } = useContext(VideoContext);
  const theme: Theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down('sm'));
  const { title, container, playIcon, rootContainer } = useStyles({ isMobile });
  const { t } = useTranslation();
  const [hasNext, setHasNext] = useState(false);
  const [hasPrev, setHasPrev] = useState(false);
  const currentVideoIndex = videos?.findIndex((d) => d.video === url) || 0;
  const prevUrl = videos && currentVideoIndex ? videos[currentVideoIndex - 1]?.video : '';
  const nextUrl = videos ? videos[currentVideoIndex + 1]?.video || '' : '';

  useEffect(() => {
    setHasPrev(!!prevUrl);
    setHasNext(!!nextUrl);
  }, [url]);

  const onPlayIconClick = (video: string) => {
    setUrl(video);
    setIsOpen(true);
  };

  return {
    rootContainer,
    url,
    setUrl,
    isOpen,
    setIsOpen,
    error,
    isMobile,
    playIcon,
    onPlayIconClick,
    title,
    container,
    loading,
    videos,
    prevUrl,
    nextUrl,
    hasNext,
    hasPrev,
    t,
  };
};

export default useVideos;
