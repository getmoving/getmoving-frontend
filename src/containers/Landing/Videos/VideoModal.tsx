import React from 'react';
import ReactPlayer from 'react-player';
import { Modal, Box, IconButton } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { HighlightOff, ChevronRight, ChevronLeft } from '@material-ui/icons';
import styles from './Videos.styles';
import useVideos from './useVideos';

const useStyles = makeStyles(styles);

const VideoModal: React.FC = () => {
  const { isMobile, hasPrev, prevUrl, hasNext, nextUrl,
    isOpen, setIsOpen, setUrl, url } = useVideos();
  const { videoBox, closeIcon, leftIcon, rightIcon } = useStyles({ isMobile });

  return (
    <Modal open={isOpen}>
      <Box className={videoBox}>
        <IconButton className={closeIcon} color="primary" onClick={() => setIsOpen(false)}>
          <HighlightOff fontSize="large" />
        </IconButton>
        {hasPrev && (
          <IconButton className={leftIcon} color="primary" onClick={() => setUrl(prevUrl)}>
            <ChevronLeft fontSize="large" />
          </IconButton>
        )}
        <ReactPlayer
          height="100%"
          width="100%"
          url={url}
          controls
        />
        {hasNext && (
          <IconButton className={rightIcon} color="primary" onClick={() => setUrl(nextUrl)}>
            <ChevronRight fontSize="large" />
          </IconButton>
        )}
      </Box>
    </Modal>
  );
};

export default VideoModal;
