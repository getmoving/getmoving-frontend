import React from 'react';
import { Box } from '@material-ui/core';

import useVideos from 'containers/Landing/Videos/useVideos';
import { LandingVideoDataProps as VideoProps } from 'containers/Landing/Videos/Videos.types';
import { PlayIcon } from 'assets/icons';

interface Props {
  videoProps: VideoProps;
}

const RenderCards: React.FC<Props> = ({ videoProps }) => {
  const { playIcon, onPlayIconClick } = useVideos();

  return (
    <Box position="relative">
      <Box className={playIcon} onClick={() => onPlayIconClick(videoProps.video)}>
        <PlayIcon />
      </Box>
      <img
        style={{ height: 170, width: 300, margin: '0 auto' }}
        alt="thumbnail"
        src={videoProps.thumbnail}
      />
    </Box>
  );
};

export default RenderCards;
