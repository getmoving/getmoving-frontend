import React from 'react';
import { Box } from '@material-ui/core';

import { LandingVideoDataProps } from 'containers/Landing/Videos/Videos.types';
import { uuid } from 'utils/uuid';
import { Carousel } from 'components';
import PlaceHolder from './PlaceHolder';
import RenderCards from './RenderCards';

interface Props {
  loading: boolean;
  videos?: LandingVideoDataProps[]
}

const VideoSlider: React.FC<Props> = ({ loading, videos }) => (
  <Box pt={5} pb={5}>
    <Carousel>
      {(loading || !videos)
        ? <PlaceHolder />
        : videos.map((v) => (
          <RenderCards
            key={uuid()}
            videoProps={v}
          />
        ))}
    </Carousel>
  </Box>
);

export default VideoSlider;
