import React from 'react';
import { Box } from '@material-ui/core';
import Skeleton from '@material-ui/lab/Skeleton';

import { uuid } from 'utils/uuid';
import useVideos from 'containers/Landing/Videos/useVideos';

const PlaceHolder: React.FC = () => {
  const { isMobile } = useVideos();
  return (
    <Box my={8} key={uuid()}>
      <Box
        display="flex"
        alignItems="center"
        justifyContent="space-evenly"
        flexDirection={isMobile ? 'column' : 'row'}
      >
        {Array(4).fill('').map(() => (
          <Skeleton key={uuid()} variant="rect" width={288} height={164} />
        ))}
      </Box>
    </Box>
  );
};

export default PlaceHolder;
