import React from 'react';
import { Typography, Box } from '@material-ui/core';

import { UnderlineWrapper } from 'components';
import { withVideoContext } from './VideoContext';

import VideoModal from './VideoModal';
import useVideos from './useVideos';
import VideoSlider from './components/VideoSlider';

const Videos: React.FC = () => {
  const { t, error, title: titleStyle, loading, videos, rootContainer } = useVideos();

  if (error) return <Typography color="error">{error}</Typography>;

  return (
    <Box mt={3} className={rootContainer}>
      <UnderlineWrapper margin="auto" centered>
        <Typography variant="subtitle1" className={titleStyle}>
          {t('landing.videos')}
        </Typography>
      </UnderlineWrapper>
      <VideoSlider videos={videos} loading={loading} />
      <VideoModal />
    </Box>
  );
};

export default withVideoContext(Videos);
