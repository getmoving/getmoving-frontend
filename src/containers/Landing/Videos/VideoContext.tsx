import React, { createContext, useState, ComponentType, Dispatch, SetStateAction } from 'react';
import { API_ROOT } from 'utils/constants';
import useGetRequest from 'hooks/useGetRequest';
import { LandingVideoDataProps as VideoProps } from './Videos.types';

interface RequestProps {
  data?: VideoProps[];
  loading: boolean;
  error: string;
}

interface Props {
  url: string;
  isOpen: boolean;
  setIsOpen: Dispatch<SetStateAction<boolean>>;
  setUrl: Dispatch<SetStateAction<string>>;
  request: RequestProps;
}

const VideoContext = createContext<Props>({
  isOpen: false,
  setIsOpen: () => {},
  url: '',
  setUrl: () => {},
  request: {
    data: [],
    loading: true,
    error: '',
  },
});

export const withVideoContext = (Component: ComponentType) => () => {
  const [url, setUrl] = useState<string>('');
  const [isOpen, setIsOpen] = useState<boolean>(false);

  const request = useGetRequest(`${API_ROOT}/api/get-landing-videos`) as RequestProps;

  return (
    <VideoContext.Provider value={{
      url,
      isOpen,
      setIsOpen,
      setUrl,
      request,
    }}
    >
      <Component />
    </VideoContext.Provider>
  );
};

export default VideoContext;
