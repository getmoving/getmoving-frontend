export interface LandingVideoDataProps {
  video: string;
  thumbnail: string;
}
