import { createStyles, Theme } from '@material-ui/core/styles';
import colors from 'utils/colors';

const styles = (theme: Theme) => createStyles({
  container: {
    [theme.breakpoints.only('xs')]: {
      padding: theme.spacing(4, 2),
    },
    padding: theme.spacing(8, 2),
    backgroundColor: colors.greyF6,
    textAlign: 'center',
  },
  title: {
    textTransform: 'uppercase',
    [theme.breakpoints.only('xs')]: {
      fontSize: 20,
    },
  },
  details: {
    margin: '30px auto',
    maxWidth: 800,
  },
});

export default styles;
