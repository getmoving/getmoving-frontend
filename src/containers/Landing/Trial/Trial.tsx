import React from 'react';
import { Typography, Box, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';

import { UnderlineWrapper } from 'components';
import styles from './Trial.styles';

const useStyles = makeStyles(styles);

const Trial: React.FC = () => {
  const { container, title, details } = useStyles();
  const { t } = useTranslation();
  const history = useHistory();

  return (
    <Box className={container}>
      <UnderlineWrapper margin="auto" centered>
        <Typography className={title} variant="subtitle1">
          {t('landing.trial.title')}
        </Typography>
      </UnderlineWrapper>
      <Typography className={details}>
        {t('landing.trial.details')}
      </Typography>
      <Button color="primary" variant="contained" onClick={() => history.push('/auth/register')}>
        {t('landing.trial.button')}
      </Button>
    </Box>
  );
};

export default Trial;
