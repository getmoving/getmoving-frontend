import React, { FormEvent, useReducer, useState } from 'react';
import axios from 'axios';
import { TextField, FormControlLabel, Checkbox, Box, Button, CircularProgress, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { useTranslation } from 'react-i18next';

import { API_ROOT } from 'utils/constants';
import { reducer, initialState } from './ContactReducer';
import styles from './Contact.styles';

const useStyles = makeStyles(styles);

const ContactForm: React.FC = () => {
  const { t } = useTranslation();
  const { form, inputProps, inputLabelProps, checkbox } = useStyles({ isLandingPage: false });
  const [state, dispatch] = useReducer(reducer, initialState);
  const [isLoading, setLoading] = useState(false);
  const [isSent, setSent] = useState(false);

  const onSubmit = async (e: FormEvent) => {
    e.preventDefault();
    try {
      setLoading(true);
      await axios.post(`${API_ROOT}/api/send-contact-email`, state);
      setSent(true);
      dispatch(initialState);
    } catch (error) {
      // eslint-disable-next-line no-alert
      alert(error.message); // TODO: use styled alerts
    } finally {
      setLoading(false);
    }
  };

  if (isSent) return <Typography color="secondary">{t('landing.contact.emailSent')}</Typography>;

  return (
    <form className={form} onSubmit={onSubmit}>
      <TextField
        required
        fullWidth
        InputLabelProps={{ className: inputLabelProps, required: false }}
        InputProps={{ className: inputProps }}
        value={state.name}
        onChange={(e) => dispatch({ name: e.target.value })}
        name="name"
        label={t('landing.contact.name')}
      />
      <TextField
        required
        fullWidth
        InputLabelProps={{ className: inputLabelProps, required: false }}
        InputProps={{ className: inputProps }}
        value={state.email}
        onChange={(e) => dispatch({ email: e.target.value })}
        name="email"
        type="email"
        label={t('landing.contact.email')}
      />
      <TextField
        required
        fullWidth
        InputLabelProps={{ className: inputLabelProps, required: false }}
        InputProps={{ className: inputProps }}
        value={state.message}
        onChange={(e) => dispatch({ message: e.target.value })}
        name="message"
        label={t('landing.contact.message')}
      />
      <FormControlLabel
        label={t('landing.contact.checkbox')}
        control={(
          <Checkbox
            required
            classes={{ root: checkbox }}
            color="secondary"
            checked={state.accepted}
            onChange={(e) => dispatch({ accepted: e.target.checked })}
          />
        )}
      />
      <Box textAlign="right" mt={5}>
        <Button
          disabled={isLoading}
          endIcon={isLoading && <CircularProgress size={16} />}
          type="submit"
          variant="contained"
          color="primary"
        >
          {t('landing.contact.send')}
        </Button>
      </Box>
    </form>
  );
};

export default ContactForm;
