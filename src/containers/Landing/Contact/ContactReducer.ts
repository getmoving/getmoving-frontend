export const initialState = {
  name: '',
  email: '',
  message: '',
  accepted: false,
};

type State = typeof initialState;
type Action = Partial<State>;

export const reducer = (s: State, a: Action) => ({ ...s, ...a });
