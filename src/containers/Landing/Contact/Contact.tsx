import React from 'react';
import { Box, Typography, Grid, Theme, useMediaQuery } from '@material-ui/core';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import { useTranslation } from 'react-i18next';
import { useLocation } from 'react-router-dom';

import { UnderlineWrapper } from 'components';
import ContactForm from './ContactForm';
import ContactDetails from './ContactDetails';
import styles from './Contact.styles';

const useStyles = makeStyles(styles);

const Contact: React.FC = () => {
  const location = useLocation();
  const isLandingPage = !location.pathname.includes('contact');
  const { imageContainer, title } = useStyles({ isLandingPage });
  const { t } = useTranslation();
  const theme: Theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down('sm'));

  return (
    <Box className={imageContainer}>
      <Grid container spacing={isMobile ? 0 : 10}>
        <Grid item xs={12} sm={6}>
          <UnderlineWrapper color="white" bottom={-16}>
            <Typography className={title} variant="h2">
              {t('landing.contact.title')}
            </Typography>
          </UnderlineWrapper>
          <ContactDetails />
        </Grid>
        <Grid item xs={12} sm={6}>
          <ContactForm />
        </Grid>
      </Grid>
    </Box>
  );
};

export default Contact;
