import { createStyles, Theme } from '@material-ui/core/styles';
import contactbg1 from 'assets/contactbg.png';
import contactbg2 from 'assets/contactbg2.jpeg';
import colors from 'utils/colors';

interface Props {
  isLandingPage?: boolean;
}

const styles = (theme: Theme) => createStyles<string, Props>({
  imageContainer: {
    backgroundImage: ({ isLandingPage }) => `url(${isLandingPage ? contactbg1 : contactbg2})`,
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    minHeight: ({ isLandingPage }) => (isLandingPage ? 490 : 700),
    width: '100%',
    paddingLeft: 100,
    paddingRight: 100,
    boxSizing: 'border-box',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    [theme.breakpoints.down('sm')]: {
      padding: theme.spacing(4, 2),
    },
  },
  title: {
    fontSize: 50,
    [theme.breakpoints.down('sm')]: {
      fontSize: 30,
    },
    fontWeight: 900,
    color: colors.secondary,
  },
  form: {
    color: colors.secondary,
    display: 'unset',
    '& > div': {
      marginTop: theme.spacing(2),
      marginBottom: theme.spacing(2),
    },
  },
  inputProps: {
    color: colors.secondary,
    '&:before': {
      borderBottomColor: `${colors.secondary} !important`,
    },
  },
  inputLabelProps: {
    color: colors.secondary,
  },
  checkbox: {
    color: colors.secondary,
  },
  table: {
    marginTop: 50,
    '& td': {
      color: colors.secondary,
      border: 'none',
      padding: theme.spacing(1),
      '&:nth-child(2)': {
        color: colors.secondary,
        opacity: 0.7,
      },
    },
  },
});

export default styles;
