import React from 'react';
import { Table, TableBody, TableRow, TableCell } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { useTranslation } from 'react-i18next';

import { CONTACTS } from 'utils/constants';
import styles from './Contact.styles';

const useStyles = makeStyles(styles);

const ContactDetails: React.FC = () => {
  const { t } = useTranslation();
  const { table } = useStyles({ isLandingPage: false });
  return (
    <Table className={table}>
      <TableBody>
        <TableRow>
          <TableCell>{t('common.email')}</TableCell>
          <TableCell>{CONTACTS.email}</TableCell>
        </TableRow>
        <TableRow>
          <TableCell>{t('landing.contact.phone')}</TableCell>
          <TableCell>{CONTACTS.phone}</TableCell>
        </TableRow>
        <TableRow>
          <TableCell>{t('landing.contact.address')}</TableCell>
          <TableCell>{CONTACTS.address}</TableCell>
        </TableRow>
      </TableBody>
    </Table>
  );
};

export default ContactDetails;
