export const API_ROOT = ENV_PRODUCTION
  ? 'https://us-central1-getmoving-5a566.cloudfunctions.net/app'
  : 'http://localhost:5001/getmoving-5a566/us-central1/app';

export const CONTACTS = {
  name: 'Vikár András',
  email: 'get.moving2021@gmail.com',
  phone: '(+36) 30 271 7990',
  address: '1126 Budapest Istenhegyi út 2.',
};

export const PRICES = {
  MONTHLY: {
    MONTH: '3 590',
  },
  YEARLY: {
    YEAR: '34 990',
  },
};
