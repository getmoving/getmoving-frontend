const colors = {
  primary: '#FFC800',
  secondary: '#F8F8F8',
  active: '#FFDC30',
  error: '#F04B35',
  greyF6: '#F6F6F6',
  grey33: '#333',
  grey7D: '#7D8A99',
  grey6D: '#6D6D78',
  grey66: '#666666',
  black1F: '#1F1F1F',
  linkBlue: '#0075FF',
  green: '#91C03E',
  redEB: '#EB5757',
};

export default colors;
