/* eslint-disable */

export const makeChunksFromArray = (arr: any[], size: number) => arr.reduce((all, one, i) => {
  const ch = Math.floor(i / size);
  all[ch] = [].concat(all[ch] || [], one);
  return all;
}, []);
