import { useEffect, useState } from 'react';
import fApp from 'firebase/init-firebase';
import useAxios from 'axios-hooks';
import { API_ROOT } from 'utils/constants';
import { PaymentType } from 'containers/Profile/Subscription/Subscription.types';

interface IGenerator {
  equipment: string[];
  exerciseNumber: string;
  experience: string;
  fitness: string;
  weekNumber: number;
}

export interface IUser {
  disabled: boolean;
  email: string;
  emailVerified: boolean;
  firstName: string;
  lastName: string;
  generator?: IGenerator;
  planId?: string;
  payment?: {
    isActive?: boolean;
    isCancelled?: boolean;
    paymentType?: PaymentType;
    recurrenceId?: string;
    traceId?: string;
    validUntil?: number;
  };
}

const useFirebaseAuthentication = () => {
  const [authUser, setAuthUser] = useState<firebase.User | null>();
  const [userNode, setUserNode] = useState<IUser | null | undefined>(null);
  const [isLoading, setLoading] = useState(true);
  const [{ data, loading }, refetch] = useAxios<IUser>(`${API_ROOT}/api/get-user-by-id/${authUser?.uid}`);

  useEffect(() => {
    fApp.auth().onAuthStateChanged((user) => {
      setAuthUser(user);
      if (!user) {
        setLoading(false);
        setUserNode(null);
        return;
      }
      setUserNode(data);
      setLoading(loading);
    });
  }, [fApp, authUser, loading]);

  return { user: authUser, userNode, isLoading, refetch };
};

export default useFirebaseAuthentication;
