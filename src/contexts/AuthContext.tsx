import React, { createContext, ComponentType } from 'react';
import useFirebaseAuthentication, {
  IUser,
} from 'hooks/useFirebaseAuthentication';
import { LinearProgress } from '@material-ui/core';
import { AxiosPromise } from 'axios';

const AuthContext = createContext<{
  user: firebase.User | null;
  userNode: IUser | null | undefined;
  isLoading: boolean;
  isActive: boolean;
  refetch:() => AxiosPromise<IUser> | null;
    }>({ user: null, userNode: null, isLoading: true, refetch: () => null, isActive: false });

export const withAuthContext = (Component: ComponentType) => () => {
  const { user, userNode, isLoading, refetch } = useFirebaseAuthentication();

  if (user === undefined) return <LinearProgress color="primary" />;
  return (
    <AuthContext.Provider
      value={{ user, userNode, isLoading, refetch, isActive: !!userNode?.payment?.isActive }}
    >
      <Component />
    </AuthContext.Provider>
  );
};

export default AuthContext;
