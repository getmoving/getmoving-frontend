import React, { createContext, ComponentType } from 'react';
import { VideoModal } from 'components';

interface Props {
  url: string;
  setUrl: (v: string) => void;
}

const VideoContext = createContext<Props>({ url: '', setUrl: () => null });

export const withVideoContext = (Component: ComponentType) => () => {
  const [url, setUrl] = React.useState('');

  return (
    <VideoContext.Provider value={{ url, setUrl }}>
      <Component />
      <VideoModal />
    </VideoContext.Provider>
  );
};

export default VideoContext;
