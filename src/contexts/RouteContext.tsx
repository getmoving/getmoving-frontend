import { createContext } from 'react';

export type VariantType = 'black' | 'transparent';
export type ComponentTypeInstance = 'public' | 'private' | 'login' | 'register' | 'blank';

interface Props {
  variant: VariantType;
  componentType: ComponentTypeInstance;
  setComponentType: (componentType: ComponentTypeInstance) => void;
  setVariant: (variant: VariantType) => void;
}

export default createContext<Props>({ variant: 'transparent', componentType: 'blank', setVariant: () => {}, setComponentType: () => {} });
