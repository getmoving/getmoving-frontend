import React, { createContext, useReducer, Dispatch, ComponentType } from 'react';

export enum GENDER {
  OTHER = 'other',
  MALE = 'male',
  FEMALE = 'female',
}

const initialState = {
  firstName: '',
  lastName: '',
  email: '',
  subbed: false,
  accepted: false,
  password: '',
  passwordRepeat: '',
  gender: GENDER.OTHER,
  addressCountry: '',
  addressZipCode: '',
  addressCity: '',
  addressStreet: '',
  addressNumber: '',
};

type State = typeof initialState;
type Action = Partial<State>;

const RegisterContext = createContext<{ state: State; dispatch: Dispatch<Action> }>({
  state: initialState,
  dispatch: () => null,
});

const reducer = (s: State, a: Action) => ({ ...s, ...a });

export const withRegisterContext = (Component: ComponentType) => () => {
  const [state, dispatch] = useReducer(reducer, initialState);

  return (
    <RegisterContext.Provider value={{ state, dispatch }}>
      <Component />
    </RegisterContext.Provider>
  );
};

export default RegisterContext;
